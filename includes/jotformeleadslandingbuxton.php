
<?php

	//Strips all slashes in an array
	function stripslashes_deep($value){
		$value = is_array($value) ?
					array_map('stripslashes_deep', $value) :
					stripslashes($value);
		return $value;
	}
	$result = stripslashes_deep($_REQUEST['rawRequest']);	
	
	$obj = json_decode($result, true);
	$FirstName =  $obj['q13_fullName13']['first'];
	$LastName =  $obj['q13_fullName13']['last'];
	$Email =  $obj['q4_email4'];
	$Ph_No =  $obj['q5_phone']['area'] ."-".  $obj['q5_phone']['phone'];
	$PreviousEducation = $obj['q8_graduated'];
	$Campus = $obj['q6_campusOf'];
	$LeadSource = 'BUXTON';
//  $ProgramInterest = "NDS";
      
    $campusArr=array();
    $campusArr['Amarillo']='AMARILLO';
    $campusArr['Beaumont']='BEAUMONT';
	$campusArr['College Station']='COLSTAT';
    $campusArr['El Paso']='ELPASO';
    $campusArr['Las Cruces']='LASCRUCE';
    $campusArr['Lubbock']='LUBBOCK';
    $campusArr['Longview']='LONGVIEW';
    $campusArr['Online']='ONLINE';
    $campusArr['Killeen']='KILLEEN';
    $Campus=$campusArr[$Campus];
       
    $previousEducationArr=array();
    $previousEducationArr['High School / GED']='HS';
    $previousEducationArr['College Degree']='COL-HS';
    $previousEducationArr['None of the above']='NONHSGRA';
    $PreviousEducation=$previousEducationArr[$PreviousEducation];
	
	
	$LeadType="VWEB";
   
		//$url="https://api5061.campusnet.net/Cmc.Integration.LeadImport.HttpPost/ImportLeadProcessor.aspx";
		$url="https://api1.vistacollege.edu/cmc.integration.leadimport.httppost/importleadprocessor.aspx";
		$dataStr="?Format=Jotform&LeadType=".$LeadType."&FirstName=".$FirstName."&LastName=".$LastName."&Email=".$Email."&Phone=".$Ph_No."&Campus=".$Campus."&PostalCodeOrZip=''&LeadSource=".$LeadSource."&PreviousEducation=".$PreviousEducation;
		
    $dataStr=str_replace(" ", "_", $dataStr);
	
    $ch = curl_init();
    // Set query data here with the URL
    curl_setopt($ch, CURLOPT_URL, $url . $dataStr);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, '3600');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $xmlResponse = trim(curl_exec($ch));
    curl_close($ch);
		
	
?>
