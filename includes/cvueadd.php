<?php
		
  function sanitize($var)
	{
			$var = stripslashes($var);
			$var = strip_tags($var);
			$var = htmlentities($var);
			return $var;
	}
	/* Get all Post vars */
	$campus=sanitize($_POST['campus']);
	$email=sanitize($_POST['email']);
	$phone=sanitize($_POST['phone']);
	$campustype=sanitize($_POST['campustype']);
	$fname=sanitize($_POST['fname']);
	$lname=sanitize($_POST['lname']);
	$prevedu=sanitize($_POST['prevedu']);
	$program=sanitize($_POST['program']);
	//$source=@$_SESSION['source']; 
	
	$source=sanitize($_POST['source']);	
	$LeadType="VWEB";
	
	
	$phone = preg_replace('/\D/', '', $phone);
	$phone = substr($phone,-10);
	$PhoneAreacode = substr($phone,0,3);
	$PhoneLast = substr($phone,-7);
	$phone = $PhoneAreacode."-".$PhoneLast;

	if (strpos($program, 'hvac') !== false) { // for Trades Management - HVAC AAS
			$ProgramInterest="HVAC";
	}
	if (strpos($program, 'Security') !== false) {  // for AAS Information Systems Security & Assurance Online
			$ProgramInterest="ISA";
	}
	 
	
switch ($program)
{
	case "Information Technology Diploma":
		$ProgramInterest="INFT";
		break;
	case "Information Technology Associate of Applied Science":
		$ProgramInterest="INFT";
		break;
	case "Information Technology Associate Degree":
		$ProgramInterest="INFT";
		break;
	case "Information Systems Security & Assurance Associate of Applied Science":
		$ProgramInterest="INFT";
		break;
	case "Business Administration Diploma":
		$ProgramInterest="BUS";
		break;
	case "Business Administration Associate of Applied Science":
		$ProgramInterest="BUS";
		break;
	case "Business Administration Associate Degree":
		$ProgramInterest="BUS";
		break;
	case "Dental Assistant Diploma":
		$ProgramInterest="DA";
		break;
	case "Medical Lab Technician Associate of Applied Science":
		$ProgramInterest="MLT";
		break;
	case "Medical Lab Technician Associate Degree":
		$ProgramInterest="MLT";
		break;
	case "Vocational/Practical Nurse Diploma":
		$ProgramInterest="PN";
		break;
	case "Vocational Nurse Diploma":
		$ProgramInterest="PN";
		break;
	case "Medical Assisting Diploma":
		$ProgramInterest="MAS";
		break;
	case "Medical Assisting Associate of Applied Science":
		$ProgramInterest="MAS";
		break;
	case "Medical Assisting Associate Degree":
		$ProgramInterest="MAS";
		break;
	case "Medical Insurance Billing and Coding Diploma Program":
		$ProgramInterest="MBC";
		break;
	case "Medical Insurance Billing and Coding Associate of Applied Science":
		$ProgramInterest="MBC";
		break;
	case "Medical Insurance Billing and Coding Associate Degree":
		$ProgramInterest="MBC";
		break;
	case "Veterinary Technology Associate of Applied Science":
		$ProgramInterest="VET";
		break;
	case "Business Management Associate of Applied Science":
		$ProgramInterest="BUS";
		break;
	case "Business Management Associate Degree":
		$ProgramInterest="BUS";
		break;
	case "Business Management Accounting Associate of Applied Science":
		$ProgramInterest="BMACCT";
		break;
	case "Business Management Accounting Associate Degree":
		$ProgramInterest="BMACCT";
		break;
	case "Criminal Justice Associate of Applied Science":
		$ProgramInterest="CJ";
		break;
	case "Criminal Justice Associate Degree":
		$ProgramInterest="CJ";
		break;
	case "Paralegal Associate of Applied Science":
		$ProgramInterest="PL";
		break;
	case "Paralegal Associate Degree":
		$ProgramInterest="PL";
		break;
	case "Construction Management Associate of Applied Science":
		$ProgramInterest="CTD";
		break;
	case "Construction Management Associate Degree":
		$ProgramInterest="CTD";
		break;
	case "Construction Technology Diploma":
		$ProgramInterest="CTD";
		break;
	case "Industrial Maintenance Mechanic Diploma":
		$ProgramInterest="IMM";
	case "Cosmetology Certificate of Proficiency":
		$ProgramInterest="COS";
		break;
	case "Cosmetology Instructor Program":
		$ProgramInterest="COS";
		break;
	case "Cosmetology":
			$ProgramInterest="COS";
		break;
	case "Electrical Technician Diploma":
		$ProgramInterest="ETD";
		break;
	case "HVAC Diploma":
		$ProgramInterest="HVAC";
		break;
	case "Trades Management – HVAC Associate of Applied Science":
		$ProgramInterest="HVAC";
		break;
	case "Trades Management – HVAC Associate Degree":
		$ProgramInterest="HVAC";
		break;
	case "HVAC Associate Degree":
		$ProgramInterest="HVAC";
		break;
	case "Supply Chain Management Associate of Applied Science Online":
		$ProgramInterest="SCMA";
		break;
	case "Supply Chain Management Associate Degree Online":
		$ProgramInterest="SCMA";
		break;
	case "Healthcare Administration Bachelor of Science Online":
		$ProgramInterest="BHCA";
		break;
	case "Healthcare Administration Bachelor Degree Online":
		$ProgramInterest="BHCA";
		break;
	case "Project Management Bachelor of Science Online":
		$ProgramInterest="BSPM";
		break;
	case "Project Management Bachelor Degree Online":
		$ProgramInterest="BSPM";
		break;
	case "Medical Insurance Billing and Coding Diploma Online":
		$ProgramInterest="MBC";
		break;
	case "Medical Insurance Billing and Coding Associate of Applied Science Online":
		$ProgramInterest="MBC";
		break;
	case "Medical Insurance Billing and Coding Associate Degree Online":
		$ProgramInterest="MBC";
		break;
	case "Business Administration and Leadership Diploma Online":
		$ProgramInterest="BUS";
		break;
	case "Business Administration Associate of Applied Science Online":
		$ProgramInterest="BUS";
		break;
	case "Business Administration Associate Degree Online":
		$ProgramInterest="BUS";
		break;
	case "Business Administration Bachelor of Science Online":
		$ProgramInterest="BSPM";
		break;
	case "Business Administration Bachelor Degree Online":
		$ProgramInterest="BSPM";
		break;
	case "Information Technology Online":
		$ProgramInterest="INFT";
		break;
	case "Criminal Justice Associate of Applied Science Online":
		$ProgramInterest="CJ";
		break;
	case "Criminal Justice Associate Degree Online":
		$ProgramInterest="CJ";
		break;
	case "Logistics And Operations Management":
		$ProgramInterest="LOAM";
		break;
	case "-Undecided-":
		$ProgramInterest="UND";
		break;
	default:
		$ProgramInterest="UND";
}
	
	if (strpos($campus, 'Amarillo') !== false) {
		$campuscode="AMARILLO";
	} elseif (strpos($campus, 'Beaumont') !== false) {
		$campuscode="BEAUMONT";
	} elseif (strpos($campus, 'Station') !== false) {
		$campuscode="COLSTAT";
	} elseif (strpos($campus, 'Paso') !== false) {
		$campuscode="ELPASO";
	} elseif (strpos($campus, 'Smith') !== false) {
		$campuscode="FTSMITH";
	} elseif (strpos($campus, 'Cruces') !== false) {
		$campuscode="LASCRUCE";
	} elseif (strpos($campus, 'Lubbock') !== false) {
		$campuscode="LUBBOCK";
	} elseif (strpos($campus, 'Longview') !== false) {
		$campuscode="LONGVIEW";
	} elseif (strpos($campus, 'Online') !== false) {
		$campuscode="ONLINE";
	} elseif (strpos($campus, 'Killeen') !== false) {
		$campuscode="KILLEEN";
	}

	switch($source){
		case "Internet":
			$leadsource="INT";
			break;
		case "Facebook":
			$leadsource="FB";
			break;
		case "TV Ads":
			$leadsource="TV";
			break;
		case "Newspaper":
			$leadsource="NP";
			break;
		case "Billboard":
			$leadsource="BLBRD";
			break;
		case "Radio":
			$leadsource="RADIO";
			break;
		case "Friend":
			$leadsource="REFERRAL";
			break;
	}
	
	switch($prevedu){
		case "High School / GED":
			$previousedu="HS";
			break;
		case "College Degree":
			$previousedu="COL-HS";
			break;
		
		case "None of the above":
			$previousedu="NONHSGRA";
			break;
	}
	
	$curfile=dirname(__FILE__).'\\'.basename(__FILE__);
	$url="https://api1.vistacollege.edu/cmc.integration.leadimport.httppost/importleadprocessor.aspx";
	$dataStr="?Format=Jotform&LeadType=".$LeadType."&FirstName=".$fname."&LastName=".$lname."&Email=".$email."&Phone=".$phone."&Campus=".$campuscode."&LeadSource=".$leadsource."&PreviousEducation=".$previousedu."&Program=".$ProgramInterest;
	$dataStr=str_replace(" ", "%20", $dataStr);

	$ch = curl_init();
    // Set query data here with the URL
    curl_setopt($ch, CURLOPT_URL, $url . $dataStr);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, '3600');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $xmlResponse = trim(curl_exec($ch));
    curl_close($ch); 
	
	
	$dataStr= $dataStr . $curfile;		
	mysql_connect("localhost", "vistacollege_edu", "r7J633Bb23") or die(mysql_error());
	mysql_select_db("vistacollege_edu") or die(mysql_error());
	$myquery="INSERT INTO eleads (LeadType, FirstName, LastName, Email,Ph_No,Campus,LeadSource,PreviousEducation,Response,date,dataSTR,Prog_Interest,Source) VALUES('$LeadType', '$FirstName','$LastName','$Email','$Phone','$Campus','$LeadSource','$PreviousEducation','$xmlResponse',now(),'$dataStr','$ProgramInterest','$utm_source') ";
	
	/*
	foreach ($_POST as $key => $value)
 $msg= $msg . "Field ".htmlspecialchars($key)." is ".htmlspecialchars($value)."<br>";
 
	ini_set( 'display_errors', 1 );
    error_reporting( E_ALL );
    $from = "suyadi@gmail.com";
    $to = "suyadi@gmail.com";
    $subject = "FB Leads result";
    $message = "Testing Cvue";
    $headers = "From:" . $from;
	$msg=$msg . "\n\n $dataStr response: $xmlResponse \n\n $myquery \n\n lastname: $templastname";
    mail($to,$subject,$msg, $headers);
	   */
?>
