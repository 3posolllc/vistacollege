<?php
	session_start();
?>	
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title>Form</title>
<link href="http://max.jotfor.ms/min/g=formCss?3.1.595" rel="stylesheet" type="text/css" />
<style type="text/css">
    .form-label{
        width:190px !important;
    }
    .form-label-left, .form-label-right, .form-label-top{
        width:190px !important;
    }
    .form-line{
        padding-top:1px;
        padding-bottom:1px;
    }
    body, html{
        margin:0;
        padding:0;
        background:#fef5ed;
    }

    .form-all{
        margin:0px auto;
        padding-top:20px;
        width:210px;
        background:#fef5ed;
        color:#000000 !important;
        font-family:'Verdana';
        font-size:12px;
    }
</style>

<script src="http://max.jotfor.ms/min/g=jotform?3.1.595" type="text/javascript"></script>
<script type="text/javascript">
   JotForm.init(function(){
      $('input_4').hint('ex: myname@example.com');
   });
</script>


<script>
    function loadXMLDoc()
    {
        //document.form_22267385809968.submit();
        var FirstName=document.getElementById("first_14").value;
        var LastName=document.getElementById("last_14").value;
        var Email=document.getElementById("input_4").value;
        var Area=document.getElementById("input_5_area").value;
        var Phone=document.getElementById("input_5_phone").value;
        var PreviousEducation=document.getElementById("input_8").value;
        var Campus=document.getElementById("input_6").value;
        var LeadSource=document.getElementById("input_7").value;

       //alert(FirstName);
       //alert(LastName);
       //alert(Email);
       //alert(Area);
       //alert(Phone);
       //alert(PreviousEducation);
       //alert(Campus);
       //alert(LeadSource);   
       // 
        
        if(FirstName!='' && LastName!='' && Email!='' && Area!='' && Phone!='' && PreviousEducation!='' && Campus!='' && LeadSource!='')
        {
            document.getElementById("input_2").type="button";
            
            var xmlhttp;
            if (window.XMLHttpRequest)
            {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp=new XMLHttpRequest();
            }
            else
            {// code for IE6, IE5
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange=function()
            {
                if (xmlhttp.readyState==4 && xmlhttp.status==200)
                {
                    //document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
                    //alert(xmlhttp.responseText);
                    
                    document.form_22267385809968.submit();
                    //return true;
                }
                else
                {
                    //alert('error');
                    //return false;
                }
            }
            xmlhttp.open("GET","ajax_campus2.php?FirstName="+FirstName+"&LastName="+LastName+"&Email="+Email+"&Area="+Area+"&Phone="+Phone+"&PreviousEducation="+PreviousEducation+"&Campus="+Campus+"&LeadSource="+LeadSource,true);
            xmlhttp.send();
        }
        else
        {
             document.getElementById("input_2").type="submit";
        }
        //return false;
    }
</script>
<!--<script type="text/javascript" src="jquery-latest.js"></script>-->
<!--<script type="text/javascript" src="common_js.js"></script>-->
</head>
<body>
<form class="jotform-form" action="http://submit.jotformpro.com/submit/22267385809968/" method="post" name="form_22267385809968" id="22267385809968" accept-charset="utf-8">
  <input type="hidden" name="formID" value="22267385809968" />
  <div class="form-all">
    <ul class="form-section">
      <li class="form-line" id="id_14">
        <label class="form-label-top" id="label_14" for="input_14">
          Full Name<span class="form-required">*</span>
        </label>
        <div id="cid_14" class="form-input-wide"><span class="form-sub-label-container"><input class="form-textbox validate[required]" type="text" size="8" name="q14_fullName14[first]" id="first_14" />
            <label class="form-sub-label" for="first_14" id="sublabel_first"> First Name </label></span><span class="form-sub-label-container"><input class="form-textbox validate[required]" type="text" size="12" name="q14_fullName14[last]" id="last_14" />
            <label class="form-sub-label" for="last_14" id="sublabel_last"> Last Name </label></span>
        </div>
      </li>
      <li class="form-line" id="id_4">
        <label class="form-label-top" id="label_4" for="input_4">
          E-mail<span class="form-required">*</span>
        </label>
        <div id="cid_4" class="form-input-wide">
          <input type="email" class="form-textbox validate[required, Email]" id="input_4" name="q4_email" size="23" />
        </div>
      </li>
      <li class="form-line" id="id_5">
        <label class="form-label-top" id="label_5" for="input_5">
          Phone Number<span class="form-required">*</span>
        </label>
        <div id="cid_5" class="form-input-wide"><span class="form-sub-label-container"><input class="form-textbox validate[required]" type="tel" name="q5_phoneNumber5[area]" id="input_5_area" size="3">
            -
            <label class="form-sub-label" for="input_5_area" id="sublabel_area"> Area Code </label></span><span class="form-sub-label-container"><input class="form-textbox validate[required]" type="tel" name="q5_phoneNumber5[phone]" id="input_5_phone" size="8">
            <label class="form-sub-label" for="input_5_phone" id="sublabel_phone"> Phone Number </label></span>
        </div>
      </li>
      <li class="form-line" id="id_8">
        <label class="form-label-top" id="label_8" for="input_8">
          Graduated<span class="form-required">*</span>
        </label>
        <div id="cid_8" class="form-input-wide">
          <select class="form-dropdown validate[required]" style="width:150px" id="input_8" name="q8_graduated">
            <option>  </option>
            <option value="High School / GED"> High School / GED </option>
            <option value="College Degree"> College Degree </option>
            <option value="None of the above"> None of the above </option>
          </select>
        </div>
      </li>
      <li class="form-line" id="id_6">
        <label class="form-label-top" id="label_6" for="input_6">
          Campus of interest<span class="form-required">*</span>
        </label>
        <div id="cid_6" class="form-input-wide">
          <select class="form-dropdown validate[required]" style="width:150px" id="input_6" name="q6_campusOf">
            <option>  </option>
            <option value="Amarillo"> Amarillo </option>
            <option value="Beaumont"> Beaumont </option>
            <option value="El Paso"> El Paso </option>
            <option value="Las Cruces"> Las Cruces </option>
            <option value="Lubbock"> Lubbock </option>
            <option value="Longview"> Longview </option>
            <option value="Killeen"> Killeen</option>
            <option value="Online"> Online </option>
          </select>
        </div>
      </li>
      <li class="form-line" id="id_7">
        <label class="form-label-top" id="label_7" for="input_7">
          How did you hear about us<span class="form-required">*</span>
        </label>
        <div id="cid_7" class="form-input-wide">
          <select class="form-dropdown validate[required]" style="width:150px" id="input_7" name="q7_howDid">
            <option>  </option>
            <option value="Internet"> Internet </option>
            <option value="Facebook"> Facebook </option>
            <option value="TV Ads"> TV Ads </option>
            <option value="Newspaper"> Newspaper </option>
            <option value="Billboard"> Billboard </option>
            <option value="Radio"> Radio </option>
            <option value="Friend"> Friend </option>
          </select>
        </div>
      </li>
      <li class="form-line" id="id_2">
        <div id="cid_2" class="form-input-wide">
          <div style="text-align:center" class="form-buttons-wrapper">
            
            <!--<input id="input_2" type="button" class="form-submit-button form-submit-button-img" onclick="loadXMLDoc()">-->
           <button onclick="loadXMLDoc()" id="input_2" type="button" class="form-submit-button form-submit-button-img">
              <img src="http://216.244.89.213/wp-content/themes/vista/images/submit_btn.png" alt="Submit" />
            </button>
          </div>
        </div>
      </li>
      <li class="form-line" id="id_11">
        <div id="cid_11" class="form-input-wide">
          <div id="text_11" class="form-html">
            <p>
              We
              <a title="Vista College Privacy Statement" href="http://www.vistacollege.edu/privacy-statement/" target="_blank">Respect Your Privacy</a>
            </p>
          </div>
        </div>
      </li>
      <li style="display:none">
        Should be Empty:
        <input type="text" name="website" value="" />
      </li>
    </ul>
  </div>
  <input type="hidden" id="simple_spc" name="simple_spc" value="22267385809968" />
  <script type="text/javascript">
  document.getElementById("si" + "mple" + "_spc").value = "22267385809968-22267385809968";
  </script>
  <input type="hidden" class="form-hidden" value="<?php echo $_SESSION['source'] ?>" id="input_12" name="q12_source" />
</form></body>
</html>
