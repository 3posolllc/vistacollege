
<?php

	//Strips all slashes in an array
	function stripslashes_deep($value){
		$value = is_array($value) ?
					array_map('stripslashes_deep', $value) :
					stripslashes($value);
		return $value;
	}
	$result = stripslashes_deep($_REQUEST['rawRequest']);	
	
	$obj = json_decode($result, true);
	$FirstName =  $obj['q13_fullName']['first'];
	$LastName =  $obj['q13_fullName']['last'];
	$Email =  $obj['q4_email'];
	$Ph_No =  $obj['q5_phoneNumber5']['area'] ."-".  $obj['q5_phoneNumber5']['phone'];
	$PreviousEducation = $obj['q8_graduated'];
	$Campus = $obj['q6_campusOf'];
	$LeadSource = $obj['q7_howDid'];
	
   $ProgramInterest = $obj['q17_programsOf'];
   
	 $program=$ProgramInterest;
	 include("programcodes.php");
   
    $campusArr=array();
    $campusArr['Amarillo']='AMARILLO';
    $campusArr['Beaumont']='BEAUMONT';
	$campusArr['College Station']='COLSTAT';
    $campusArr['El Paso']='ELPASO';
	$campusArr['Fort Smith']='FTSMITH';
    $campusArr['Las Cruces']='LASCRUCE';
    $campusArr['Lubbock']='LUBBOCK';
    $campusArr['Longview']='LONGVIEW';
    $campusArr['Online']='ONLINE';
    $campusArr['Killeen']='KILLEEN';
    $Campus=$campusArr[$Campus];
    
    $leadSourceArr=array();
    $leadSourceArr['Internet']='INT';
    $leadSourceArr['Facebook']='FB';
    $leadSourceArr['TV Ads']='TV';
    $leadSourceArr['Newspaper']='NP';
    $leadSourceArr['Billboard']='BLBRD';
    $leadSourceArr['Radio']='RADIO';
    $leadSourceArr['Friend']='REFERRAL';
    $LeadSource=$leadSourceArr[$LeadSource];
    
    $previousEducationArr=array();
    $previousEducationArr['High School / GED']='HS';
    $previousEducationArr['College Degree']='COL-HS';
    $previousEducationArr['None of the above']='NONHSGRA';
    $PreviousEducation=$previousEducationArr[$PreviousEducation];
	
	
	$LeadType="VWEB";
   
		//$url="https://api5061.campusnet.net/Cmc.Integration.LeadImport.HttpPost/ImportLeadProcessor.aspx";
		$url="https://api1.vistacollege.edu/cmc.integration.leadimport.httppost/importleadprocessor.aspx";
		$dataStr="?Format=Jotform&LeadType=".$LeadType."&FirstName=".$FirstName."&LastName=".$LastName."&Email=".$Email."&Phone=".$Ph_No."&Campus=".$Campus."&LeadSource=".$LeadSource."&PreviousEducation=".$PreviousEducation."&Program=".$ProgramInterest;
		
    $dataStr=str_replace(" ", "%20", $dataStr);
	
    $ch = curl_init();
    // Set query data here with the URL
    curl_setopt($ch, CURLOPT_URL, $url . $dataStr);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, '3600');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $xmlResponse = trim(curl_exec($ch));
    curl_close($ch);
		
	
?>
