
<?php

	//Strips all slashes in an array
	function stripslashes_deep($value){
		$value = is_array($value) ?
					array_map('stripslashes_deep', $value) :
					stripslashes($value);
		return $value;
	}
	$result = stripslashes_deep($_REQUEST['rawRequest']);	
	
	$obj = json_decode($result, true);
	$FirstName =  $obj['q13_fullName']['first'];
	$LastName =  $obj['q13_fullName']['last'];
	$Email =  $obj['q4_email4'];
	$Ph_No =  $obj['q5_phone']['area'] ."-".  $obj['q5_phone']['phone'];
	$PreviousEducation = $obj['q8_graduated'];
	$Campus = $obj['q6_campusOf'];
	$LeadSource = $obj['q7_howDid'];
	
   $ProgramInterest = $obj['q19_program'];
   
	switch ($ProgramInterest)
	{
	case "Business Administration":
	  $ProgramInterest="BUS";
	  break;
	case "Business Management Accounting":
	  $ProgramInterest="BMACCT";
	  break;
	case "Business Management Bachelors Degree":
	  $ProgramInterest="BSPM";
	  break;
	case "Business Management Human Resources":
	  $ProgramInterest="BMHR";
	  break;
	case "Construction":
	  $ProgramInterest="CTD";
	  break;
	case "Cosmetology":
	  $ProgramInterest="COS";
	  break;
	case "Criminal Justice":
	  $ProgramInterest="CJ";
	  break;
	case "Dental Assistant":
	  $ProgramInterest="DA";
	  break;
	case "EKG Technician":
	  $ProgramInterest="EKG";
	  break;
	case "Electrical Technician":
	  $ProgramInterest="ETD";
	  break;
	case "Electronic Medical Records Technician":
	  $ProgramInterest="EMRT";
	  break;
	case "Executive Administrative Assistant":
	  $ProgramInterest="EAA";
	  break;
	case "Healthcare Administration":
	  $ProgramInterest="BHCA";
	  break;
	case "Heating, Ventilating, and Air Conditioning":
	  $ProgramInterest="HVAC";
	  break;
	case "Massage Therapy":
	  $ProgramInterest="MT";
	  break;
	case "Medical Assisting":
	  $ProgramInterest="MAS";
	  break;
	case "Medical Information Specialist":
	  $ProgramInterest="MIS";
	  break;
	case "Medical Insurance Billing and Coding":
	  $ProgramInterest="MBC";
	  break;
	case "Medical Lab Technician":
	  $ProgramInterest="MLT";
	  break;
	case "Medical Office Specialist":
	  $ProgramInterest="MOS";
	  break;
	case "Information Technology":
	  $ProgramInterest="INFT";
	  break;
	case "Computer-Aided Drafting and Design":
	  $ProgramInterest="CAD";
	  break;
	case "Non-Degree Seeking":
	  $ProgramInterest="UND";
	  break;
	case "Nurse Assistant":
	  $ProgramInterest="NA";
	  break;
	case "Paralegal":
	  $ProgramInterest="PL";
	  break;
	case "Patient Care Technician":
	  $ProgramInterest="PCT";
	  break;
	case "Personal Fitness Trainer":
	  $ProgramInterest="PFT";
	  break;
	case "Phlebotomy":
	  $ProgramInterest="PHLE";
	  break;
	case "Practical Nurse":
	  $ProgramInterest="PN";
	  break;
	case "-Undecided-":
	  $ProgramInterest="UND";
	  break;
	case "Veterinary Technology":
	  $ProgramInterest="VET";
	  break;
	case "Vocational Nurse":
	  $ProgramInterest="VN";
	  break;
	case "Project Management":
	  $ProgramInterest="BSPM";
	  break;
	case "Plumbing":
	  $ProgramInterest="PLU";
	  break;
	case "Information Systems Security":
	  $ProgramInterest="ISA";
	  break;
	case "Industrial Maintenance Mechanic":
	  $ProgramInterest="IMM";
	  break;
	case "Supply Chain Management":
	  $ProgramInterest="SCMA";
	  break;
	}
   
    $campusArr=array();
    $campusArr['Amarillo']='AMARILLO';
    $campusArr['Beaumont']='BEAUMONT';
	$campusArr['College Station']='COLSTAT';
    $campusArr['El Paso']='ELPASO';
	$campusArr['Fort Smith']='FTSMITH';
    $campusArr['Las Cruces']='LASCRUCE';
    $campusArr['Lubbock']='LUBBOCK';
    $campusArr['Longview']='LONGVIEW';
    $campusArr['Online']='ONLINE';
    $campusArr['Killeen']='KILLEEN';
    $Campus=$campusArr[$Campus];
    
    $leadSourceArr=array();
    $leadSourceArr['Internet']='INT';
    $leadSourceArr['Facebook']='FB';
    $leadSourceArr['TV Ads']='TV';
    $leadSourceArr['Newspaper']='NP';
    $leadSourceArr['Billboard']='BLBRD';
    $leadSourceArr['Radio']='RADIO';
    $leadSourceArr['Friend']='REFERRAL';
    $LeadSource=$leadSourceArr[$LeadSource];
    
    $previousEducationArr=array();
    $previousEducationArr['High School / GED']='HS';
    $previousEducationArr['College Degree']='COL-HS';
    $previousEducationArr['None of the above']='NONHSGRA';
    $PreviousEducation=$previousEducationArr[$PreviousEducation];
	
	
	$LeadType="VWEB";
   
		//$url="https://api5061.campusnet.net/Cmc.Integration.LeadImport.HttpPost/ImportLeadProcessor.aspx";
		$url="https://api1.vistacollege.edu/cmc.integration.leadimport.httppost/importleadprocessor.aspx";
		$dataStr="?Format=Jotform&LeadType=".$LeadType."&FirstName=".$FirstName."&LastName=".$LastName."&Email=".$Email."&Phone=".$Ph_No."&Campus=".$Campus."&LeadSource=".$LeadSource."&PreviousEducation=".$PreviousEducation."&Program=".$ProgramInterest;
		
    $dataStr=str_replace(" ", "%20", $dataStr);
	
    $ch = curl_init();
    // Set query data here with the URL
    curl_setopt($ch, CURLOPT_URL, $url . $dataStr);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, '3600');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $xmlResponse = trim(curl_exec($ch));
    curl_close($ch);
		
?>
