<?php
switch ($program)
		{
			case "Information Technology Diploma":
				$ProgramInterest="INFT";
				break;
			case "Information Technology Associate of Applied Science":
				$ProgramInterest="INFT";
				break;
			case "Information Technology Associate Degree":
				$ProgramInterest="INFT";
				break;
			case "Information Systems Security & Assurance Associate of Applied Science":
				$ProgramInterest="INFT";
				break;
			case "Business Administration Diploma":
				$ProgramInterest="BUS";
				break;
			case "Business Administration Associate of Applied Science":
				$ProgramInterest="BUS";
				break;
			case "Business Administration Associate Degree":
				$ProgramInterest="BUS";
				break;
			case "Dental Assistant Diploma":
				$ProgramInterest="DA";
				break;
			case "Medical Lab Technician Associate of Applied Science":
				$ProgramInterest="MLT";
				break;
			case "Medical Lab Technician Associate Degree":
				$ProgramInterest="MLT";
				break;
			case "Vocational/Practical Nurse Diploma":
				$ProgramInterest="PN";
				break;
			case "Vocational Nurse Diploma":
				$ProgramInterest="PN";
				break;
			case "Medical Assisting Diploma":
				$ProgramInterest="MAS";
				break;
			case "Medical Assisting Associate of Applied Science":
				$ProgramInterest="MAS";
				break;
			case "Medical Assisting Associate Degree":
				$ProgramInterest="MAS";
				break;
			case "Medical Insurance Billing and Coding Diploma Program":
				$ProgramInterest="MBC";
				break;
			case "Medical Insurance Billing and Coding Associate of Applied Science":
				$ProgramInterest="MBC";
				break;
			case "Medical Insurance Billing and Coding Associate Degree":
				$ProgramInterest="MBC";
				break;
			case "Veterinary Technology Associate of Applied Science":
				$ProgramInterest="VET";
				break;
			case "Business Management Associate of Applied Science":
				$ProgramInterest="BUS";
				break;
			case "Business Management Associate Degree":
				$ProgramInterest="BUS";
				break;
			case "Business Management Accounting Associate of Applied Science":
				$ProgramInterest="BMACCT";
				break;
			case "Business Management Accounting Associate Degree":
				$ProgramInterest="BMACCT";
				break;
			case "Criminal Justice Associate of Applied Science":
				$ProgramInterest="CJ";
				break;
			case "Criminal Justice Associate Degree":
				$ProgramInterest="CJ";
				break;
			case "Paralegal Associate of Applied Science":
				$ProgramInterest="PL";
				break;
			case "Paralegal Associate Degree":
				$ProgramInterest="PL";
				break;
			case "Construction Management Associate of Applied Science":
				$ProgramInterest="CTD";
				break;
			case "Construction Management Associate Degree":
				$ProgramInterest="CTD";
				break;
			case "Construction Technology Diploma":
				$ProgramInterest="CTD";
				break;
			case "Industrial Maintenance Mechanic Diploma":
				$ProgramInterest="IMM";
			case "Cosmetology Certificate of Proficiency":
				$ProgramInterest="COS";
				break;
			case "Cosmetology Instructor Program":
				$ProgramInterest="COS";
				break;
			case "Cosmetology":
					$ProgramInterest="COS";
					break;
			case "Electrical Technician Diploma":
				$ProgramInterest="ETD";
				break;
			case "HVAC Diploma":
				$ProgramInterest="HVAC";
				break;
			case "Trades Management – HVAC Associate of Applied Science":
				$ProgramInterest="HVAC";
				break;
			case "Trades Management – HVAC Associate Degree":
				$ProgramInterest="HVAC";
				break;
			case "HVAC Associate Degree":
				$ProgramInterest="HVAC";
				break;
			case "Supply Chain Management Associate of Applied Science Online":
				$ProgramInterest="LOAM";
				break;
			case "Supply Chain Management Associate Degree Online":
				$ProgramInterest="LOAM";
				break;
			case "Logistics and Operations Management Associate of Applied Science Online":
				$ProgramInterest="LOAM";
				break;
			case "Logistics and Operations Management Associate Degree Online":
				$ProgramInterest="LOAM";
				break;
			case "Healthcare Administration Bachelor of Science Online":
				$ProgramInterest="BHCA";
				break;
			case "Healthcare Administration Bachelor Degree Online":
				$ProgramInterest="BHCA";
				break;
			case "Medical Administrative Assistant Diploma Online":
				$ProgramInterest="MAA";
				break;
			case "Project Management Bachelor Degree Online":
				$ProgramInterest="BSPM";
				break;
			case "Medical Insurance Billing and Coding Diploma Online":
				$ProgramInterest="MBC";
				break;
			case "Medical Insurance Billing and Coding Diploma Online":
				$ProgramInterest="MBC";
				break;
			case "Medical Insurance Billing and Coding Associate of Applied Science Online":
				$ProgramInterest="MBC";
				break;
			case "Medical Insurance Billing and Coding Associate Degree Online":
				$ProgramInterest="MBC";
				break;
			case "Business Administration and Leadership Diploma Online":
				$ProgramInterest="BUS";
				break;
			case "Business Administration and Leadership Associate of Applied Science Online":
				$ProgramInterest="BUS";
				break;
			case "Business Administration and Leadership Associate Degree Online":
				$ProgramInterest="BUS";
				break;
			case "Business Administration Associate of Applied Science Online":
				$ProgramInterest="BUS";
				break;
			case "Business Administration Associate Degree Online":
				$ProgramInterest="BUS";
				break;
			case "Business Administration Bachelor of Science Online":
				$ProgramInterest="BSPM";
				break;
			case "Business Administration Bachelor Degree Online":
				$ProgramInterest="BSPM";
				break;
			case "Information Technology Online":
				$ProgramInterest="INFT";
				break;
			case "Criminal Justice Associate of Applied Science Online":
				$ProgramInterest="CJ";
				break;
			case "Criminal Justice Associate Degree Online":
				$ProgramInterest="CJ";
				break;
			case "Logistics And Operations Management":
				$ProgramInterest="LOAM";
				break;
			case "-Undecided-":
				$ProgramInterest="UND";
				break;
			default:
				$ProgramInterest="UND";
	}
	?>