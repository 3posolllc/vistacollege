<?php 
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>lform</title>
</head>

<body>
<script src="http://max.jotfor.ms/min/g=jotform?3.0.2741" type="text/javascript"></script>
<script type="text/javascript">
   JotForm.init(function(){
      $('input_4').hint('ex: myname@example.com');
   });
</script>
<link href="http://max.jotfor.ms/min/g=formCss?3.0.2741" rel="stylesheet" type="text/css" />
<style type="text/css">
    body {background:#dea71f;}
	.form-label{
        width:150px !important;
    }
    .form-label-left{
        width:150px !important;
    }
    .form-line{
        padding:1px;
    }
    .form-label-right{
        width:150px !important;
    }
    .form-all{
        width:240px;
        background:#dea71f;
        color:#000000 !important;
        font-family:Verdana;
        font-size:12px;
    }
</style>

<form class="jotform-form" action="http://submit.jotformpro.com/submit/20647577549972/" method="post" name="form_20647577549972" id="20647577549972" accept-charset="utf-8">
  <input type="hidden" name="formID" value="20647577549972" />
  <div class="form-all">
    <ul class="form-section">
      <li class="form-line" id="id_9">
        <label class="form-label-top" id="label_9" for="input_9">
          Full Name<span class="form-required">*</span>
        </label>
        <div id="cid_9" class="form-input-wide">
          <input type="text" class="form-textbox validate[required]" id="input_9" name="q9_fullName" size="28" />
        </div>
      </li>
      <li class="form-line" id="id_4">
        <label class="form-label-top" id="label_4" for="input_4"> E-mail </label>
        <div id="cid_4" class="form-input-wide">
          <input type="email" class="form-textbox validate[Email]" id="input_4" name="q4_email" size="28" />
        </div>
      </li>
      <li class="form-line" id="id_5">
        <label class="form-label-top" id="label_5" for="input_5">
          Phone Number<span class="form-required">*</span>
        </label>
        <div id="cid_5" class="form-input-wide"><span class="form-sub-label-container"><input class="form-textbox validate[required]" type="tel" name="q5_phoneNumber5[area]" id="input_5_area" size="3">
            -
            <label class="form-sub-label" for="input_5_area" id="sublabel_area"> Area Code </label></span><span class="form-sub-label-container"><input class="form-textbox validate[required]" type="tel" name="q5_phoneNumber5[phone]" id="input_5_phone" size="8">
            <label class="form-sub-label" for="input_5_phone" id="sublabel_phone"> Phone Number </label></span>
        </div>
      </li>
      <li class="form-line" id="id_8">
        <label class="form-label-top" id="label_8" for="input_8">
          Graduated<span class="form-required">*</span>
        </label>
        <div id="cid_8" class="form-input-wide">
          <select class="form-dropdown validate[required]" style="width:170px" id="input_8" name="q8_graduated">
            <option>  </option>
            <option value="High School / GED"> High School / GED </option>
            <option value="College Degree"> College Degree </option>
            <option value="None of the above"> None of the above </option>
          </select>
        </div>
      </li>
      <li class="form-line" id="id_6">
        <label class="form-label-top" id="label_6" for="input_6">
          Campus of interest<span class="form-required">*</span>
        </label>
        <div id="cid_6" class="form-input-wide">
          <select class="form-dropdown validate[required]" style="width:170px" id="input_6" name="q6_campusOf">
            <option>  </option>
            <option value="Amarillo"> Amarillo </option>
            <option value="Beaumont"> Beaumont </option>
            <option value="El Paso"> El Paso </option>
            <option value="Las Cruces"> Las Cruces </option>
            <option value="Lubbock"> Lubbock </option>
            <option value="Longview"> Longview </option>
            <option value="Online"> Online </option>
          </select>
        </div>
      </li>
      <li class="form-line" id="id_7">
        <label class="form-label-top" id="label_7" for="input_7">
          How did you hear about us?<span class="form-required">*</span>
        </label>
        <div id="cid_7" class="form-input-wide">
          <select class="form-dropdown validate[required]" style="width:170px" id="input_7" name="q7_howDid">
            <option>  </option>
            <option value="Internet"> Internet </option>
            <option value="Facebook"> Facebook </option>
            <option value="TV Ads"> TV Ads </option>
            <option value="Newspaper"> Newspaper </option>
            <option value="Billboard"> Billboard </option>
            <option value="Radio"> Radio </option>
            <option value="Friend"> Friend </option>
          </select>
        </div>
      </li>
      <li class="form-line" id="id_2">
        <div id="cid_2" class="form-input-wide">
          <div style="text-align:center" class="form-buttons-wrapper">
            <button id="input_2" type="submit" class="form-submit-button form-submit-button-img" style="padding-top:5px;">
              <img src="http://www.vistacollege.edu/wp-content/themes/vista/images/submit_btn.png" alt="Submit" />
            </button>
          </div>
        </div>
      </li>
      <li class="form-line" id="id_11">
        <div id="cid_11" class="form-input-wide">
          <div id="text_11" class="form-html">
            <p>
              We
              <a title="Vista College Privacy Statement" href="http://www.vistacollege.edu/privacy-statement/" target="_blank">Respect Your Privacy</a>
            </p>
          </div>
        </div>
      </li>
      <li style="display:none">
        Should be Empty:
        <input type="text" name="website" value="" />
      </li>
    </ul>
  </div>
  <input type="hidden" id="simple_spc" name="simple_spc" value="20647577549972" />
  <script type="text/javascript">
  document.getElementById("si" + "mple" + "_spc").value = "20647577549972-20647577549972";
  </script>
  <input type="hidden" class="form-hidden" value="<?php echo $_SESSION['source'] ?>" id="input_12" name="q12_source" />
</form>

</body>
</html>