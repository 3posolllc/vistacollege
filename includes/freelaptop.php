<div class="laptopBox">
<img style="float: left;" src="http://www.vistacollege.edu/images/graphics/getlaptop.png" alt="Laptop Included" />

Enroll in online degree programs and receive a laptop!
<em>*Optional laptop purchase, laptop models will vary. If laptop is purchased through Vista College, students will receive laptop upon successful completion of first five week class.  Ownership of laptop will be transferred to student upon graduation.</em>
</div>