<?php
		
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
  function sanitize($var)
	{
			$var = stripslashes($var);
			$var = strip_tags($var);
			$var = htmlentities($var);
			return $var;
	}
	/* Get all Post vars */
	$campus=sanitize($_POST['campus']);
	$email=sanitize($_POST['email']);
	$phone=sanitize($_POST['phone']);
	$campustype=sanitize($_POST['campustype']);
	$fname=sanitize($_POST['fname']);
	$lname=sanitize($_POST['lname']);
	$prevedu=sanitize($_POST['prevedu']);
	$program=sanitize($_POST['program']);
	$source=sanitize($_POST['source']);	
	$LeadType="VWEB";
	
	
	$phone = preg_replace('/\D/', '', $phone);
	$phone = substr($phone,-10);
	$PhoneAreacode = substr($phone,0,3);
	$PhoneLast = substr($phone,-7);
	$phone = $PhoneAreacode."-".$PhoneLast;

	if (strpos($program, 'hvac') !== false) { // for Trades Management - HVAC AAS
			$ProgramInterest="HVAC";
	}
	if (strpos($program, 'Security') !== false) {  // for AAS Information Systems Security & Assurance Online
			$ProgramInterest="ISA";
	}
    switch ($program)
		{
			case "Information Technology Diploma":
				$ProgramInterest="INFT";
				break;
			case "Information Technology Associate of Applied Science":
				$ProgramInterest="INFT";
				break;
			case "Business Administration Diploma":
				$ProgramInterest="BUS";
				break;
			case "Dental Assistant Diploma":
				$ProgramInterest="DA";
				break;
			case "Medical Lab Technician Associate of Applied Science":
				$ProgramInterest="MLT";
				break;
			case "Vocational/Practical Nurse Diploma":
				$ProgramInterest="PN";
				break;
			case "Medical Assisting Diploma":
				$ProgramInterest="MAS";
				break;
			case "Medical Assisting Associate of Applied Science":
				$ProgramInterest="MAS";
				break;
			case "Medical Insurance Billing and Coding Diploma Program":
				$ProgramInterest="MBC";
				break;
			case "Medical Insurance Billing and Coding Associate of Applied Science":
				$ProgramInterest="MBC";
				break;
			case "Veterinary Technology Associate of Applied Science":
				$ProgramInterest="VET";
				break;
			case "Business Management Associate of Applied Science":
				$ProgramInterest="BUS";
				break;
			case "Business Management Accounting Associate of Applied Science":
				$ProgramInterest="BMACCT";
				break;
			case "Criminal Justice Associate of Applied Science":
				$ProgramInterest="CJ";
				break;
			case "Paralegal Associate of Applied Science":
				$ProgramInterest="PL";
				break;
			case "Construction Management Associate Degree":
				$ProgramInterest="CTD";
				break;
			case "Construction Technology Diploma":
				$ProgramInterest="CTD";
				break;
			case "Industrial Maintenance Mechanic Diploma":
				$ProgramInterest="IMM";
			case "Cosmetology Certificate of Proficiency":
				$ProgramInterest="COS";
				break;
			case "Electrical Technician Diploma":
				$ProgramInterest="ETD";
				break;
			case "Heating, Ventilation and Air Conditioning":
				$ProgramInterest="HVAC";
				break;
			case "AAS Supply Chain Management Online":
				$ProgramInterest="SCMA";
				break;
			case "BS Healthcare Administration Online":
				$ProgramInterest="BHCA";
				break;
			case "BS Project Management Online":
				$ProgramInterest="BSBM";
				break;
			case "Medical Insurance Billing and Coding Online":
				$ProgramInterest="MBC";
				break;
			case "Business Administration Online":
				$ProgramInterest="BUS";
				break;
			case "BS Business Administration Online":
				$ProgramInterest="BSBM";
				break;
			case "Information Technology Online":
				$ProgramInterest="INFT";
				break;
			case "AAS Criminal Justice Online":
				$ProgramInterest="CJ";
				break;
			default:
				$ProgramInterest="NOTLISTED";
	}
	
	if (strpos($campus, 'Amarillo') !== false) { // for Trades Management - HVAC AAS
		$campuscode="AMARILLO";
	} elseif (strpos($campus, 'Beaumont') !== false) { // for Trades Management - HVAC AAS
		$campuscode="BEAUMONT";
	} elseif (strpos($campus, 'Station') !== false) { // for Trades Management - HVAC AAS
		$campuscode="COLSTAT";
	} elseif (strpos($campus, 'Paso') !== false) { // for Trades Management - HVAC AAS
		$campuscode="ELPASO";
	} elseif (strpos($campus, 'Smith') !== false) { // for Trades Management - HVAC AAS
		$campuscode="FTSMITH";
	} elseif (strpos($campus, 'Cruces') !== false) { // for Trades Management - HVAC AAS
		$campuscode="LASCRUCE";
	} elseif (strpos($campus, 'Lubbock') !== false) { // for Trades Management - HVAC AAS
		$campuscode="LUBBOCK";
	} elseif (strpos($campus, 'Longview') !== false) { // for Trades Management - HVAC AAS
		$campuscode="LONGVIEW";
	} elseif (strpos($campus, 'Online') !== false) { // for Trades Management - HVAC AAS
		$campuscode="ONLINE";
	} elseif (strpos($campus, 'Killeen') !== false) { // for Trades Management - HVAC AAS
		$campuscode="KILLEEN";
	}

	switch($source){
		case "Internet":
			$leadsource="INT";
			break;
		case "Facebook":
			$leadsource="FB";
			break;
		
		case "TV Ads":
			$leadsource="TV";
			break;
		case "Newspaper":
			$leadsource="NP";
			break;
		
		case "Billboard":
			$leadsource="BLBRD";
			break;
		case "Radio":
			$leadsource="RADIO";
			break;
		case "Friend":
			$leadsource="REFERRAL";
			break;
	}
	
	switch($prevedu){
		case "High School / GED":
			$previousedu="HS";
			break;
		case "College Degree":
			$previousedu="COL-HS";
			break;
		
		case "None of the above":
			$previousedu="NONHSGRA";
			break;
	}
	
	$url="https://api1.vistacollege.edu/cmc.integration.leadimport.httppost/importleadprocessor.aspx";
	$dataStr="?Format=Jotform&LeadType=".$LeadType."&FirstName=".$fname."&LastName=".$lname."&Email=".$email."&Phone=".$phone."&Campus=".$campuscode."&LeadSource=".$leadsource."&PreviousEducation=".$previousedu."&Program=".$ProgramInterest;
	$dataStr=str_replace(" ", "%20", $dataStr);

	$ch = curl_init();
    // Set query data here with the URL
    curl_setopt($ch, CURLOPT_URL, $url . $dataStr);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, '3600');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $xmlResponse = trim(curl_exec($ch));
    curl_close($ch); 
		
	mysql_connect("localhost", "vclog", "ja27&#HW!") or die(mysql_error());
	mysql_select_db("eleads") or die(mysql_error());
	$myquery="INSERT INTO eleads (LeadType, FirstName, LastName, Email,Ph_No,Campus,LeadSource,PreviousEducation,Response,date,dataSTR) VALUES('$LeadType', '$FirstName','$LastName','$Email','$Ph_No','$Campus','$LeadSource','$PreviousEducation','$xmlResponse',now(),'$dataStr') ";
	
	echo $myquery . "<BR><BR>";
	mysql_query("$myquery") or die(mysql_error());  
	
/*    $from = "suyadi@gmail.com";
    $to = "suyadi@gmail.com";
    $subject = "cvue result";
    $headers = "From:" . $from;
	$msg=$msg . "\n\n $dataStr";
    mail($to,$subject,$msg, $headers);
*/
?>
