<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title></title>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<body>
<style>
ul.degreelist {
    color: #da9537;
}
ul.degreelist li {
    line-height: 26px;
}
ul.degreelist li span{
    font-size: 16px;
	font-family: 'Roboto', sans-serif;
	font-weight: 300;
    line-height: 26px;   
	color:#000000;
}
ul.degreelist li{
    list-style-image: url(https://www.vistacollege.edu/wp-content/themes/vista-college/img/list.png)!important;
    margin-left: 10px!important;
}
</style>
<?php
header('Access-Control-Allow-Origin: *');
include_once (__DIR__ . '/simple_html_dom.php');
    $programsOffered = array();
    $url = "https://www.vistacollege.edu/degree-programs/business/";
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_REFERER, $curl_url);
    $html = curl_exec($ch);
    curl_close($ch);
    $html = \simplehtmldom_1_5\str_get_html($html);
    $programsDom = $html->find('.availability-boxes .availability-box ul li a text');
    if (sizeof($programsDom)>0) {
		echo "<ul class='degreelist'>";
        foreach ($programsDom as $dom) {
			echo "<li><span>" . $dom->_[4] . "</span></li>";
            array_push($programsOffered, $dom->_[4]);
        }
		echo "</ul>";
    }
?>
</body>
</html>