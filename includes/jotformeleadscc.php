
<?php

	//Strips all slashes in an array
	function stripslashes_deep($value){
		$value = is_array($value) ?
					array_map('stripslashes_deep', $value) :
					stripslashes($value);
		return $value;
	}
	$result = stripslashes_deep($_REQUEST['rawRequest']);
	
	$obj = json_decode($result, true);
	$FirstName =  $obj['q3_fullName3']['first'];
	$LastName =  $obj['q3_fullName3']['last'];
	$Email =  $obj['q4_email4'];
	$Ph_No =  $obj['q5_phoneNumber5']['area'] ."-".  $obj['q5_phoneNumber5']['phone'];
	$Address =  $obj['q6_address6']['addr_line1'] .". ". $obj['q6_address6']['addr_line2'];
	$City =  $obj['q6_address6']['city'];
	$State =  $obj['q6_address6']['state'];
	$Zip =  $obj['q6_address6']['postal'];
	$PreviousEducation = $obj['q7_whatIs'];
    $ProgramInterest = $obj['q9_whichProgram'];
   
		$program=$ProgramInterest;
		include("programcodes.php");
    $previousEducationArr=array();
    $previousEducationArr['High School / GED']='HS';
    $previousEducationArr['College Degree']='COL-HS';
    $previousEducationArr['None of the above']='NONHSGRA';
    $PreviousEducation=$previousEducationArr[$PreviousEducation];
	
	
	$LeadType="EMAIL";
   
		//$url="https://api5061.campusnet.net/Cmc.Integration.LeadImport.HttpPost/ImportLeadProcessor.aspx";
		$url="https://api1.vistacollege.edu/cmc.integration.leadimport.httppost/importleadprocessor.aspx";
		$dataStr="?Format=Jotform&LeadType=".$LeadType."&FirstName=".$FirstName."&LastName=".$LastName."&Email=".$Email."&Phone=".$Ph_No."&Address1=".$Address."&City=".$City."&State=".$State."&PostalCodeOrZip=".$Zip."&Campus=ONLINE&LeadSource=TVNATIONAL&PreviousEducation=".$PreviousEducation."&Program=".$ProgramInterest;
		
	$dataStr=str_replace(" ", "%20", $dataStr);
    
    $ch = curl_init();
    // Set query data here with the URL
    curl_setopt($ch, CURLOPT_URL, $url . $dataStr);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, '3600');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $xmlResponse = trim(curl_exec($ch));
    curl_close($ch);
			
?>
