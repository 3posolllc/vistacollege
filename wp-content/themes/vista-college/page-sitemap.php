<?php /* Template name: Sitemap */ ?> 


<?php get_header(); ?>


<div class="section">

    <div class="content group">
    
    
        <div class="grid grid-4">
                    
            <div class="grid-content">
            
                <div class="grid-box">
                
                    <h3>Programs</h3>
        
                    <?php $programs_loop = new WP_Query( array( 'post_type' => 'programs', 'posts_per_page' => -1 ) ); if ( $programs_loop->have_posts() ) : ?>

                        <ul>

                            <?php while ( $programs_loop->have_posts() ) : $programs_loop->the_post(); ?>
                            
                                <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                            <?php endwhile; ?>
                            
                        </ul>
                        
                    <?php endif; ?>
                    
                </div>
                
                <div class="grid-box">
                
                    <h3>Campuses</h3>
        
                    <?php $campus_loop = new WP_Query( array( 'post_type' => 'campus', 'posts_per_page' => -1 ) ); if ( $campus_loop->have_posts() ) : ?>

                        <ul>

                            <?php while ( $campus_loop->have_posts() ) : $campus_loop->the_post(); ?>
                            
                                <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                            <?php endwhile; ?>
                            
                        </ul>
                        
                    <?php wp_reset_postdata(); endif; ?>
                    
                </div>
                
                <div class="grid-box">
                
                    <h3>Pages</h3>
        
                    <ul><?php wp_list_pages( array( 'title_li' => '' ) ); ?></ul>
                    
                </div>

                <div class="grid-box">
                
                    <h3>Blog</h3>
            
                    <ul>
                    
                        <?php query_posts('post_type=post&posts_per_page=-1'); ?>
                        
                        <?php if(have_posts()) : ?>
                        
                            <?php while (have_posts()) : the_post(); ?>

                                <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                            <?php endwhile; ?>
                            
                        <?php endif; ?>
                        
                    </ul>
                    
                </div>

               
            </div>
            
        </div>
        
    </div>
    
</div>


<?php get_footer(); ?>