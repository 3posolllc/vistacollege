<?php /* Template name: Request */ ?>


<?php get_header(); ?>


<div class="section section-7">

    <div class="content group">
    
        
        <div class="request-content" id="request-page">
            
            <ul class="request-progress group">
                <li class="request-back active-progress">Step 1 <span>Your Interests</span></li>
                <li>Step 2 <span>Your Information</span></li>
            </ul>
            
            <div class="request-form-full">
            
                <?php echo do_shortcode("[gravityform id='2' title='false' description='false' ajax='false']"); ?>
            
                <p class="request-info">
                    We <a href="<?php echo site_url(); ?>/privacy-statement/">Respect Your Privacy</a>
                </p>
                
            </div>
            
            <p>By submitting this form, I agree that Vista College may use this information to contact me by methods I provided and consented to, including phone (mobile and home, dialed manually or automatically), social media, email, mail and text message.</p>

        </div>    
        

    </div>
    
</div>


<?php if(isset($_POST['input_8'])): ?>

    <script>
        
        jQuery(document).ready(function(){
        
            var campus = "<?php echo $_POST['input_8']; ?>";
            
            if (campus == "Ground Campus"){
                jQuery("#choice_2_8_0").prop("checked", true);
                jQuery("#choice_2_8_1").prop("checked", false);
            }
            
            if (campus == "Online Campus"){
                jQuery("#choice_2_8_1").prop("checked", true);
                jQuery("#choice_2_8_0").prop("checked", false);
            }
        
            jQuery("#input_2_10").val("<?php echo urldecode($_POST['input_10']); ?>");
            jQuery("#input_2_12").val("<?php echo urldecode($_POST['input_12']); ?>");
            jQuery("#input_2_24").val("<?php echo $_POST['input_24']; ?>");
        
            jQuery('#part-1').css("display","none");
            jQuery('#part-2, #gform_wrapper_2 .gform_footer').css("display","block");
            jQuery('.request-progress li:first-child').removeClass("active-progress");
            jQuery('.request-progress li:last-child').addClass("active-progress");
            
        });
    
    </script>
    
<?php endif; ?>


<?php get_footer(); ?>