<?php get_header(); ?>

<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
 
<div class="section section-7 single-campus">

    <div class="content group">

        <aside class="left-side">
        
            <ul class="page-navigation vertical group">
                
                <li class="tab-btn-99 <?php if(!isset($x)){echo'active';} ?>">
                    <a onclick="tab(99, true);">Programs Offered</a>
                </li>
                <?php if( have_rows('ta_tabs'.$GLOBALS['device']) ): ?>
            
                    <?php $x=1; while( have_rows('ta_tabs'.$GLOBALS['device']) ): the_row(); ?>
                        
                        <?php if(get_sub_field('ta_title'.$GLOBALS['device'])): ?>
                            <li class="tab-btn-<?php echo $x; ?>"><a onclick="tab(<?php echo $x; ?>, true);"><?php the_sub_field('ta_title'.$GLOBALS['device']); ?></a></li>
                        <?php endif; ?>
                    
                    <?php $x++; endwhile; ?>
                    
                <?php endif; ?>
                
                
            </ul>
            
        </aside>

        <div class="center-content">
        
            <?php if( have_rows('ta_tabs'.$GLOBALS['device']) ): ?>
            
                <?php $x=1; while( have_rows('ta_tabs'.$GLOBALS['device']) ): the_row(); ?>
                
                    <ul class="page-navigation vertical mobile-tabs group">
                        <?php if(get_sub_field('ta_title'.$GLOBALS['device'])): ?>
                            <li class="tab-btn-<?php echo $x; ?> <?php if($x==1){echo'active';} ?>"><a onclick="tab(<?php echo $x; ?>, true);"><?php the_sub_field('ta_title'.$GLOBALS['device']); ?></a></li>
                        <?php endif; ?>
                    </ul>
                
                    <div class="tab" id="tab-<?php echo $x; ?>">
                                       
                        <?php if($x == 1): ?>
                            
                            <?php if( get_field('ci_enable'.$GLOBALS['device']) ): ?>
        
                                <div class="detail-box">
                                
                                    <div class="campus-details">
                                        
                                        <div class="campus-right google-map">
                                        
                                            <?php if( get_field('ci_map'.$GLOBALS['device']) ): ?>
                                                <?php the_field('ci_map'.$GLOBALS['device']); ?>
                                            <?php endif; ?>
                                        
                                        </div>
                                        
                                        <div class="campus-left">
                                        
                                            <div class="campus-center">
                                                
                                                <?php if( get_field('ci_phone'.$GLOBALS['device']) && get_field('ci_phone_link'.$GLOBALS['device']) ): ?>
                                                    <a class="campus-phone" href="tel:<?php the_field('ci_phone_link'.$GLOBALS['device']); ?>"><?php the_field('ci_phone'.$GLOBALS['device']); ?></a>
                                                <?php endif; ?>
                                                
                                                <?php if( get_field('ci_heading'.$GLOBALS['device']) ): ?>
                                                    <h3><?php the_field('ci_heading'.$GLOBALS['device']); ?></h3>
                                                <?php else: ?>
                                                    <h3><?php echo the_title(); ?></h3>
                                                <?php endif; ?>
                                                
                                                <?php if( get_field('ci_address'.$GLOBALS['device']) && get_field('ci_map_link'.$GLOBALS['device']) ): ?>
                                                    <a href="<?php the_field('ci_map_link'.$GLOBALS['device']); ?>"><?php the_field('ci_address'.$GLOBALS['device']); ?></a>
                                                <?php endif; ?>
                                                
                                            </div>
                                        
                                        </div>
                                    
                                    </div>
                                
                                </div>
                                
                            <?php endif; ?>
                            
                        <?php endif; ?>
                        
                    
                        <?php if( have_rows('ta_sections'.$GLOBALS['device']) ): ?>
                        
                            <?php while( have_rows('ta_sections'.$GLOBALS['device']) ): the_row(); ?>
                        
                                <?php if(get_sub_field('ta_description'.$GLOBALS['device'])): ?>
                            
                                    <div class="detail-box">
                                            
                                        <?php the_sub_field('ta_description'.$GLOBALS['device']); ?>
                                        
                                    </div>
                                    
                                <?php endif; ?>
                                
                            <?php endwhile; ?>
                        
                        <?php endif; ?>
                        
                    </div>
                
                <?php $x++; endwhile; ?>
            
            <?php endif; ?>
            <?php 
                switch ($post->ID) {
                    case 26:
                        $campuse_disclosure = "Vista College Online Campus is not accepting enrollments in the Project Management Bachelor of Science and the Medical Insurance Billing and Coding Diploma programs at this time.";
                        break;
                    case 111:
                        $campuse_disclosure = "Vista College Lubbock – Frankford is no longer accepting enrollments in the HVAC Diploma program.";
                        break;                    
                    case 106:
                        $campuse_disclosure = "Vista College El Paso Campus is not accepting enrollments in the Medical Assisting Associate of Applied Science, Medical Insurance Billing and Coding Associate of Applied Science, and Information Systems Security & Assurance Associate of Applied Science programs at this time.";
                        break;                    
                    case 104:
                        $campuse_disclosure = "Vista College Fort Smith Campus is not accepting enrollments in the Medical Assisting Associate of Applied Science, Medical Insurance Billing and Coding Associate of Applied Science, Business Administration Diploma, and Information Technology Associate of Applied Science programs at this time.";
                        break;                    
                    case 105:
                        $campuse_disclosure = "Vista College Killeen Campus is not accepting enrollments in the Cosmetology Instructor Program at this time.";
                        break;                    
                    default:
                        $campuse_disclosure = "";
                        break;
                }
            ?>
            <?php if(get_field('av_programs')): ?>
            
                <ul class="page-navigation vertical mobile-tabs group">
                    <li class="tab-btn-99 <?php if(!isset($x)){echo'active';} ?>">
                        <a onclick="tab(99, true);">Programs Offered</a>
                    </li>
                </ul>
            
                <div class="tab" id="tab-99" <?php if(!isset($x)): ?>style="display:block;"<?php endif; ?>>
            
                    <div class="program-availability">
                    
                        <h2 class="section-title">Programs <b>Offered</b></h2>
                        
                        <div class="availability-boxes group">
                        
                            <div class="availability-box no-border">

                                <?php $ids = get_field('av_programs'); ?>
                                
                                <?php $program_loop = new WP_Query( array( 'post_type' => 'programs', 'post__in' => $ids, 'posts_per_page' => -1 ) ); 
                                if($program_loop->have_posts()) : ?>

                                    <ul>
                            
                                        <?php while ( $program_loop->have_posts() ) : $program_loop->the_post(); ?>
                                        
                                            <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                        
                                        <?php endwhile; ?>
                                    </ul>
                                    <?php if ($campuse_disclosure):?>
                                        <p><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                                            <?php echo $campuse_disclosure;?></p>
                                    <?php endif;?>
                                
                                <?php wp_reset_postdata(); endif; ?>

                            </div>
                        
                        </div>
                        
                        <?php if (get_the_ID() == 26): ?>
                        
                            <div class="availability-footer group">

                                <p class="full-width">Vista College Online Campus is accredited by <b>ACCSC</b></p>
                            
                            </div>
                        
                        <?php else: ?>
                        
                            <div class="availability-footer group">

                                <p class="full-width">Vista College ground campuses are accredited by <b>COE</b></p>
                            
                            </div>
                        
                        <?php endif; ?>
                        
                    </div>
                    
                </div>
            
            <?php endif; ?>
        
        </div>
        
        <aside class="right-side">
        
            <div class="right-side-testimonials">
        
                <?php if( have_rows('ft_testimonials'.$GLOBALS['device']) ): ?>
                
                    <?php while( have_rows('ft_testimonials'.$GLOBALS['device']) ): the_row(); ?>
            
                        <?php if( get_sub_field('ft_testimonial'.$GLOBALS['device']) ): ?>
                            
                            <?php $post = get_sub_field('ft_testimonial'.$GLOBALS['device']); setup_postdata($post); ?>
                            
                                <div class="featured-testimonial">
                                
                                    <div class="testimonial" <?php if( get_field('te_image_fe') ): ?>style="background-image: url(<?php the_field('te_image_fe'); ?>);"<?php endif; ?>>
                                        
                                        <?php if( get_field('te_fe_video') ): ?>
                                            
                                            <input type="hidden" class="video-iframe" value='<?php the_field('te_fe_video') ?>'/>
                                            
                                            <a class="player-btn fa fa-play-circle" aria-hidden="true"></a>
                                        
                                        <?php endif; ?>
                                        
                                        <a href="<?php the_permalink(144); ?>" class="testimonials-link"><i class="fa fa-angle-right"></i>View more Stories</a>
                                    
                                        <div>
                                    
                                            <?php if( get_field('te_testimonial_fe') ): ?>
                                                <p>"<?php the_field('te_testimonial_fe'); ?>"</p>
                                            <?php endif; ?>
                                            
                                            <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                                <h4><?php the_field('te_name'); ?><br/> <?php the_field('te_campus'); ?></h4>
                                            <?php endif; ?>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                </div>

                            <?php wp_reset_postdata(); ?>
                            
                        <?php endif; ?>
                        
                    <?php endwhile; ?>
                
                <?php else: ?>
                
                    <?php $tag = sanitize_title(get_the_title()); ?>
                
                    <?php query_posts("post_type=testimonials&tag=$tag&posts_per_page=5"); ?>
                
                        <?php if(have_posts()) : ?>
                        
                            <?php while (have_posts()) : the_post(); ?>
                            
                                <div class="featured-testimonial">
                                
                                    <div class="testimonial" <?php if( get_field('te_image_fe') ): ?>style="background-image: url(<?php the_field('te_image_fe'); ?>);"<?php endif; ?>>
                                        
                                        <?php if( get_field('te_fe_video') ): ?>
                                            
                                            <input type="hidden" class="video-iframe" value='<?php the_field('te_fe_video') ?>'/>
                                            
                                            <a class="player-btn fa fa-play-circle" aria-hidden="true"></a>
                                        
                                        <?php endif; ?>
                                        
                                        <a href="<?php the_permalink(144); ?>" class="testimonials-link"><i class="fa fa-angle-right"></i>View more Stories</a>
                                    
                                        <div>
                                    
                                            <?php if( get_field('te_testimonial_fe') ): ?>
                                                <p>"<?php the_field('te_testimonial_fe'); ?>"</p>
                                            <?php endif; ?>
                                            
                                            <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                                <h4><?php the_field('te_name'); ?><br/> <?php the_field('te_campus'); ?></h4>
                                            <?php endif; ?>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            
                            <?php endwhile; ?>
                        
                        <?php endif; ?>
                                    
                    <?php wp_reset_query(); ?>
                
                <?php endif; ?>
                
            </div>
        
        </aside>
        
    </div>
    
</div>

<?php if( have_rows('fc_content', 'option') ): ?>
  <div class="gallerywrap" style="<?php if((get_the_ID() == 14)||(get_the_ID()==26)){echo 'display: none';}else{/**/} ?>">
    <div class="content group "> 
     <h2 class="section-title">What's <b> HAPPENING </b> On Campus </h2>

     <?php
        $instagram_id = '';
        $instagram_url = '#';
        switch (get_the_ID()) {
            case '7':   //Beaumont, TX
                $instagram_id = 1;
                $instagram_url = 'https://www.instagram.com/vistacollege_beaumont/';
                break;
            case '103':   //College Station, TX
                $instagram_id = 10;
                $instagram_url = 'https://www.instagram.com/vistacollege_collegestation/';
                break;
            case '106':   //El Paso, TX (Main Campus)
                $instagram_id = 3;
                $instagram_url = 'https://www.instagram.com/vistacollege_elpaso/';
                break;
            case '104':   //Fort Smith, AR
                $instagram_id = 4;
                $instagram_url = 'https://www.instagram.com/vistacollege_fortsmith/';
                break;
            case '105':   //Killeen, TX
                $instagram_id = 5;
                $instagram_url = 'https://www.instagram.com/vistacollege_killeen/';
                break;
             case '8':   //Las Cruces, NM
                $instagram_id = 6;
                $instagram_url = 'https://www.instagram.com/vistacollege_lascruces/';
                break;
             case '109':   //Longview, TX
                $instagram_id = 7;
                $instagram_url = 'https://www.instagram.com/vistacollege_longview/';
                break;
             case '110':   //Lubbock, TX
                $instagram_id = 8;
                $instagram_url = 'https://www.instagram.com/vistacollege_lubbock/';
                break;
            case '111':   //Lubbock, TX – Frankford Branch Campus
                $instagram_id = 9;
                $instagram_url = 'https://www.instagram.com/vistacollege_lubbock/';
                break;
            default:
                $instagram_id = '';
                $instagram_url = '#';
                break;

        }
     ?>

    <div class="rights" style="text-align:center">
        <a href="<?php echo $instagram_url; ?>" target="_blank" rel="noreferrer" class="follow-instagram"><i class="fa fa-instagram" aria-hidden="true"></i>Follow Us on Instagram</a>
    </div>

   </div>

  </div>

  <div class="instagram_container">
      <?php
        if ($instagram_id)
            echo do_shortcode( '[grace id="'.$instagram_id.'"]' );
      ?>
  </div>
  <div class="footer-top">
    <div class="footer-left-top">
        <div class="left-side-top">
            <div class="left-side-title">
                <h1>Ready to take the next step?</h1>
                <span>Our dedicated Admission team will be glad to help<span>
            </div>
        </div>
        <div class="left-side-bottom">
            <?php $online_status = get_the_ID() == 26 ?  true : false; ?>
            <input type="hidden" name="online_status" id="online_status" value="<?php echo $online_status;?>">
            <select id="program_select">
                <option value="">Which Program are You Interested in?</option>
                <?php $ids = get_field('av_programs'); ?>
                
                <?php $program_loop = new WP_Query( array( 'post_type' => 'programs', 'post__in' => $ids, 'posts_per_page' => -1 ) ); 
                if($program_loop->have_posts()) : ?>
                    <?php while ( $program_loop->have_posts() ) : $program_loop->the_post(); ?>
                    
                        <option value="<?php the_title(); ?>"><?php the_title(); ?></option>
                    
                    <?php endwhile; ?>
                <?php wp_reset_postdata(); endif; ?>
            </select>
            <a class="get-more-info" onClick="_gaq.push(['_trackEvent', 'RFI', 'Click', 'NextStep']);">GET MORE INFO <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
        </div>
    </div>
    <div class="footer-right-top"></div>
  </div>

<?php endif; ?>   

 <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> 
<?php get_footer(); ?>