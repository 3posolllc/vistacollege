<?php /* Template name: Campuses */ ?>


<?php get_header(); ?>


<div class="section section-7">

    <div class="content group">
    

        <?php if( get_field('ft_enable'.$GLOBALS['device']) ): ?>
    
            <div class="programs-intro group">
            
                <?php if( get_field('ft_description'.$GLOBALS['device']) ): ?>
                    
                    <div class="left-content">

                        <?php the_field('ft_description'.$GLOBALS['device']); ?>
                        
                    </div>
                
                <?php endif; ?>
                
                <?php if( get_field('ft_testimonial'.$GLOBALS['device']) ): ?>
                
                    <?php $post=get_field('ft_testimonial'.$GLOBALS['device']); setup_postdata($post); ?>
                
                    <aside class="right-side">
                    
                        <div class="testimonial" <?php if( get_field('te_image_fe') ): ?>style="background-image: url(<?php  the_field('te_image_fe'); ?>);"<?php endif; ?>>
                            
                            <?php if( get_field('te_fe_video') ): ?>
                                
                                <input type="hidden" class="video-iframe" value='<?php the_field('te_fe_video') ?>'/>
                                
                                <a class="player-btn fa fa-play-circle" aria-hidden="true"></a>
                            
                            <?php endif; ?>
                            
                            <a href="<?php the_permalink(144); ?>" class="testimonials-link"><i class="fa fa-angle-right"></i>View more Stories</a>
                        
                            <div>
                        
                                <?php if( get_field('te_testimonial_fe') ): ?>
                                    <p>"<?php the_field('te_testimonial_fe'); ?>"</p>
                                <?php endif; ?>
                                
                                <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                    <h4><?php the_field('te_name'); ?><br/> <?php the_field('te_campus'); ?></h4>
                                <?php endif; ?>
                                
                            </div>
                            
                        </div>
                    
                    </aside>
                
                <?php wp_reset_postdata(); endif; ?>
            
            </div>
            
        <?php endif; ?>
    
        
        <div class="group">
        
            <div class="left-content full-content">
            
                <ul class="page-navigation group">
                    <li class="tab-btn-1 active"><a onclick="tab(1, false);">Ground Campuses</a></li>
                    <li class="tab-btn-2"><a onclick="tab(2, false);">Online Campus</a></li>
                </ul>
                
                <ul class="page-navigation vertical mobile-tabs group">
                    <li class="tab-btn-1 active"><a onclick="tab(1, false);">Ground Campuses</a></li>
                </ul>
                
                <div class="tab" id="tab-1">
                    
                    <?php if( get_field('ma_enable'.$GLOBALS['device']) ): ?>
                    
                        <?php if( get_field('ma_map'.$GLOBALS['device']) ): ?>
                    
                            <div class="campuses-map google-map">
                            
                                <?php the_field('ma_map'.$GLOBALS['device']); ?>
                            
                            </div>
                            
                        <?php endif; ?>
                    
                    <?php endif; ?>
                
                    <?php $campus_loop = new WP_Query( array( 'post_type' => 'campus', 'post__not_in' => array(26), 'posts_per_page' => -1 ) ); if ( $campus_loop->have_posts() ) : ?>

                        <ul class="dropdown-list dropdown-child grey-box-list">

                            <?php while ( $campus_loop->have_posts() ) : $campus_loop->the_post(); $i++;?>
                            
                                <li>
                                    <a href="<?php the_permalink(); ?>" class="campus<?php echo $i; ?>">
                                        <h4>
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <?php the_title(); ?></h4>
                                        <p><?php the_title(); ?></p>                                        
                                    </a>
                                </li>

                            <?php endwhile; ?>
                            
                        </ul>
                        
                    <?php endif; ?>
                
                </div>
                
                <ul class="page-navigation vertical mobile-tabs group">
                    <li class="tab-btn-2"><a onclick="tab(2, false);">Online Campus</a></li>
                </ul>
                
                <div class="tab" id="tab-2">
                
                    <?php $online_loop = new WP_Query( array( 'p' => 26, 'post_type' => 'campus' ) ); if ( $online_loop->have_posts() ) : ?>
        
                        <ul class="dropdown-list dropdown-child grey-box-list">
                        
                            <?php while ( $online_loop->have_posts() ) : $online_loop->the_post(); ?>
                            
                                <li>
                                    <a href="<?php the_permalink(); ?>" class="onlinebox">
                                        <h4>
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <?php the_title(); ?></h4>
                                        <p>Dallas, TX</p>                                        
                                    </a>
                                </li>
                            
                            <?php endwhile; ?>
                        
                        </ul>
                            
                    <?php wp_reset_postdata(); endif; ?>
                
                </div>
            
            </div>
        
        </div>

    </div>
    
</div>


<?php if( have_rows('fc_content', 'option') ): ?>

    <div class="section section-8" <?php if( get_field('fc_background', 'option') ): ?>style="background-image: url(<?php  the_field('fc_background', 'option'); ?>);"<?php endif; ?>>
        
        <div class="content group">

            <div class="career-areas">
                
                <div class="grid grid-3">
                        
                    <div class="grid-content">
                    
                        <?php while( have_rows('fc_content', 'option') ): the_row(); ?>
                    
                            <div class="grid-box">
                            
                                <div>
                                
                                    <?php if(get_sub_field('fc_heading', 'option')): ?>
                                        <h3 class="section-title"><?php the_sub_field('fc_heading', 'option'); ?></h3>
                                    <?php endif; ?>
                                    
                                    <?php if(get_sub_field('fc_description', 'option')){the_sub_field('fc_description', 'option');} ?>
                                    
                                    <?php if(get_sub_field('fc_type', 'option') == "External"): ?>

                                        <?php if(get_sub_field('fc_title', 'option') && get_sub_field('fc_link', 'option')): ?>
                                        
                                            <a class="link" href="<?php the_sub_field('fc_link', 'option'); ?>" target="_blank"><?php the_sub_field('fc_title', 'option'); ?></a>
                                            
                                        <?php endif; ?>
                                        
                                    <?php else: ?>

                                        <?php if(get_sub_field('fc_page', 'option')): $id=get_sub_field('fc_page', 'option'); ?>

                                            <a class="link" href="<?php echo get_the_permalink($id); ?>">
                                                <?php if(get_sub_field('fc_title_cu', 'option')){ the_sub_field('fc_title_cu', 'option');}else{ echo get_the_title($id);} ?>
                                            </a>
                                    
                                        <?php endif; ?>
                                    
                                    <?php endif; ?>
                                    
                                </div>
                                
                            </div>

                        <?php endwhile; ?>
                    
                    </div>
                    
                </div>
            
            </div>

        </div>
        
    </div>

<?php endif; ?>


<?php get_footer(); ?>