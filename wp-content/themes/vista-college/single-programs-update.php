<?php get_header(); ?>
<style type="text/css">
/* update tabs */
.categories ul.children {
    padding-top: 0px;
    border-top: 0px;
}
.categories ul.children li {
    position: relative;
    display: block;
    padding: 10px 15px;
    background-color: #ffffff;
    border-bottom: 1px solid #e1e1e1;
}
.categories ul.children li a {
    color: #566168;
    padding: 15px 17px 15px 15px;
    display: block;
    margin: -10px -15px;
    cursor: pointer;
    background-image: url(../../../images/right-arrow-gray.png);
    background-repeat: no-repeat;
    background-position: right center;
    background-size: 16px 10px;
    -webkit-transform: translateZ(0);
    transform: translateZ(0);
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    -moz-osx-font-smoothing: grayscale;
    overflow: hidden;
    -webkit-transition-duration: 0.3s;
    transition-duration: 0.3s;
    -webkit-transition-property: color, background-color;
    transition-property: color, background-color;
}
.categories ul.children li a:hover {
    text-decoration: none;
    background-color: #f1f1f1;
    color: #192026;
}
.categories ul.children li:hover {
    border-bottom-color: #192026;
}
.categories ul.children li:first-child {
    border-top: 4px solid #ad1a16;
}
.categories ul.children li.current_page_item {
    z-index: 2;
    color: #333333;
    background-color: #f1f1f1;
    border-color: #e1e1e1;
    border-bottom-color: #e1e1e1;
}
.categories ul.children li:first-child.current_page_item {
    border-top: 4px solid #ad1a16;
}

.page-navigation.vertical {
    padding-top: 0px;
    border-top: 0px;
    list-style: none;
    margin: 15px 0 -12px 0;
    padding-left: 0;
}
.page-navigation.vertical li {
    position: relative;
    display: block;
    padding: 10px 15px;
    background-color: #ffffff;
    border-bottom: 1px solid #e1e1e1;
    margin-bottom: 0px;
    border-radius: 0px;
    border-right: 0px;
    border-top: 0px;
}
.page-navigation.vertical li.active {
    border-top-width: 0px;
    border-color: #e1e1e1 !important;
}
.page-navigation.vertical li a {
    color: #566168;
    padding: 15px 17px 15px 15px;
    display: block;
    margin: -10px -15px;
    cursor: pointer;
    background-image: url(../../../images/right-arrow-gray.png);
    background-repeat: no-repeat;
    background-position: right center;
    background-size: 16px 10px;
    background-color: #fff;
    -webkit-transform: translateZ(0);
    transform: translateZ(0);
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    -moz-osx-font-smoothing: grayscale;
    overflow: hidden;
    -webkit-transition-duration: 0.3s;
    transition-duration: 0.3s;
    -webkit-transition-property: color, background-color;
    transition-property: color, background-color;
}
.page-navigation.vertical li a:hover {
    text-decoration: none;
    background-color: #f1f1f1;
    color: #192026;
}
.page-navigation.vertical li:hover {
    border-bottom-color: #192026 !important;
}
.page-navigation.vertical li:first-child {
    border-top: 4px solid #ad1a16;
}
.page-navigation.vertical li.active a {
    z-index: 2;
    color: #333333;
    background-color: #f1f1f1;
    border-color: #e1e1e1 !important;
    border-bottom-color: #e1e1e1 !important;
}
.page-navigation.vertical li:first-child {
    border-top: 4px solid #ad1a16 !important;
}
/* Testimonials Update */
.single-programs .left-side .page-navigation li:last-child {
    display: none;
}
.single-programs .left-side .featured-testimonial {
    margin-top: 0px;
}
.single-programs .left-side .featured-testimonial .testimonial {
    padding: 34px 0 !important;
    background-color: transparent;
    height: auto;
    background-position: 110px;
}
.single-programs .right-side .right-side-testimonials {
    padding: 30px 20px;
    background: #AB2E2E;
    color: #fff;
}
.single-programs .right-side .program-length {
    padding: 15px;
}
.single-programs .right-side .program-length h4 {
    color: #fff;
    font-size: 20px;
    margin-bottom: 0px;
    line-height: 24px;
    font-weight: 500;
    text-transform: uppercase;
}
.single-programs .right-side .program-length p {
    margin-bottom: 10px;
    font-size: 18px;
}
.single-programs .right-side .request {
    border: 1px solid;
    padding: 10px;
    text-align: center;
}
.single-programs .right-side .request #request_select {
    height: 40px;
    min-width: 30%;
    max-width: 90%;
    padding: 0 6px;
    margin-top: 10px;
    margin-bottom: 10px;
}
.single-programs .right-side .request .request-btn {
    display: inline-block;
    width: 100%;
    color: #fff;
    font-size: 16px !important;
    background: #f68e56;
    margin-left: 0px;
    padding: 15px 0px;
    font-weight: 500;
    border: 0px;
    border-radius: 0px;
    text-transform: uppercase;
    cursor: pointer;
}
.single-programs .right-side .request .request-btn:hover {
    background: #f19a6c;
}
@media (max-width: 1441px) {
    .single-programs .center-content {
        float: left;
        width: calc(100% - 740px);
    }
    .single-programs .right-side {
        float: right;
    }
}
@media (max-width: 1400px) {
    .single-programs .left-side {
        margin-right: 50px;
    }
}
@media (max-width: 1200px) {
    .single-programs .center-content {
        float: left;
        width: calc(100% - 370px);
    }
}

@media (max-width: 1200px) {
    .single-programs .right-side {
        margin-right: 120px;
        margin-top: 50px;
        width: 420px;
    }
}
@media (max-width: 1000px) {
    .single-programs .left-side .featured-testimonial {
        text-align: center;
        width: 420px;
        margin: 0px auto;
    }
    .single-programs .left-side .featured-testimonial .testimonial {
        background-position: 200px;
    }
    .single-programs .center-content {
        width: 100%;
    }
    .single-programs .right-side {
        float: left;
        margin-left: 100px;
    }
}
@media (max-width: 600px) {
    .single-programs .left-side .featured-testimonial {
        text-align: left;
        margin: 20px auto;
        width: 100%;
    }
    .single-programs .left-side .featured-testimonial .testimonial {
        background-size: 160px;
        background-position: 150px;
    }
    .single-programs .left-side .featured-testimonial .testimonial>div {
        max-width: 200px;
    }
    .single-programs .right-side {
        margin-left: 0px;
        margin-right: 0px;
    }    
}
@media (max-width: 400px) {
    .single-programs .left-side .featured-testimonial .testimonial {
        background-position: 110px;
    }
}    
</style>
    <div class="section section-7">

        <div class="content group">
        
        
            <aside class="left-side">
            
                <ul class="page-navigation vertical group">
                
                    <?php if( have_rows('ta_tabs'.$GLOBALS['device']) ): ?>
                
                        <?php $x=1; while( have_rows('ta_tabs'.$GLOBALS['device']) ): the_row(); ?>
                            
                            <?php if(get_sub_field('ta_title'.$GLOBALS['device'])): ?>
                                <li class="tab-btn-<?php echo $x; ?> <?php if($x==1){echo'active';} ?>"><a onclick="tab(<?php echo $x; ?>, true);"><?php the_sub_field('ta_title'.$GLOBALS['device']); ?></a></li>
                            <?php endif; ?>
                        
                        <?php $x++; endwhile; ?>
                        
                    <?php endif; ?>
                    
                    <li class="tab-btn-99 <?php if(!isset($x)){echo'active';} ?>">
                        <a onclick="tab(99, true);">Program Availability</a>
                    </li>
                    
                </ul>
                <?php if( have_rows('ft_testimonials'.$GLOBALS['device']) ): ?>
            
                    <?php while( have_rows('ft_testimonials'.$GLOBALS['device']) ): the_row(); ?>
            
                        <?php if( get_sub_field('ft_testimonial'.$GLOBALS['device']) ): ?>
                            
                            <?php $post = get_sub_field('ft_testimonial'.$GLOBALS['device']); setup_postdata($post); ?>
                            
                                <div class="featured-testimonial">
                                
                                    <div class="testimonial" <?php if( get_field('te_image_fe') ): ?>style="background-image: url(<?php the_field('te_image_fe'); ?>);"<?php endif; ?>>
                                        
                                        <?php if( get_field('te_fe_video') ): ?>
                                            
                                            <input type="hidden" class="video-iframe" value='<?php the_field('te_fe_video') ?>'/>
                                            
                                            <a class="player-btn fa fa-play-circle" aria-hidden="true"></a>
                                        
                                        <?php endif; ?>
                                        
                                        <a href="<?php the_permalink(144); ?>" class="testimonials-link"><i class="fa fa-angle-right"></i>View more Stories</a>
                                    
                                        <div>
                                    
                                            <?php if( get_field('te_testimonial_fe') ): ?>
                                                <p>"<?php the_field('te_testimonial_fe'); ?>"</p>
                                            <?php endif; ?>
                                            
                                            <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                                <h4><?php the_field('te_name'); ?><br/> <?php the_field('te_campus'); ?></h4>
                                            <?php endif; ?>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                </div>

                            <?php wp_reset_postdata(); ?>
                            
                        <?php endif; ?>
                        
                    <?php endwhile; ?>
                    
                <?php else: ?>
            
                    <?php $tag = sanitize_title(get_the_title()); ?>
                
                    <?php query_posts("post_type=testimonials&tag=$tag&posts_per_page=5"); ?>
                
                        <?php if(have_posts()) : ?>
                        
                            <?php while (have_posts()) : the_post(); ?>
                            
                                <div class="featured-testimonial">
                                
                                    <div class="testimonial" <?php if( get_field('te_image_fe') ): ?>style="background-image: url(<?php the_field('te_image_fe'); ?>);"<?php endif; ?>>
                                        
                                        <?php if( get_field('te_fe_video') ): ?>
                                            
                                            <input type="hidden" class="video-iframe" value='<?php the_field('te_fe_video') ?>'/>
                                            
                                            <a class="player-btn fa fa-play-circle" aria-hidden="true"></a>
                                        
                                        <?php endif; ?>
                                        
                                        <a href="<?php the_permalink(144); ?>" class="testimonials-link"><i class="fa fa-angle-right"></i>View more Stories</a>
                                    
                                        <div>
                                    
                                            <?php if( get_field('te_testimonial_fe') ): ?>
                                                <p>"<?php the_field('te_testimonial_fe'); ?>"</p>
                                            <?php endif; ?>
                                            
                                            <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                                <h4><?php the_field('te_name'); ?><br/> <?php the_field('te_campus'); ?></h4>
                                            <?php endif; ?>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            
                            <?php endwhile; ?>
                        
                        <?php endif; ?>
                                    
                    <?php wp_reset_query(); ?>
                    
                <?php endif; ?>
            </aside>
            
        
            <div class="center-content">
            
            
                <?php if( have_rows('ta_tabs'.$GLOBALS['device']) ): ?>
                
                    <?php $x=1; while( have_rows('ta_tabs'.$GLOBALS['device']) ): the_row(); ?>
                    
                        <ul class="page-navigation vertical mobile-tabs group">
                            <?php if(get_sub_field('ta_title'.$GLOBALS['device'])): ?>
                                <li class="tab-btn-<?php echo $x; ?> <?php if($x==1){echo'active';} ?>"><a onclick="tab(<?php echo $x; ?>, true);"><?php the_sub_field('ta_title'.$GLOBALS['device']); ?></a></li>
                            <?php endif; ?>
                        </ul>
                    
                        <div class="tab" id="tab-<?php echo $x; ?>">
                            <?php if( have_rows('ta_sections'.$GLOBALS['device']) ): ?>
                            
                                <?php while( have_rows('ta_sections'.$GLOBALS['device']) ): the_row(); ?>
                            
                                    <?php if(get_sub_field('ta_description'.$GLOBALS['device'])): ?>
                                
                                        <div class="detail-box">
                                                
                                            <?php the_sub_field('ta_description'.$GLOBALS['device']); ?>
                                            
                                        </div>
                                        
                                    <?php endif; ?>
                                    
                                <?php endwhile; ?>
                            
                            <?php endif; ?>
                        
                        </div>
                    
                    <?php $x++; endwhile; ?>
                
                <?php endif; ?>
                
                
                <ul class="page-navigation vertical mobile-tabs group">
                    <li class="tab-btn-99 <?php if(!isset($x)){echo'active';} ?>">
                        <a onclick="tab(99, true);">Program Availability</a>
                    </li>
                </ul>
            
                <div class="tab" id="tab-99" <?php if(!isset($x)): ?>style="display:block;"<?php endif; ?>>
                
                    <div class="program-availability">
                    
                        <h2 class="section-title">Program <b>Availability</b></h2>
                        
                        <?php $online = false; ?>
                        
                        <?php $campuses_loop = new WP_Query( array( 'post_type' => 'campus', 'meta_query' => array(array('key' => 'av_programs','value' => '"' . get_the_ID() . '"','compare' => 'LIKE')) ) ); if ( $campuses_loop->have_posts() ) : ?>
                        
                            <?php while ( $campuses_loop->have_posts() ) : $campuses_loop->the_post(); ?>
                            
                                <?php if (get_the_ID() == 26){ $online = true; } ?>

                            <?php endwhile; ?>
                            
                        <?php wp_reset_postdata(); endif; ?>
                        
                        <div class="availability-boxes group">
                        
                            <div class="availability-box <?php if( !get_field('ao_notification') ){echo "no-border";} ?>">
                                
                                <?php if($online == false): ?>
                                    <h3>Ground Campuses</h3>
                                <?php else: ?>
                                    <h3>Online Campus</h3>
                                <?php endif; ?>
                        
                                <?php $campuses_loop = new WP_Query( array( 'post_type' => 'campus', 'meta_query' => array(array('key' => 'av_programs','value' => '"' . get_the_ID() . '"','compare' => 'LIKE')) ) ); if ( $campuses_loop->have_posts() ) : ?>
                                
                                    <ul>
                                    
                                        <?php while ( $campuses_loop->have_posts() ) : $campuses_loop->the_post(); ?>
                                        
                                            <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                                        <?php endwhile; ?>
                                        
                                    </ul>
                                    
                                <?php wp_reset_postdata(); endif; ?>
                                
                            </div>
                            
                            <?php if( get_field('ao_notification') && $online == false ): ?>
                            
                                <?php $online_loop = new WP_Query( array( 'p' => 26, 'post_type' => 'campus' ) ); if ( $online_loop->have_posts() ) : ?>
                                
                                    <?php while ( $online_loop->have_posts() ) : $online_loop->the_post(); ?>
                            
                                        <div class="availability-box">
                                        
                                            <h3>Program Also Available At</h3>
                                            
                                            <ul>
                                                <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                            </ul>
                                            
                                        </div>
                                        
                                    <?php endwhile; ?>
                                
                                <?php wp_reset_postdata(); endif; ?>
                            
                            <?php endif; ?>
                            
                        </div>
                        
                        <div class="availability-footer group">
                            
                            <?php if( $online == false ): ?>
                                <p <?php if( !get_field('ao_notification') ){echo "class='full-width'";} ?>>Vista College ground campuses are accredited by <b>COE</b></p>
                            <?php endif; ?>
                            
                            <?php if( get_field('ao_notification') || $online == true ): ?>
                                <p <?php if( $online == true ){echo "class='full-width'";} ?>>Vista College Online Campus is accredited by <b>ACCSC</b></p>
                            <?php endif; ?>
                        
                        </div>
                        
                    </div>
                
                </div>
                
            
            </div>
            <?php
                global $post;
                if ($post->ID == 6465 || $post->ID == 116) {
                    $starting_dates = get_field('custom_starting_dates', 'option');
                } else {
                    $starting_dates = get_field('default_starting_dates', 'option');
                }
                if ($starting_dates) {
                    $starting_dates = explode(',', $starting_dates);
                } else {
                    $starting_dates = [];
                }
                $today = date('Y-m-d');
                $starting_date = '';
                foreach ($starting_dates as $value) {
                    $date_obj = explode('-', $value);
                    $date = date('Y-m-d', mktime(0,0,0,$date_obj[0],$date_obj[1],$date_obj[2]));
                    if ($date >= $today) {
                        $starting_date = date('F j', mktime(0,0,0,$date_obj[0],$date_obj[1],$date_obj[2]));
                        break;
                    }
                }
            ?>        
            
            <aside class="right-side">
            
                <div class="right-side-testimonials">
                    <div class="program-length">
                        <h4 style="margin-bottom:20px;"><?php echo the_title();?></h4>
                        <h4>PROGRAM LENGTH</h4>
                        <p><?php echo get_field('program_length', $post->ID);?> Months*</p>
                        <h4>CREDIT HOURS</h4>
                        <p style="margin-bottom:-8px;"><?php echo get_field('credit_hours', $post->ID);?></p>
                        <p style="font-size:14px">Theory/Lab/Quater Credit Hours</p>
                        <h4>NEXT START DATE</h4>
                        <p><?php echo $starting_date;?></p>

                    </div>
                    <div class="request">
                        <select id="request_select">
                            <option value="">Campus of Interest?</option>
                            <?php $campuses_loop = new WP_Query( array( 'post_type' => 'campus', 'meta_query' => array(array('key' => 'av_programs','value' => '"' . get_the_ID() . '"','compare' => 'LIKE')) ) ); if ( $campuses_loop->have_posts() ) : ?>                            
                                <?php while ( $campuses_loop->have_posts() ) : $campuses_loop->the_post(); ?>
                                    <option value="<?php $value = get_the_ID()!=26 ? the_title(): 'Online Campus';echo $value; ?>"><?php the_title(); ?></option>
                                <?php endwhile; ?>

                            <?php wp_reset_postdata(); endif; ?>

                            <?php if( get_field('ao_notification')): ?>
                            
                                <?php $online_loop = new WP_Query( array( 'p' => 26, 'post_type' => 'campus' ) ); if ( $online_loop->have_posts() ) : ?>
                                
                                    <?php while ( $online_loop->have_posts() ) : $online_loop->the_post(); ?>
                                        <option value="Online Campus"><?php the_title(); ?></option>                                
                                    <?php endwhile; ?>
                                
                                <?php wp_reset_postdata(); endif; ?>
                            
                            <?php endif; ?>                    
                        </select>
                        <a class="request-btn" onClick="_gaq.push(['_trackEvent', 'RFI', 'Click', 'NextStep']);">REQUEST INFO</i></a>                        
                    </div>
                        
                </div>
                
                <?php if( get_field('yl_enable'.$GLOBALS['device']) ): ?>
                
                    <?php if( have_rows('yl_learn'.$GLOBALS['device']) ): ?>
                
                        <div class="learn-how-to">
                        
                            <h2>You'll Learn How To</h2>
                            
                            <div>
                            
                                <ul>
                                
                                    <?php while( have_rows('yl_learn'.$GLOBALS['device']) ): the_row(); ?>
                                     
                                        <?php if(get_sub_field('yl_type'.$GLOBALS['device']) == "External"): ?>
                                        
                                            <?php if(get_sub_field('yl_title'.$GLOBALS['device']) && get_sub_field('yl_link'.$GLOBALS['device'])): ?>
                                            
                                                <li>
                                                    <a href="<?php the_sub_field('yl_link'.$GLOBALS['device']); ?>" target="_blank"><?php the_sub_field('yl_title'.$GLOBALS['device']); ?></a>
                                                </li>
                                                
                                            <?php endif; ?>
                                            
                                        <?php else: ?>
                                        
                                            <?php if(get_sub_field('yl_page'.$GLOBALS['device'])): $id=get_sub_field('yl_page'.$GLOBALS['device']); ?>
                                        
                                                <li>
                                                    <a href="<?php echo get_the_permalink($id); ?>"><?php echo get_the_title($id); ?></a>
                                                </li>
                                        
                                            <?php endif; ?>
                                        
                                        <?php endif; ?>
                                    
                                    <?php endwhile; ?>
                                    
                                </ul>
                                
                            </div>
                        
                        </div>
                        
                    <?php endif; ?>
                
                <?php endif; ?>
                
                
                <?php if( get_field('rt_enable'.$GLOBALS['device']) ): ?>
                
                    <?php if( have_rows('rt_topics'.$GLOBALS['device']) ): ?>
                
                        <div class="program-areas relevant-topics">
                        
                            <h2>Relevant and<br/><b>Cutting-edge topics</b></h2>
                            
                            <div>
                            
                                <ul>
                                    
                                    <?php while( have_rows('rt_topics'.$GLOBALS['device']) ): the_row(); ?>
                                        
                                        <?php if(get_sub_field('rt_type'.$GLOBALS['device']) == "External"): ?>
                                        
                                            <?php if(get_sub_field('rt_title'.$GLOBALS['device']) && get_sub_field('rt_link'.$GLOBALS['device'])): ?>
                                            
                                                <li>
                                                    <a href="<?php the_sub_field('rt_link'.$GLOBALS['device']); ?>" target="_blank"><?php the_sub_field('rt_title'.$GLOBALS['device']); ?></a>
                                                </li>
                                                
                                            <?php endif; ?>
                                            
                                        <?php else: ?>
                                        
                                            <?php if(get_sub_field('rt_post'.$GLOBALS['device'])): $id=get_sub_field('rt_post'.$GLOBALS['device']); ?>
                                        
                                                <li>
                                                    <a href="<?php echo get_the_permalink($id); ?>"><?php echo get_the_title($id); ?></a>
                                                </li>
                                        
                                            <?php endif; ?>
                                        
                                        <?php endif; ?>
                                    
                                    <?php endwhile; ?>
                                    
                                </ul>
                                
                            </div>
                        
                        </div>
                        
                    <?php endif; ?>
                    
                <?php endif; ?>
            
            </aside>
            
            
        </div>
        
    </div>

    <?php if( have_rows('fc_content', 'option') ): ?>
        <div class="footer-top">
            <div class="footer-left-top">
                <div class="left-side-top">
                    <div class="left-side-title">
                        <h1>Let's Get Started!</h1>
                        <span>Questions? An Admission Representative would love to help<span>
                    </div>
                </div>
                <div class="left-side-bottom">
                    <input type="hidden" name="online_status" id="online_status" value="<?php echo $online;?>">
                    <select id="campus_select">
                        <option value="">Which Campus are You Interested in?</option>
                        <?php $campuses_loop = new WP_Query( array( 'post_type' => 'campus', 'meta_query' => array(array('key' => 'av_programs','value' => '"' . get_the_ID() . '"','compare' => 'LIKE')) ) ); if ( $campuses_loop->have_posts() ) : ?>                            
                            <?php while ( $campuses_loop->have_posts() ) : $campuses_loop->the_post(); ?>
                                <option value="<?php $value = get_the_ID()!=26 ? the_title(): 'Online Campus';echo $value; ?>"><?php the_title(); ?></option>
                            <?php endwhile; ?>

                        <?php wp_reset_postdata(); endif; ?>

                        <?php if( get_field('ao_notification')): ?>
                        
                            <?php $online_loop = new WP_Query( array( 'p' => 26, 'post_type' => 'campus' ) ); if ( $online_loop->have_posts() ) : ?>
                            
                                <?php while ( $online_loop->have_posts() ) : $online_loop->the_post(); ?>
                                    <option value="Online Campus"><?php the_title(); ?></option>                                
                                <?php endwhile; ?>
                            
                            <?php wp_reset_postdata(); endif; ?>
                        
                        <?php endif; ?>                    
                    </select>
                    <a class="get-more-info" onClick="_gaq.push(['_trackEvent', 'RFI', 'Click', 'NextStep']);">REQUEST INFO <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="footer-right-top"></div>
        </div>

    <?php endif; ?>

<?php get_footer(); ?>