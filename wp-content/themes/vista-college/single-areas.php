<?php get_header(); ?>


<div class="section section-7">

    <div class="content group">
    
    
        <aside class="left-side proofferd">

            <ul class="page-navigation vertical mobile-tabs group">
                <!-- <li class="tab-btn-99 active">
                    <a onclick="tab(99, true);">Programs Offered</a>
                </li> -->
            </ul>
        
            <div class="tab" id="tab-99" style="display:block;">
            
                <?php $online_loop = new WP_Query( array( 'p' => 26, 'post_type' => 'campus' ) ); if ( $online_loop->have_posts() ) : ?>
        
                    <?php while ( $online_loop->have_posts() ) : $online_loop->the_post(); ?>

                        <?php if(get_field('av_programs')): ?>
                            
                            <?php $online_ids = get_field('av_programs'); ?>

                        <?php endif; ?>

                    <?php endwhile; ?>
                        
                <?php endif; wp_reset_postdata(); ?>
        
                <div class="program-availability">
                
                    <h2 class="section-title">Programs <b>Offered</b></h2>
                    
                    <?php $parent = get_the_ID(); ?>
                                
                    <?php $programs_count = new WP_Query( array( 'post_type' => 'programs', 'post_parent' => $parent, 'post__not_in' => $online_ids, 'posts_per_page' => -1 ) ); ?>
                    
                    <?php if($programs_count->post_count > 0): ?>
                    
                        <div class="availability-boxes group">
                        
                            <div class="availability-box no-border">
                            
                                <h3>Ground Campus Programs</h3>
                                
                                <?php $program_loop = new WP_Query( array( 'post_type' => 'programs', 'post_parent' => $parent, 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1 ) ); 
                                if($program_loop->have_posts()) : ?>

                                    <ul>
                            
                                        <?php while ( $program_loop->have_posts() ) : $program_loop->the_post(); ?>
                                        
                                            <?php if (!in_array(get_the_ID(), $online_ids)): ?>
                                                <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                            <?php endif; ?>
                                        
                                        <?php endwhile; ?>
                                
                                    </ul>
                                    <div class="availability-footer group">

                                        <p class="full-width">Vista College ground campuses are accredited by <b>Council on Occupational Education</b></p>
                            
                                    </div>
                                <?php endif; wp_reset_postdata(); ?>

                            </div>
                        
                        </div>
                        
                       
                    
                    <?php endif; ?>
                                
                    <?php $programs_count = new WP_Query( array( 'post_type' => 'programs', 'post_parent' => $parent, 'post__in' => $online_ids, 'posts_per_page' => -1 ) ); ?>
                    
                    <?php if($programs_count->post_count > 0): ?>
                    
                        <div class="availability-boxes availability-area group">
                        
                            <div class="availability-box no-border">
                            
                                <h3>Online Campus Programs</h3>
                                
                                <?php $program_loop = new WP_Query( array( 'post_type' => 'programs', 'post_parent' => $parent, 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1 ) ); 
                                if($program_loop->have_posts()) : ?>

                                    <ul>
                            
                                        <?php while ( $program_loop->have_posts() ) : $program_loop->the_post(); ?>
                                        
                                            <?php if (in_array(get_the_ID(), $online_ids)): ?>
                                                <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                            <?php endif; ?>
                                        
                                        <?php endwhile; ?>
                                
                                    </ul>
                                    <div class="availability-footer group">

                                         <p class="full-width">Vista College online campus is accredited by <b>Accrediting Commission of Career Schools and Colleges</b></p>
                            
                                     </div>
                                <?php endif; wp_reset_postdata(); ?>

                            </div>
                        
                        </div>

                       
                        
                    <?php endif; ?>
                    
                </div>
                
            </div>
            
        </aside>
        
    
        <div class="center-content proarealeft">

            <?php if( have_rows('ta_tabs'.$GLOBALS['device']) ): ?>
            
                <?php $x=1; while( have_rows('ta_tabs'.$GLOBALS['device']) ): the_row(); ?>
                
                    <ul class="page-navigation vertical mobile-tabs group">
                        <?php if(get_sub_field('ta_title'.$GLOBALS['device'])): ?>
                            <!-- <li class="tab-btn-<?php echo $x; ?> <?php if($x==1){echo'active';} ?>"><a onclick="tab(<?php echo $x; ?>, true);"><?php the_sub_field('ta_title'.$GLOBALS['device']); ?></a></li> -->
                        <?php endif; ?>
                    </ul>
                
                    <div class="tab" id="tab-<?php echo $x; ?>">
                    
                        <?php if( have_rows('ta_sections'.$GLOBALS['device']) ): ?>
                        
                            <?php while( have_rows('ta_sections'.$GLOBALS['device']) ): the_row(); ?>
                        
                                <?php if(get_sub_field('ta_description'.$GLOBALS['device'])): ?>
                            
                                    <div class="detail-box">
                                            
                                        <?php the_sub_field('ta_description'.$GLOBALS['device']); ?>
                                        
                                    </div>
                                    
                                <?php endif; ?>
                                
                            <?php endwhile; ?>
                        
                        <?php endif; ?>
                    
                    </div>
                
                <?php $x++; endwhile; ?>
            
            <?php endif; ?>
            
            
          
            
        
        </div>
        
        
 <!--        <aside class="right-side">
        
            <div class="program-areas">
            
                <h2>Program <b>Areas</b></h2>
                
                <div>
                
                    <p>We offer career training that will help you start in a new rewarding career in a matter of months.</p>
        
                    <?php $area_loop = new WP_Query( array( 'post_type' => 'areas', 'posts_per_page' => -1 ) ); if ( $area_loop->have_posts() ) : ?>

                        <ul>

                            <?php while ( $area_loop->have_posts() ) : $area_loop->the_post(); ?>
                            
                                <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                            <?php endwhile; ?>
                            
                        </ul>
                        
                    <?php wp_reset_postdata(); endif; ?>
                    
                </div>
            
            </div>
        
        </aside> -->
        
        
    </div>
    
</div>


<?php if( have_rows('fc_content', 'option') ): ?>

    <div class="section section-8" <?php if( get_field('fc_background', 'option') ): ?>style="background-image: url(<?php  the_field('fc_background', 'option'); ?>);"<?php endif; ?>>
        
        <div class="content group">

            <div class="career-areas">
                
                <div class="grid grid-3">
                        
                    <div class="grid-content">
                    
                        <?php while( have_rows('fc_content', 'option') ): the_row(); ?>
                    
                            <div class="grid-box">
                            
                                <div>
                                
                                    <?php if(get_sub_field('fc_heading', 'option')): ?>
                                        <h3 class="section-title"><?php the_sub_field('fc_heading', 'option'); ?></h3>
                                    <?php endif; ?>
                                    
                                    <?php if(get_sub_field('fc_description', 'option')){the_sub_field('fc_description', 'option');} ?>
                                    
                                    <?php if(get_sub_field('fc_type', 'option') == "External"): ?>

                                        <?php if(get_sub_field('fc_title', 'option') && get_sub_field('fc_link', 'option')): ?>
                                        
                                            <a class="link" href="<?php the_sub_field('fc_link', 'option'); ?>" target="_blank"><?php the_sub_field('fc_title', 'option'); ?></a>
                                            
                                        <?php endif; ?>
                                        
                                    <?php else: ?>

                                        <?php if(get_sub_field('fc_page', 'option')): $id=get_sub_field('fc_page', 'option'); ?>

                                            <a class="link" href="<?php echo get_the_permalink($id); ?>">
                                                <?php if(get_sub_field('fc_title_cu', 'option')){ the_sub_field('fc_title_cu', 'option');}else{ echo get_the_title($id);} ?>
                                            </a>
                                    
                                        <?php endif; ?>
                                    
                                    <?php endif; ?>
                                    
                                </div>
                                
                            </div>

                        <?php endwhile; ?>
                    
                    </div>
                    
                </div>
            
            </div>

        </div>
        
    </div>

<?php endif; ?>


<?php get_footer(); ?>