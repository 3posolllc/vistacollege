<?php 
	get_header(); 
	$pageid = get_the_ID();
?>

<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
<style>
    
#menu-header-1 .sub-menu {
    width:239px!important;
}
</style> 
<div class="section section-7 single-campus">

    <div class="content group">

        <aside class="left-side">
        
            <ul class="page-navigation vertical group">
                
                <li class="tab-btn-99 <?php if(!isset($x)){echo'active';} ?>">
                    <a onclick="tab(99, true);">Programs Offered</a>
                </li>
                <?php if( have_rows('ta_tabs'.$GLOBALS['device']) ): ?>
            
                    <?php $x=1; while( have_rows('ta_tabs'.$GLOBALS['device']) ): the_row(); ?>
                        
                        <?php if(get_sub_field('ta_title'.$GLOBALS['device'])): ?>
                            <li class="tab-btn-<?php echo $x; ?>"><a onclick="tab(<?php echo $x; ?>, true);"><?php the_sub_field('ta_title'.$GLOBALS['device']); ?></a></li>
                        <?php endif; ?>
                    
                    <?php $x++; endwhile; ?>
                    
                <?php endif; ?>
                
                
            </ul>
			
			<?php
			switch ($pageid){
				case 7:
					$fileurl="Beaumont-brochures";
					break;
				case 103:
					$fileurl="CollegeStation-brochures";
					break;
				case 106:
					$fileurl="ElPaso-brochures";
					break;
				case 104:
					$fileurl="FtSmith-brochures";
					break;
				case 105:
					$fileurl="Killeen-brochures";
					break;
				case 8:
					$fileurl="Las Cruces-brochures";
					break;
				case 109:
					$fileurl="Longview-brochures";
					break;
				case 110:
					$fileurl="";
					break;
					
			}
			?>
			<?php if ($fileurl!=''){ ?>
			<a href="https://www.vistacollege.edu/files/onlinebrochure/<?php echo $fileurl; ?>.pdf" target="_blank"><img src="/wp-content/uploads/2020/06/vista-college-brochure-CTA.png"/></a>
			<?php } ?>
            <div class="right-side-testimonials">
        
            <?php if( have_rows('ft_testimonials'.$GLOBALS['device']) ): ?>
            
                <?php while( have_rows('ft_testimonials'.$GLOBALS['device']) ): the_row(); ?>
        
                    <?php if( get_sub_field('ft_testimonial'.$GLOBALS['device']) ): ?>
                        

                        <?php $post = get_sub_field('ft_testimonial'.$GLOBALS['device']); setup_postdata($post); ?>
                        
                            <div class="featured-testimonial">
                            
                                <div class="testimonial" <?php if( get_field('te_image_fe') ): ?>style="background-image: url(<?php the_field('te_image_fe'); ?>);"<?php endif; ?>>
                                    
                                    <?php if( get_field('te_fe_video') ): ?>
                                        
                                        <input type="hidden" class="video-iframe" value='<?php the_field('te_fe_video') ?>'/>
                                        
                                        <a class="player-btn fa fa-play-circle" aria-hidden="true"></a>
                                    
                                    <?php endif; ?>
                                    
                                    <a href="<?php the_permalink(144); ?>" class="testimonials-link"><i class="fa fa-angle-right"></i>View more Stories</a>
                                
                                    <div>
                                
                                        <?php if( get_field('te_testimonial_fe') ): ?>
                                            <p>"<?php the_field('te_testimonial_fe'); ?>"</p>
                                        <?php endif; ?>
                                        
                                        <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                            <h4><?php the_field('te_name'); ?><br/> <?php the_field('te_campus'); ?></h4>
                                        <?php endif; ?>
                                        
                                    </div>
                                    
                                </div>
                                
                            </div>

                        <?php wp_reset_postdata(); ?>
                        
                    <?php endif; ?>
                    
                <?php endwhile; ?>
            
            <?php else: ?>
            
                <?php $tag = sanitize_title(get_the_title()); ?>
            
                <?php query_posts("post_type=testimonials&tag=$tag&posts_per_page=5"); ?>
            
                    <?php if(have_posts()) : ?>
                    
                        <?php while (have_posts()) : the_post(); ?>
                        
                            <div class="featured-testimonial">
                            
                                <div class="testimonial" <?php if( get_field('te_image_fe') ): ?>style="background-image: url(<?php the_field('te_image_fe'); ?>);"<?php endif; ?>>
                                    
                                    <?php if( get_field('te_fe_video') ): ?>
                                        
                                        <input type="hidden" class="video-iframe" value='<?php the_field('te_fe_video') ?>'/>
                                        
                                        <a class="player-btn fa fa-play-circle" aria-hidden="true"></a>
                                    
                                    <?php endif; ?>
                                    
                                    <a href="<?php the_permalink(144); ?>" class="testimonials-link"><i class="fa fa-angle-right"></i>View more Stories</a>
                                
                                    <div>
                                
                                        <?php if( get_field('te_testimonial_fe') ): ?>
                                            <p>"<?php the_field('te_testimonial_fe'); ?>"</p>
                                        <?php endif; ?>
                                        
                                        <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                            <h4><?php the_field('te_name'); ?><br/> <?php the_field('te_campus'); ?></h4>
                                        <?php endif; ?>
                                        
                                    </div>
                                    
                                </div>
                                
                            </div>
                        
                        <?php endwhile; ?>
                    
                    <?php endif; ?>
                                
                <?php wp_reset_query(); ?>
            
            <?php endif; ?>
            
        </div>
                
        </aside>

        <div class="center-content">
        
            <?php if( have_rows('ta_tabs'.$GLOBALS['device']) ): ?>
            
                <?php $x=1; while( have_rows('ta_tabs'.$GLOBALS['device']) ): the_row(); ?>
                
                    <ul class="page-navigation vertical mobile-tabs group">
                        <?php if(get_sub_field('ta_title'.$GLOBALS['device'])): ?>
                            <li class="tab-btn-<?php echo $x; ?> <?php if($x==1){echo'active';} ?>"><a onclick="tab(<?php echo $x; ?>, true);"><?php the_sub_field('ta_title'.$GLOBALS['device']); ?></a></li>
                        <?php endif; ?>
                    </ul>
                
                    <div class="tab" id="tab-<?php echo $x; ?>">
                                       
                        <?php if($x == 1): ?>
                            
                            <?php if( get_field('ci_enable'.$GLOBALS['device']) ): ?>
        
                                <div class="detail-box">
                                
                                    <div class="campus-details">
                                        
                                        <div class="campus-right google-map">
                                        
                                            <?php if( get_field('ci_map'.$GLOBALS['device']) ): ?>
                                                <?php the_field('ci_map'.$GLOBALS['device']); ?>
                                            <?php endif; ?>
                                        
                                        </div>
                                        
                                        <div class="campus-left">
                                        
                                            <div class="campus-center">
                                                
                                                <?php if( get_field('ci_phone'.$GLOBALS['device']) && get_field('ci_phone_link'.$GLOBALS['device']) ): ?>
                                                    <a class="campus-phone" href="tel:<?php the_field('ci_phone_link'.$GLOBALS['device']); ?>"><?php the_field('ci_phone'.$GLOBALS['device']); ?></a>
                                                <?php endif; ?>
                                                
                                                <?php if( get_field('ci_heading'.$GLOBALS['device']) ): ?>
                                                    <h3><?php the_field('ci_heading'.$GLOBALS['device']); ?></h3>
                                                <?php else: ?>
                                                    <h3><?php echo the_title(); ?></h3>
                                                <?php endif; ?>
                                                
                                                <?php if( get_field('ci_address'.$GLOBALS['device']) && get_field('ci_map_link'.$GLOBALS['device']) ): ?>
                                                    <a href="<?php the_field('ci_map_link'.$GLOBALS['device']); ?>"><?php the_field('ci_address'.$GLOBALS['device']); ?></a>
                                                <?php endif; ?>
                                                
                                            </div>
                                        
                                        </div>
                                    
                                    </div>
                                
                                </div>
                                
                            <?php endif; ?>
                            
                        <?php endif; ?>
                        
                    
                        <?php if( have_rows('ta_sections'.$GLOBALS['device']) ): ?>
                        
                            <?php while( have_rows('ta_sections'.$GLOBALS['device']) ): the_row(); ?>
                        
                                <?php if(get_sub_field('ta_description'.$GLOBALS['device'])): ?>
                            
                                    <div class="detail-box">
                                            
                                        <?php the_sub_field('ta_description'.$GLOBALS['device']); ?>
                                        
                                    </div>
                                    
                                <?php endif; ?>
                                
                            <?php endwhile; ?>
                        
                        <?php endif; ?>
                        
                    </div>
                
                <?php $x++; endwhile; ?>
            
            <?php endif; ?>
            <?php 
                switch ($post->ID) {
                    case 26:
                        $campuse_disclosure = 'Vista College Online Campus is not accepting enrollments in the following programs at this time:<ul class="teachoutlink"><li><a href="/degree-programs/healthcare/medical-insurance-billing-coding-associate-of-applied-science-online/">Medical Insurance Billing & Coding Diploma</a></li><li><a href="/degree-programs/healthcare/medical-insurance-billing-coding-diploma-online/">Project Management Bachelor of Science</a></li></ul>';
                        break;
                    case 111:
                        $campuse_disclosure = 'Vista College Lubbock – Frankford is not accepting enrollments in the following programs at this time:<ul class="teachoutlink"><li><a href="/degree-programs/healthcare/medical-insurance-billing-coding-diploma-online/">HVAC Diploma program</a></li></ul>';
                        break;  
                    case 109:
                        $campuse_disclosure = 'Vista College Longview, TX is not accepting enrollments in the following programs at this time:<ul class="teachoutlink"><li><a href="/degree-programs/business/business-administration-diploma/">Business Administration Diploma program</a></li></ul>';
                        break;                    
                    default:
                        $campuse_disclosure = "";
                        break;
                }
            ?>
            <?php if(get_field('av_programs')): ?>
            
                <ul class="page-navigation vertical mobile-tabs group">
                    <li class="tab-btn-99 <?php if(!isset($x)){echo'active';} ?>">
                        <a onclick="tab(99, true);">Programs Offered</a>
                    </li>
                </ul>
            
                <div class="tab" id="tab-99" <?php if(!isset($x)): ?>style="display:block;"<?php endif; ?>>
            
                    <div class="program-availability"><?php if($pageid==110){ echo 'Vista College has made the difficult decision to cease enrolling students at the Lubbock campus and it will close in 2021. We will provide current students the opportunity to complete their Medical Assisting program, and the campus remains committed to maintaining high quality standards throughout the program. All support services will be available to the students throughout the teach-out, including post-graduation placement assistance.<BR><BR><div class="link"><i class="fa fa-bookmark"></i> <a href="/wp-content/files/LubbockFAQs.pdf" target="_blank">Read FAQs</a></div><br><br>';} ?>
                        <div class="availability-header">
								
                            <h2 class="section-title">Programs <b>Offered</b></h2>
                            <div class="orderDropdown">
                                <button class="dropbtn">Sort Programs By</button>
                                <div class="dropdown-content">
                                    <a href="#" data-order="parent">Default</a>
                                    <a href="#" data-order="title">Alphabetical</a>
                                    <a href="#" data-order="degree">Degree Type</a>
                                </div>
                            </div>
                        </div>
                        
                        <div class="availability-boxes group">
                        
                            <div class="availability-box no-border">
                            
                                <div id="programs_by_parent" class="row equal-height programs-offered-list">
                                    <?php echo do_shortcode('[programs_offered order="parent"]'); ?>
                                </div>

                                <div id="programs_by_title" class="row equal-height programs-offered-list" style="display: none;">
                                    <?php echo do_shortcode('[programs_offered order="title"]'); ?>
                                </div>

                                <div id="programs_by_degree" class="row equal-height programs-offered-list" style="display: none;">
                                    <?php echo do_shortcode('[programs_offered order="degree"]'); ?>
                                </div>

                                <?php if ($campuse_disclosure):?>
                                    <p><BR><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                                        <?php echo $campuse_disclosure;?></p>
                                <?php endif;?>

                            </div>
                        
                        </div>
                        
                        <?php if (get_the_ID() == 26): ?>
                        
                            <div class="availability-footer group">

                                <p class="full-width">Vista College Online Campus is accredited by <b>Accrediting Commission of Career Schools and Colleges</b></p>
                            
                            </div>
                        
                        <?php else: ?>
                         <?php if (get_the_ID() == 110){ echo '<br /><i class="fa fa-thumb-tack" aria-hidden="true"></i> This program is closed for new enrollments as of July 21, 2020.';} ?>
								
                            <div class="availability-footer group">

                                <p class="full-width">Vista College ground campuses are accredited by <b>Council on Occupational Education</b></p>
                            
                            </div>
                        
                        <?php endif; ?>
                        
                    </div>
                    
                </div>
            
            <?php endif; ?>
        
        </div>
        
    </div>
    
</div>

<?php if( have_rows('fc_content', 'option') ): ?>
  <div class="gallerywrap" style="<?php if((get_the_ID() == 14)||(get_the_ID()==26)){echo 'display: none';}else{/**/} ?>">
    <div class="content group "> 
     <h2 class="section-title">What's <b> HAPPENING </b> On Campus </h2>

     <?php
        $instagram_id = '';
        $instagram_url = '#';
        switch (get_the_ID()) {
            case '7':   //Beaumont, TX
                $instagram_id = 1;
                $instagram_url = 'https://www.instagram.com/vistacollege_beaumont/';
                break;
            case '103':   //College Station, TX
                $instagram_id = 2;
                $instagram_url = 'https://www.instagram.com/vistacollege_collegestation/';
                break;
            case '106':   //El Paso, TX (Main Campus)
                $instagram_id = 3;
                $instagram_url = 'https://www.instagram.com/vistacollege_elpaso/';
                break;
            case '104':   //Fort Smith, AR
                $instagram_id = 4;
                $instagram_url = 'https://www.instagram.com/vistacollege_fortsmith/';
                break;
            case '105':   //Killeen, TX
                $instagram_id = 5;
                $instagram_url = 'https://www.instagram.com/vistacollege_killeen/';
                break;
             case '8':   //Las Cruces, NM
                $instagram_id = 6;
                $instagram_url = 'https://www.instagram.com/vistacollege_lascruces/';
                break;
             case '109':   //Longview, TX
                $instagram_id = 7;
                $instagram_url = 'https://www.instagram.com/vistacollege_longview/';
                break;
             case '110':   //Lubbock, TX
                $instagram_id = 8;
                $instagram_url = 'https://www.instagram.com/vistacollege_lubbock/';
                break;
            case '111':   //Lubbock, TX – Frankford Branch Campus
                $instagram_id = 9;
                $instagram_url = 'https://www.instagram.com/vistacollege_lubbock/';
                break;
            default:
                $instagram_id = '';
                $instagram_url = '#';
                break;

        }
     ?>

<div class="rights" style="text-align:center;margin-bottom:40px">

        <a href="<?php echo $instagram_url; ?>" target="_blank" rel="noreferrer" class="follow-instagram"><i class="fa fa-instagram" aria-hidden="true"></i>Follow Us on Instagram</a>
    </div>

   </div>

  </div>

  <div class="instagram_container">
      <?php
        if ($instagram_id)
            echo do_shortcode( '[elfsight_instagram_feed id="'.$instagram_id.'"]' );

      ?>
  </div>
  <div class="footer-top">
    <div class="footer-left-top">
        <div class="left-side-top">
            <div class="left-side-title">
                <h1>Ready to take the next step?</h1>
                <span>Our dedicated Admission team will be glad to help<span>
            </div>
        </div>
        <div class="left-side-bottom">
            <?php $online_status = get_the_ID() == 26 ?  true : false; ?>
            <input type="hidden" name="online_status" id="online_status" value="<?php echo $online_status;?>">
            <select id="program_select">
                <option value="">Which Program are You Interested in?</option>
                <?php $ids = get_field('av_programs'); ?>
                
                <?php $program_loop = new WP_Query( array( 'post_type' => 'programs', 'post__in' => $ids, 'posts_per_page' => -1 ) ); 
                if($program_loop->have_posts()) : ?>
                    <?php while ( $program_loop->have_posts() ) : $program_loop->the_post(); ?>
                    
                        <option value="<?php the_title(); ?>"><?php the_title(); ?></option>
                    
                    <?php endwhile; ?>
                <?php wp_reset_postdata(); endif; ?>
            </select>
            <a class="get-more-info" onClick="_gaq.push(['_trackEvent', 'RFI', 'Click', 'NextStep']);">GET MORE INFO <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
        </div>
    </div>
    <div class="footer-right-top"></div>
  </div>

<?php endif; ?>   

 <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> 
<?php get_footer(); ?>