<?php get_header(); ?>


<div class="section section-7">
        
    <div class="content group">
    
        <aside class="left-side">
    
            <?php if(child_pages() != ""): ?>
                <?php echo child_pages(); ?>
            <?php else: ?>
                <?php wp_nav_menu( array('menu' => 'footer-1')); ?>
            <?php endif; ?>
        
        </aside>
    
        <div class="center-content">
        
            <h3>Page Not Found</h3>        
        
            <p>The page you are looking for doesn't exist.</p>

            <a href="<?php echo site_url(); ?>" class="button">Return Home</a>
            
        </div>
        
        <aside class="right-side">
    
            <div class="program-areas">
            
                <h2>Program <b>Areas</b></h2>
                
                <div>
                
                    <p>We offer career training that will help you start in a new rewarding career in a matter of months.</p>
        
                    <?php $area_loop = new WP_Query( array( 'post_type' => 'programs', 'post_parent' => 0, 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1 ) ); if ( $area_loop->have_posts() ) : ?>

                        <ul>

                            <?php while ( $area_loop->have_posts() ) : $area_loop->the_post(); ?>
                            
                                <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                            <?php endwhile; ?>
                            
                        </ul>
                        
                    <?php wp_reset_postdata(); endif; ?>
                    
                </div>
            
            </div>
        
        </aside>
        
    </div>
    
</div>


<?php get_footer(); ?>