<?php /* Template name: Left Sidebar */ ?>


<?php 
get_header(); 
define('DONOTCACHEPAGE', true);
?>

    <?php if ( have_posts() ) : ?>
    
        <div class="section section-7">
        
            <div class="content group">
            <div class="combine">
                <aside class="left-side left">

                	<?php if(is_page(10875) || is_page(11189) || is_page(11191) || is_page(11185) || is_page(11177) || is_page(11187) ) echo "<h4>CE Courses</h4>"; ?>
                    <?php if( have_rows('na_navigation'.$GLOBALS['device']) ): ?>
        
                        <ul class="categories">
                        
                            <?php while( have_rows('na_navigation'.$GLOBALS['device']) ): the_row(); ?>
                             
                                <?php if(get_sub_field('na_type'.$GLOBALS['device']) == "External"): ?>
                                
                                    <?php if(get_sub_field('na_title'.$GLOBALS['device']) && get_sub_field('na_link'.$GLOBALS['device'])): ?>
                                    
                                        <li>
                                            <a href="<?php the_sub_field('na_link'.$GLOBALS['device']); ?>" target="_blank"><?php the_sub_field('na_title'.$GLOBALS['device']); ?></a>
                                        </li>
                                        
                                    <?php endif; ?>
                                    
                                <?php else: ?>
                                
                                    <?php if(get_sub_field('na_page'.$GLOBALS['device'])): $id=get_sub_field('na_page'.$GLOBALS['device']); ?>
                                
                                        <li>
                                            <a href="<?php echo get_the_permalink($id); ?>"><?php echo get_the_title($id); ?></a>
                                        </li>
                                
                                    <?php endif; ?>
                                
                                <?php endif; ?>
                            
                            <?php endwhile; ?>
                            
                        </ul>

                    <?php elseif(child_pages() != ""): ?>
                    
                        <?php echo child_pages(); ?>
                        
                    <?php else: ?>
                    
                        <?php wp_nav_menu( array('menu' => 'footer-1')); ?>
                        
                    <?php endif; ?>
                
                </aside>
             
                </div>
                <div class="center-contents">
                
                    <?php if(get_field('co_content'.$GLOBALS['device'])){the_field('co_content'.$GLOBALS['device']);} ?>
                    
                </div>
                
                
                
            </div>
            
        </div>

    <?php endif; ?>
        
<?php get_footer(); ?>