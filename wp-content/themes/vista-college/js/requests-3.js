jQuery(document).ready(function() {
    jQuery(".gchoice_3_8_0").append("<label class='label-button' for='choice_3_8_0'><span></span></label>");
    jQuery(".gchoice_3_8_1").append("<label class='label-button' for='choice_3_8_1'><span></span></label>");
    jQuery(".gchoice_3_8_0 label, .gchoice_3_8_1 label").click(function() {
        jQuery(this).parent().parent().find(".label-button span").removeClass("active");
        jQuery(this).parent().find(".label-button span").addClass("active")
    });
    jQuery(".gchoice_3_8_0 label").click(function() {
        jQuery("#campus-selector-2").removeClass('invalid_message');
        jQuery("#input_3_19").removeClass('invalid_message')
        jQuery("#input_3_20").removeClass('invalid_message')
        jQuery("#input_3_21").removeClass('invalid_message')
        jQuery('#campus-selector-2').addClass("disabled-selector");
        jQuery('#program-selector-2').addClass("disabled-selector");
        jQuery.ajax({
            url: "/wp-content/themes/vista-college/js/ajax/campuses.php",
            type: "POST",
            data: {
                'post_id': jQuery(".programs-request-form #post_id").val()
            },
            dataType: "HTML",
            success: function(data) {
                jQuery('#campus-selector-2').html(data);
                if (jQuery('#campus-selector-2 option').length <= 3) {
            		jQuery("#campus-selector-2").val(jQuery("#campus-selector-2 option:last").val());
            		jQuery("#campus-selector-2").change();
                }
                jQuery('#campus-selector-2').removeClass("disabled-selector")
            },
            error: function(jqXHR, data) {}
        });
        jQuery("#campus-selector-2").css("display", "block");
        jQuery('#program-selector-2').html('<option value="">Which Program are You Interested in?</option>');
        jQuery("#input_3_10").val("");
        jQuery("#input_3_12").val("")
    });
    jQuery("#campus-selector-2").change(function() {
        jQuery('#program-selector-2').addClass("disabled-selector");
        jQuery("#input_3_10").val(jQuery("#campus-selector-2").val());
        jQuery.ajax({
            url: "/wp-content/themes/vista-college/js/ajax/ground-programs.php",
            type: "POST",
            data: {
                'title': jQuery("#campus-selector-2").val()
            },
            dataType: "HTML",
            success: function(data) {
                jQuery('#program-selector-2').html(data);
                jQuery('#program-selector-2').removeClass("disabled-selector");
                if (jQuery(".programs-request-form #post_id").val()) {
                    jQuery('#program-selector-2').val(jQuery("#page_title").val()).change()
                }
            },
            error: function(jqXHR, data) {}
        })
    });
    jQuery("#program-selector-2").change(function() {
        jQuery("#input_3_12").val(jQuery("#program-selector-2").val())
    });
    jQuery(".gchoice_3_8_1 label").click(function() {
        jQuery("#campus-selector-2").removeClass('invalid_message');
        jQuery("#input_3_19").removeClass('invalid_message')
        jQuery("#input_3_20").removeClass('invalid_message')
        jQuery("#input_3_21").removeClass('invalid_message')

        jQuery('#program-selector-2').addClass("disabled-selector");
        jQuery("#campus-selector-2").css("display", "none");
        jQuery("#input_3_10").val("Online Campus");
        jQuery("#input_3_12").val("");
        jQuery.ajax({
            url: "/wp-content/themes/vista-college/js/ajax/online-programs.php",
            type: "POST",
            dataType: "HTML",
            success: function(data) {
                jQuery('#program-selector-2').html(data);
                jQuery('#program-selector-2').removeClass("disabled-selector");
                if (jQuery("#online_title").val()) {
                    jQuery('#program-selector-2').val(jQuery("#online_title").val()).change()
                }
            },
            error: function(jqXHR, data) {}
        })
    });
    jQuery("#next-button-3").click(function() {
        var valid = !0;
        jQuery("#campus-selector-2").removeClass('invalid_message');
        jQuery("#input_3_19").removeClass('invalid_message')
        jQuery("#input_3_20").removeClass('invalid_message')
        jQuery("#input_3_21").removeClass('invalid_message')
        if (jQuery('#input_3_10').val() == "") {
            valid = !1;
            jQuery("#campus-selector-2").addClass('invalid_message')
        }
        if (jQuery('#input_3_19').val() == "") {
            valid = !1;
            jQuery("#input_3_19").addClass('invalid_message')
        }
        if (jQuery('#input_3_20').val() == "") {
            valid = !1;
             jQuery("#input_3_20").addClass('invalid_message')
        }
        if (jQuery('#input_3_21').val() == "") {
            valid = !1;
             jQuery("#input_3_21").addClass('invalid_message')
        } else {
        	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			if(!jQuery('#input_3_21').val().match(mailformat)) {
	            valid = !1;
	             jQuery("#input_3_21").addClass('invalid_message')
			}
        }

        if (valid == !0) {
            jQuery('#part-1').css("display", "none");
            jQuery('#form-number-1').css("display", "none");
            jQuery('#form-number-2').css("display", "block");
            jQuery('#part-2, #gform_wrapper_3 .gform_footer').css("display", "block");
            jQuery('.request-progress li:first-child').removeClass("active-progress");
            jQuery('.request-progress li:last-child').addClass("active-progress")
        }
    });
    jQuery("#form-number-2").click(function() {
        jQuery('#part-1').css("display", "block");
        jQuery('#form-number-1').css("display", "block");
        jQuery('#form-number-2').css("display", "none");
        jQuery('#part-2, #gform_wrapper_3 .gform_footer').css("display", "none");
        jQuery('.request-progress li:first-child').addClass("active-progress");
        jQuery('.request-progress li:last-child').removeClass("active-progress")
    });

    var submit_status = false;
    jQuery(".programs-request-form #gform_3").submit(function( event ) {
        if (!submit_status) {
            event.preventDefault();
        }
    });

    jQuery(".programs-request-form #gform_submit_button_3").on('click', function(e) {
        var valid = !0;
        jQuery("#input_3_22").removeClass('invalid_message');
        jQuery("#input_3_6").removeClass('invalid_message');
        jQuery("#input_3_23").removeClass('invalid_message');
        if (jQuery('#input_3_22').val() == "") {
            valid = !1;
            jQuery("#input_3_22").addClass('invalid_message')
        }
        if (jQuery('#input_3_6').val() == "") {
            valid = !1;
            jQuery("#input_3_6").addClass('invalid_message')
        }
        if (jQuery('#input_3_23').val() == "") {
            valid = !1;
            jQuery("#input_3_23").addClass('invalid_message')
        }

        if (valid == !0) {
            submit_status = true;
            jQuery(".programs-request-form #gform_3").submit();
        } else {
            submit_status = false;
        }
    });

    var online_title = jQuery("#online_title").val();
    if (online_title) {
        jQuery(".gchoice_3_8_1 label").trigger('click')
    } else {
        jQuery(".gchoice_3_8_0 label").trigger('click')
    }

	jQuery(document).click(function(event) { 
		if (event.target.id == "programModal") {
			jQuery('#programModal').css("display", "none");
		}
	});

    jQuery(".form-desc").click(function() {
    	jQuery('#programModal').css("display", "block");
    });

    jQuery("#closeProgramModal").click(function() {
    	jQuery('#programModal').css("display", "none");
    });

    jQuery('#programModal').on('focusout', function () {
	 	jQuery('#programModal').css("display", "none");
	});
})