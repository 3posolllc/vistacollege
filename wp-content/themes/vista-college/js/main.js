(function ($) {
    "use strict";
    $(document).ready(function () {
        $(function () {
            $('#myList a:first-child').tab('show');
        })
    });
})(jQuery);
jQuery(document).ready(function () {
    jQuery(document).click(function () {
        jQuery(".video-player").css("display", "none");
        jQuery(".request-form").css("display", "none");
        jQuery(".video-content").html('');
        jQuery(".search-section").css("right", "-400px");
        jQuery(".mobile-nav").css("max-height", "0px");
    });
    jQuery(".request-form").click(function () {
        jQuery(".request-form").css("display", "none");
    });
    jQuery(".request-close").click(function () {
        jQuery(".request-form").css("display", "none");
    });
    jQuery(".player-btn, .video-content, .request-left, .request-right, #next-button-1, .video-btn, .vid-player").click(function (e) {
        e.stopPropagation();
    });
    jQuery(".search-section, .mobile-nav").click(function (e) {
        e.stopPropagation();
    });
    jQuery(".video-player").click(function () {
        jQuery(".video-player").css("display", "none");
        jQuery(".video-content").html('');
    });
    jQuery(".search-close").click(function () {
        jQuery(".search-section").hide();
        jQuery(".search-section").css("right", "-400px");
    });
    jQuery(".search-get").click(function (e) {
        e.stopPropagation();
        jQuery(".search-section").show();
        jQuery(".search-section").css("right", "0px");
    });
    var slideshow_3 = jQuery('#slideshow-3').glide({
        startAt: '1',
        autoplay: 8000,
        hoverpause: false
    }).data('api_glide');
    jQuery("#news-prev").click(function () {
        slideshow_3.prev();
    });
    jQuery("#news-next").click(function () {
        slideshow_3.next();
    });
    jQuery("#menu-header-1 .menu-item-990").hover(function () {
        jQuery(".dropdown").css("display", "block");
    }, function () {
        jQuery(".dropdown").css("display", "none");
    });
    jQuery(".dropdown").hover(function (e) {
        e.stopPropagation();
        jQuery(this).css("display", "block");
    }, function () {
        jQuery(this).css("display", "none");
    });
    jQuery("#menu-header-1 li a").hover(function () {
        jQuery(this).parent().find(".sub-menu").css("display", "block");
    }, function () {
        jQuery(this).parent().find(".sub-menu").css("display", "none");
    });
    jQuery("#menu-header-1 li .sub-menu").hover(function (e) {
        e.stopPropagation();
        jQuery(this).css("display", "block");
    }, function () {
        jQuery(this).css("display", "none");
    });
    jQuery(".dropdown-list > li > a").click(function () {
        if (jQuery(this).parent().find("ul").height() > 0) {
            jQuery(this).parent().removeClass("active");
            jQuery(this).parent().find("ul").css("max-height", "0px");
        } else {
            jQuery(this).parent().addClass("active");
            jQuery(this).parent().find("ul").css("max-height", "1000px");
        }
    });
    jQuery(".google-map").click(function () {
        jQuery(this).find("iframe").css("pointer-events", "initial");
    });
    jQuery(".google-map").hover(function () { },
        function () {
            jQuery(this).find("iframe").css("pointer-events", "none");
        });
    jQuery(".player-btn, .video-btn, .vid-player").click(function () {
        var video = jQuery(this).parent().find(".video-iframe").val();
        jQuery(".video-player").css("display", "table");
        jQuery(".video-content").html(video);
        jQuery("iframe[src*='youtube']").each(function () {
            jQuery(this).height(jQuery(this).width() * 0.5625);
        });
    });
    jQuery(".book-btn").click(function (e) {
        e.stopPropagation();
        if (jQuery(window).width() > 768) {
            var video = jQuery(this).parent().find(".video-iframe").val();
            jQuery(".video-player").css("display", "table");
            jQuery(".video-content").html(video);
            jQuery(".video-content").css("background-color", "#fff");
            jQuery("iframe[src*='youtube']").each(function () {
                jQuery(this).height(jQuery(this).width() * 0.5625);
            });
        } else {
            window.open("./set-an-appointment/", "_self");
        }
    });
    jQuery(".get-request").click(function (e) {
        e.stopPropagation();
        jQuery(".request-form").css("display", "table");
    });
    jQuery(".fa-bars").click(function (e) {
        e.stopPropagation();
        if (jQuery(".mobile-nav").height() > 0) {
            jQuery(".mobile-nav").css("max-height", "0px");
        } else {
            jQuery(".mobile-nav").css("max-height", "10000px");
        }
    });
    jQuery("iframe[src*='youtube']").each(function () {
        jQuery(this).height(jQuery(this).width() * 0.5625);
    });
    jQuery(window).resize(function () {
        jQuery("iframe[src*='youtube']").each(function () {
            jQuery(this).height(jQuery(this).width() * 0.5625);
        });
    });
    jQuery("#menu-header .menu-item-has-children").append('<span class="open">+</span>');
    jQuery(".open").on("click", function (e) {
        e.stopPropagation();
        if (jQuery(this).parent().find("> .sub-menu").height() > 0) {
            jQuery(this).html("+");
            jQuery(this).parent().find("> .sub-menu").css("max-height", "0px");
        } else {
            jQuery(this).html("-");
            jQuery(this).parent().find("> .sub-menu").css("max-height", "10000px");
        }
    });
    jQuery(".show-more").click(function () {
        jQuery(".show-more").css("display", "none");
        jQuery(".show-less").css("display", "block");
        jQuery(".testimonial-column").addClass("active-testimonial-column");
    });
    jQuery(".show-less").click(function () {
        jQuery(".show-less").css("display", "none");
        jQuery(".show-more").css("display", "block");
        jQuery(".testimonial-column").removeClass("active-testimonial-column");
    });
    jQuery(".get-more-info").click(function (e) {
        if ((jQuery("#campus_select").val()) || (jQuery("#program_select").val())) {
            // if (jQuery(window).width() > 1200){
            // e.stopPropagation();
            // jQuery(".request-form").css("display","table");
            // jQuery("#next-button-2").trigger('click');            
            // }else {
            var current = jQuery("#page_title").val();
            var program = jQuery("#program_select").val();
            var campus = jQuery("#campus_select").val();
            var online = jQuery("#online_status").val();
            if (jQuery("#program_select").val() && online) current = 'Online Campus';
            window.location.href = "/request-info/?cur_title=" + current + "&sel_pro=" + program + "&sel_cam=" + campus + "&online=" + online;
            // }
        } else {
            jQuery("#campus_select").css("border-color", "#dc433d");
            jQuery("#program_select").css("border-color", "#dc433d");
            return false;
        }
    });
    jQuery(".get-more-info-new").click(function (e) {
        if ((jQuery('input[name="campus_select"]:checked').val()) || (jQuery("#program_select").val())) {
            // if (jQuery(window).width() > 1200){
            // e.stopPropagation();
            // jQuery(".request-form").css("display","table");
            // jQuery("#next-button-2").trigger('click');            
            // }else {
            var current = jQuery("#page_title").val();
            var program = jQuery("#program_select").val();
            var campus = jQuery('input[name="campus_select"]:checked').val();
            var online = jQuery("#online_status").val();
            if (jQuery("#program_select").val() && online) current = 'Online Campus';
            window.location.href = "/request-info/?cur_title=" + current + "&sel_pro=" + program + "&sel_cam=" + campus + "&online=" + online;
            // }
        } else {
            jQuery("#campus_select").css("border-color", "#dc433d");
            jQuery("#program_select").css("border-color", "#dc433d");
            jQuery("#campus-error").css("display", "block");
            return false;
        }
    });
    jQuery(".request-btn").click(function (e) {
        if (jQuery("#request_select").val()) {
            var current = jQuery("#page_title").val();
            var campus = jQuery("#request_select").val();
            var online = jQuery("#online_status").val();
            if (jQuery("#program_select").val() && online) current = 'Online Campus';
            window.location.href = "/request-info/?cur_title=" + current + "&sel_pro=''&sel_cam=" + campus + "&online=" + online;
        } else {
            jQuery("#request_select").css("border-color", "#f00");
            return false;
        }
    });
    jQuery('input[name="campus_select"]').change(function () {
        if (this.value) {
            jQuery("#campus-error").css("display", "none");
        }
    });
    jQuery("#campus_select").change(function () {
        jQuery("#campus_select").css("border-color", "rgb(169, 169, 169)");
        jQuery(".gchoice_2_8_0 label").trigger('click');
        jQuery("#input_2_12").val(jQuery("#page_title").val());
        jQuery("#input_2_10").val(jQuery("#campus_select").val());
    });
    jQuery("#program_select").change(function () {
        jQuery("#program_select").css("border-color", "rgb(169, 169, 169)");
        jQuery(".gchoice_2_8_0 label").trigger('click');
        jQuery("#input_2_12").val(jQuery("#program_select").val());
        jQuery("#input_2_10").val(jQuery("#page_title").val());
    });
    jQuery("#request_select").change(function () {
        jQuery("#request_select").css("border-color", "rgb(169, 169, 169)");
        jQuery(".gchoice_2_8_0 label").trigger('click');
        jQuery("#input_2_12").val(jQuery("#page_title").val());
        jQuery("#input_2_10").val(jQuery("#request_select").val());
    });
    if (jQuery('#campus_select option').length == 2) {
        jQuery("select#campus_select").val(jQuery('#campus_select option').eq(1).val());
        jQuery("#campus_select").trigger('change');
    }
    if (jQuery('#program_select option').length == 2) {
        jQuery("select#program_select").val(jQuery('#program_select option').eq(1).val());
        jQuery("#program_select").trigger('change');
    }
    jQuery("#careers").change(function () {
        if (jQuery("#careers").val() == "") {
            jQuery(".careers-form a").css("pointer-events", "none");
        } else {
            jQuery(".careers-form a").attr("href", jQuery("#careers").val());
            jQuery(".careers-form a").css("pointer-events", "initial");
        }
    });
    jQuery(".subnav-button .fa-navicon").on("click", function (e) {
        jQuery(".subnav.categories").toggle();
    })
    jQuery(".orderDropdown a").on("click", function (e) {
        e.preventDefault();
        jQuery(".programs-offered-list").hide(1000);
        jQuery("#programs_by_" + jQuery(this).attr('data-order')).show(1000);
    })
    // MOBILE BUTTON ARRANGEMENT - 8/2020 - Jamon Abercrombie
    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
        jQuery('body').addClass('idevice');
    }
    jQuery(window).on("resize", function () {
        var deviceWidth = jQuery(document).width();
        if (deviceWidth < 760) {
            jQuery(".content.group.view-programs").detach().prependTo('.section.section-2');
            jQuery('.view-programs .grid-box > a[href*="trades"] > img').attr('src', 'https://www.vistacollege.edu/wp-content/uploads/2020/05/icon-2020-trades.png');
            jQuery('.view-programs .grid-box > a[href*="technology"] > img').attr('src', 'https://www.vistacollege.edu/wp-content/uploads/2020/05/icon-2020-technology.png');
            jQuery('.view-programs .grid-box > a[href*="legal"] > img').attr('src', 'https://www.vistacollege.edu/wp-content/uploads/2020/05/icon-2020-legal.png');
            jQuery('.view-programs .grid-box > a[href*="healthcare"] > img').attr('src', 'https://www.vistacollege.edu/wp-content/uploads/2020/05/icon-2020-healthcare.png');
            jQuery('.view-programs .grid-box > a[href*="cosmetology"] > img').attr('src', 'https://www.vistacollege.edu/wp-content/uploads/2020/05/icon-2020-cosmetology.png');
            jQuery('.view-programs .grid-box > a[href*="business"] > img').attr('src', 'https://www.vistacollege.edu/wp-content/uploads/2020/05/icon-2020-business.png');
        } else {
            jQuery(".content.group.view-programs").detach().appendTo('.section.section-2');
            jQuery('.view-programs .grid-box > a[href*="trades"] > img').attr('src', 'https://www.vistacollege.edu/wp-content/uploads/2019/02/VIS_LP_Trades-Icon_20181213_v002.png');
            jQuery('.view-programs .grid-box > a[href*="technology"] > img').attr('src', 'https://www.vistacollege.edu/wp-content/uploads/2019/02/VIS_LP_Tech-Icon_20181213_v002.png');
            jQuery('.view-programs .grid-box > a[href*="legal"] > img').attr('src', 'https://www.vistacollege.edu/wp-content/uploads/2019/02/VIS_LP_Law-Icon_20181213_v002.png');
            jQuery('.view-programs .grid-box > a[href*="healthcare"] > img').attr('src', 'https://www.vistacollege.edu/wp-content/uploads/2019/02/VIS_LP_Health-Icon_20181213_v002.png');
            jQuery('.view-programs .grid-box > a[href*="cosmetology"] > img').attr('src', 'https://www.vistacollege.edu/wp-content/uploads/2019/02/VIS_LP_Cos-Icon_20181213_v002.png');
            jQuery('.view-programs .grid-box > a[href*="business"] > img').attr('src', 'https://www.vistacollege.edu/wp-content/uploads/2019/02/VIS_LP_Busi-Icon_20181213_v002.png');
        }
    }).resize();
    // #end_mobile_buttons
    // HOMEPAGE VIDEO BACKGROUND - 11/2020 - JA
    jQuery('.slideshow-banner').hide();
    jQuery("#next-button-1").text(function () {
        return jQuery(this).text().replace("Let's Get Started", "Next");
    });
    jQuery(".showform a").click(function () {
        jQuery('.slideshow-form').removeClass('closed');
        jQuery('.showform').hide();
    });
    jQuery("#closeform a").click(function () {
        jQuery('.slideshow-form').addClass('closed');
        jQuery('.showform').show();
    });
    // end video back
}); // end main jQuery function
function tab(id, scroll) {
    jQuery(".tab").css("display", "none");
    jQuery("#tab-" + id).css("display", "block");
    jQuery(".page-navigation li").removeClass("active");
    jQuery(".tab-btn-" + id).addClass("active");
}