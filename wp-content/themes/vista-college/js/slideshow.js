jQuery(document).ready(function(){

    var slideshow_1 = jQuery('#slideshow-1').glide({
        startAt: '1',
        autoplay: 8000,
        hoverpause: false
    }).data('api_glide');
    
    var slideshow_2 = jQuery('#slideshow-2').glide({
        startAt: '1',
        autoplay: 8000,
        hoverpause: false
    }).data('api_glide');

    jQuery("#prev").click(function(){
        slideshow_1.prev();
        slideshow_2.prev();
    });
    
    jQuery("#next").click(function(){
        slideshow_1.next();
        slideshow_2.next();
    });
    
    jQuery("#slideshow-2 .slider-nav a:nth-child(1)").click(function(){
        jQuery("#slideshow-1 .slider-nav a:nth-child(1)").click();
    });
    
    jQuery("#slideshow-2 .slider-nav a:nth-child(2)").click(function(){
        jQuery("#slideshow-1 .slider-nav a:nth-child(2)").click();
    });
    
    jQuery("#slideshow-2 .slider-nav a:nth-child(3)").click(function(){
        jQuery("#slideshow-1 .slider-nav a:nth-child(3)").click();
    });
    
    jQuery("#slideshow-2 .slider-nav a:nth-child(4)").click(function(){
        jQuery("#slideshow-1 .slider-nav a:nth-child(4)").click();
    });
    
    jQuery("#slideshow-2 .slider-nav a:nth-child(5)").click(function(){
        jQuery("#slideshow-1 .slider-nav a:nth-child(5)").click();
    });
    
    var position = 0;
    
    jQuery("#slideshow-2").bind('touchstart', function(event){
        position = event.originalEvent.changedTouches[0].pageX;
    }).bind('touchend', function(event){
        position = position - event.originalEvent.changedTouches[0].pageX;
        if(position < -50){
            slideshow_1.prev();
        }
        if(position > 50){
            slideshow_1.next();
        }
    });
    
    var slideshow_4 = jQuery('#slideshow-4').glide({
        startAt: '1',
        autoplay: 8000,
        hoverpause: false
    }).data('api_glide');
    
    jQuery("#featured-prev").click(function(){
        slideshow_4.prev();
    });
    
    jQuery("#featured-next").click(function(){
        slideshow_4.next();
    });
    
});