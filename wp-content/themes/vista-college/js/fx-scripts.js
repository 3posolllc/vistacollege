/* ---------------------------------------------------------------------
Global Js
Target Browsers: All
------------------------------------------------------------------------ */

var FX = (function(FX, $) {

    /**
     * Doc Ready
     */
    $(function() {

        FX.Tabs.init();
        FX.General.init();

    });

    /**
	 * General functionality — ideal for one-liners or super-duper short code blocks
	 */
    FX.General = {
    	init: function() {
			this.bind();
		},

		bind: function() {

			// IE CSS object-fit support
			objectFitImages();

		}
    };

  /**
        * Tab Content
        * @type {Object}
        */
        /* HTML Formatting should follow this basic pattern:
        <ul class="js-tabs">
        <li><a href="#tab-content-1">Tab</a></li>
        <li><a href="#tab-content-2">Tab</a></li>
        </ul>
        <div id="tab-content-1">
        <!-- content -->
        </div>
        <div id="tab-content-2">
        <!-- content -->
        </div>
        */
        FX.Tabs = {
        init: function() {
          $( '.js-tabs' ).on( 'click touchstart', 'a', this.switchTab );
        },
        switchTab: function( e ) {
          e.preventDefault();
          var $this = $( this ),
            $tab  = $( $this.attr( 'href' ) );

          $this.parent()
             .addClass( 'active' )
             .siblings()
             .removeClass( 'active' );

          $tab.addClass( 'active' )
            .siblings()
            .removeClass( 'active' );
        }
        };


    return FX;
}(FX || {}, jQuery));
