<?php include "../../../../../wp-config.php";?>
<?php $post_id = sanitize_title($_POST['post_id']); ?>

<option value="">Which Campus are You Interested in?</option>

<?php $campus_loop = new WP_Query( array( 'post_type' => 'campus', 'post__not_in' => array(26), 'posts_per_page' => -1 ) ); if ( $campus_loop->have_posts() ) : ?>

    <?php while ( $campus_loop->have_posts() ) : $campus_loop->the_post(); ?>
    	<?php if ($post_id) : ?>
	        <?php if(get_field('av_programs')): ?>
	            <?php $ids = get_field('av_programs'); ?>
	            <?php if (in_array($post_id, $ids)) : ?>					
					<option value="<?php the_title(); ?>"><?php the_title(); ?></option>					
	            <?php endif; ?>
	        <?php endif; ?>
		<?php else : ?>
	       	 <option value="<?php the_title(); ?>"><?php the_title(); ?></option>
	    <?php endif; ?>
    <?php endwhile; ?>
    
<?php wp_reset_postdata(); endif; ?>