<?php include "../../../../../wp-config.php"; ?>

<?php $programs = sanitize_title($_POST['programs']); ?>

<?php $programs_loop = new WP_Query( array( 'post_type' => 'programs' ) ); if ( $programs_loop->have_posts() ) : ?>
        
    <?php while ( $programs_loop->have_posts() ) : $programs_loop->the_post(); ?>            
        <option value="<?php the_title(); ?>"><?php the_title(); ?></option>
    <?php endwhile; ?>
        
<?php wp_reset_postdata(); endif; ?>