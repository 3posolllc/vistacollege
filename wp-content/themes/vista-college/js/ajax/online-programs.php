<?php include "../../../../../wp-config.php"; ?>

<option value="">Which Program are You Interested in?</option>

<?php $online_loop = new WP_Query( array( 'p' => 26, 'post_type' => 'campus' ) ); if ( $online_loop->have_posts() ) : ?>
        
    <?php while ( $online_loop->have_posts() ) : $online_loop->the_post(); ?>

        <?php if(get_field('av_programs')): ?>
            
            <?php $ids = get_field('av_programs'); ?>
            
            <?php $program_loop = new WP_Query( array( 'post_type' => 'programs', 'post__in' => $ids, 'posts_per_page' => -1 ) ); if($program_loop->have_posts()) : ?>
                
                <?php while ( $program_loop->have_posts() ) : $program_loop->the_post(); ?>
                                                
                    <?php if (!get_field('tech_out')) : ?>
                        <option value="<?php the_title(); ?>"><?php the_title(); ?></option>
                    <?php endif;?>
                
                <?php endwhile; ?>
            
            <?php endif; ?>
            
        <?php endif; ?>

    <?php endwhile; ?>
        
<?php wp_reset_postdata(); endif; ?>

<option value="-Undecided-">-Undecided-</option>