<?php /* Template name: Vista Online */ ?>

<?php get_header(); ?>
<style>
.content h1{max-width:600px!important}
.breadcrumb, .breadcrumb a{color:#339999}
.online-menu a{
    color: #339999;
    font-weight: bold !important;}
a.online-menu:hover{
    color: #a01a16!important;}

#menu-header-1 .sub-menu {
	width:239px!important;
}
@media (min-width: 1200px){
    .banner-content h1{padding-right:0px!important;
    margin: 0px!important;}
} 
</style>
<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">

<div class="section section-7 vista-online">
    <div class="vista-online-body">
            
        <div class="content group vista-online-content">
        
            <aside class="left-side">
            
                <div class="subnav-button">
                    <a class="page-title online-menu" href="/vista-online/">Online Campus at Vista College</a>
                </div>
        
                <div class="subnav">
                    <?php wp_nav_menu( array('menu' => 'online')); ?>
                </div>
            
			<a href="https://www.vistacollege.edu/files/onlinebrochure/online-brochures.pdf" target="_blank"><img src="/wp-content/uploads/2020/06/vista-college-brochure-CTA.png"/></a>
            </aside>
        
            <div class="center-content">
<?php              	$pagename = get_query_var('pagename');  ?> 
                <?php if(get_field('co_content'.$GLOBALS['device'])){the_field('co_content'.$GLOBALS['device']);} ?>
                <?php if (get_field('av_programs') && $pagename=="vista-online-programs"): ?>

                        <div class="program-availability">

                            <h2 class="online-programs-title">Programs Offered</h2>

                            <div class="availability-boxes group">

                                <div class="availability-box no-border">

                                    <?php $ids = get_field('av_programs'); ?>

                                    <?php $program_loop = new WP_Query(array('post_type' => 'programs', 'post__in' => $ids, 'posts_per_page' => -1));
                                    if ($program_loop->have_posts()) : ?>

                                        <div class="row equal-height">

                                            <?php while ($program_loop->have_posts()) : $program_loop->the_post(); ?>

<?php 
	$progtitle = get_the_title();
	if( strpos($progtitle, 'Healthcare Administration Bachelor of Science Online') !== false ) : $customclass = 'hca_bs'; endif;
	if( strpos($progtitle, 'Medical Administrative Assistant Diploma Online') !== false ) : $customclass = 'maa_diploma'; endif;
	if( strpos($progtitle, 'Medical Administrative Assistant Associate of Applied Science Online') !== false ) : $customclass = 'maa_assoc'; endif;
	if( strpos($progtitle, 'Medical Insurance Billing and Coding Diploma Online') !== false ) : $customclass = 'mibc_diploma'; endif;
	if( strpos($progtitle, 'Medical Insurance Billing and Coding Associate of Applied Science Online') !== false ) : $customclass = 'mibc_assoc'; endif;
	if( strpos($progtitle, 'Business Administration') !== false ) : $customclass = 'busadmin'; endif;
	if( strpos($progtitle, 'Business Administration and Leadership Diploma Online') !== false ) : $customclass = 'busadminled_diploma'; endif;
	if( strpos($progtitle, 'Business Administration and Leadership Associate of Applied Science Online') !== false ) : $customclass = 'busadminled_assoc'; endif;
	if( strpos($progtitle, 'Digital Marketing') !== false ) : $customclass = 'digmkt'; endif;
	if( strpos($progtitle, 'Information Technology') !== false ) : $customclass = 'itsupport'; endif;
	if( strpos($progtitle, 'Project Management') !== false ) : $customclass = 'pmgt'; endif;
	if( strpos($progtitle, 'Criminal Justice') !== false ) : $customclass = 'cj'; endif;
	if( strpos($progtitle, 'Logistics') !== false ) : $customclass = 'logistics'; endif;
	if( strpos($progtitle, 'Bookkeeping') !== false ) : $customclass = 'bookkeeping'; endif;



	$title=get_the_title();
	$degree='';
	$prog_title='';
	if(strpos($title,"Bachelor of Science Online")!==false){
		$prog_title=str_replace("Bachelor of Science Online","",$title);
		$degree="Bachelor of Science Online";
	}
	if(strpos($title,"Associate of Applied Science")!==false){
		$prog_title=str_replace("Associate of Applied Science","",$title);
		$degree="Associate of Applied Science";
	}
	if(strpos($title,"Diploma Online")!==false){
		$prog_title=str_replace("Diploma Online","",$title);
		$degree="Diploma Online";
	}
	if(strpos($title,"Associate of Applied Science Online")!==false){
		$prog_title=str_replace("Associate of Applied Science Online","",$title);
		$degree="Associate of Applied Science Online";
	}
	if(strpos($title,"in Information Technology Support Technician")!==false){
		$prog_title="Information Technology Support Technician";
		$degree="Associate of Applied Science Online";
	}
	if(strpos($title,"Digital Marketing")!==false){
		$prog_title="Digital Marketing";
		$degree="Associate of Applied Science Online";
	}
?>

	<div class="col-md-6 custom-program <?= $customclass; ?>" id="post-<?php the_ID(); ?>">
		<div>
			<a href="<?php the_permalink(); ?>"><img src="<?php echo get_field( 'icon_image', $post->ID);?>" alt="Program Icon"></a>
		</div>
		<div class="original">
			<a href="<?php the_permalink(); ?>"><?=$prog_title; ?></a><BR>
			<span class="degreelevel"><?=$degree; ?></span>
			<div class="program-divider"></div>
			<p>Course Length: <strong><?php echo get_field( 'course_length');?> </strong></p>
		</div>
                                                    
		<a class="card" href="<?php the_permalink(); ?>">
			<span>
				<p class="progtitle"><?=$prog_title; ?></p>
				<p class="degreelevel"><?=$degree; ?></p>
				<p>Course Length: <strong><?php echo get_field('course_length');?> </strong></p>
			</span>
		</a>
	</div>
<?php endwhile; ?>
</div>
	<p><BR/><i class="fa fa-thumb-tack" aria-hidden="true"></i>
	Vista College Online Campus is not accepting enrollments in the following programs at this time:</p>
		<ul class="teachoutlink">
			<li><a href="/degree-programs/business/business-administration-leadership-diploma-online/">Business Administration and Leadership Diploma Online</a></li>
			<li><a href="/degree-programs/legal/criminal-justice-associate-of-applied-science-online/">Criminal Justice Associate of Applied Science Online</a></li>
			<li><a href="/degree-programs/healthcare/medical-administrative-assistant-diploma-online/">Medical Administrative Assistant Diploma</a></li>
			<li><a href="/degree-programs/healthcare/medical-insurance-billing-coding-diploma-online/">Project Management Bachelor of Science</a></li>
			<li><a href="/degree-programs/healthcare/medical-insurance-billing-coding-associate-of-applied-science-online/">Medical Insurance Billing and Coding Diploma</a></li>
		</ul>


                                        <?php wp_reset_postdata(); endif; ?>

                                </div>

                            </div>
                            <div class="online-programs-footer">
                                <p>Vista College online campus is accredited by Accrediting Commission of Career Schools and Colleges</p>
                            </div>

                        </div>

                <?php endif; ?>
            </div>
            
        </div>
        
        <?php if( get_field('te_enable'.$GLOBALS['device']) ): ?>
            <div class="content group online-testimonial">
            
                <?php if(get_field('te_heading'.$GLOBALS['device'])): ?>
                    <h2 class="section-title sectiondiv"><?php the_field('te_heading'.$GLOBALS['device']); ?></h2>
                <?php endif; ?>
                
                <?php if( get_field('te_testimonial'.$GLOBALS['device']) ): ?>
                
                    <?php $testimonials = get_field('te_testimonial'.$GLOBALS['device']); ?>

                    <div class="grid grid-3" <?php if(wp_is_mobile()): ?>id="slideshow-5"<?php endif; ?>>
                                
                        <div class="grid-content">
                            
                            <?php foreach( $testimonials as $post): ?>

                                <?php setup_postdata($post); ?>

                                <div class="grid-box">
                        
                                    <div class="testimonial" <?php if( get_field('te_image_fe') ): ?>style="background-image: url(<?php  the_field('te_image_fe'); ?>);"<?php endif; ?>>
                                
                                        <?php if( get_field('te_fe_video') ): ?>
                                        
                                            <input type="hidden" class="video-iframe" value='<?php the_field('te_fe_video') ?>'/>
                                            
                                            <a class="player-btn fa fa-play-circle" aria-hidden="true"></a>
                                        
                                        <?php endif; ?>
                                
                                        <div>
                                        
                                            <?php if( get_field('te_testimonial_fe') ): ?>
                                                <p>"<?php the_field('te_testimonial_fe'); ?>"</p>
                                            <?php endif; ?>
                                            
                                            <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                                <h4><?php the_field('te_name'); ?><br/> <?php the_field('te_campus'); ?></h4>
                                            <?php endif; ?>
                                            
                                        </div>
                                    
                                    </div>
                                    
                                </div>
                            
                            <?php endforeach; ?>

                        </div>
                        
                    </div>
                
                <?php wp_reset_postdata(); endif; ?>
                
            </div>
        <?php endif; ?>

        <div class="online-info-header"></div>
    </div>
    
    <?php include "footer-online.php";?>

</div>

<?php global $onlinePage; $onlinePage= true; ?>
<?php get_footer(); ?>