<?php get_header(); ?>


<div class="section section-7 test">
        
    <div class="content group">
    
        <aside class="left-side">
            <div class="subnav-button">
                <a class="fa fa-navicon"></a>
                <h3>Categories</h3>
            </div>
    
            <div class="subnav categories">
                <?php wp_nav_menu( array('menu' => 'blog')); ?>
            </div>
        
        </aside>
    
        <div class="center-content">
        
            <?php if(have_posts()) : ?>

                <div class="search-results">
            
                    <?php while (have_posts()) : the_post(); ?>
                    
                        <?php 
                            if(wp_is_mobile() && get_field('de_enable')){
                                $GLOBALS['device'] = "_mo"; 
                            }
                            else {
                                $GLOBALS['device'] = "";
                            }
                        ?>
                        
                        <div class="search-result group">
                            
                            <a href="<?php the_permalink(); ?>">
                                <?php if(get_field('po_image'.$GLOBALS['device'])): ?>
                                    <?php $image = get_field('po_image'.$GLOBALS['device']); ?>
                                    <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                                <?php else: ?>
                                    <?php the_post_thumbnail( 'thumbnail' ); ?>
                                <?php endif; ?>
                            </a>
                            
                            <div <?php if(get_the_post_thumbnail() || get_field('po_image'.$GLOBALS['device'])): ?>class="post-excerpt"<?php endif; ?>>
                            
                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

                                 <span>Posted on <i><?php echo get_the_date(); ?></i></span>

                                <p>
                                    <?php excerpt(get_the_ID()); ?>
                                    <a href="<?php the_permalink(); ?>" class="link">Read More</a>
                                </p>
                                
                            </div>
                        
                        </div>
                        
                    <?php endwhile; ?>
                    
                </div>
        
                <?php if($wp_query->max_num_pages > 1) : ?>
            
                    <?php posts_nav(); ?>

                <?php endif; ?>

            <?php else: ?>

                <h3>Nothing Found</h3>

                <p>The category you are looking for is empty.</p>

                <a href="<?php echo site_url(); ?>" class="button">Return Home</a>

            <?php endif; ?>
            
        </div>
        
        <aside class="right-side">
    
            <?php if( have_rows('sidebar', 'option') ): ?>
             
                <?php while( have_rows('sidebar', 'option') ): the_row(); ?>
                
                    <?php if( get_sub_field('si_item', 'option') ): ?>
                    
                        <div class="sidebar-box">
                    
                            <?php the_sub_field('si_item', 'option'); ?>
                        </div>
                        
                    <?php endif; ?>
                    
                <?php endwhile; ?>
                 
            <?php endif; ?>
        
        </aside>
        
    </div>
    
</div>


<?php get_footer(); ?>