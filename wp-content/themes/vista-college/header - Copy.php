<?php

    if (isset($_GET['gclid'])) {
        $ls = $_GET['gclid'];
    } else if (isset($_GET['utm_ls'])) {
        $ls = $_GET['utm_ls'];
    } else {
        $ls = "normal";
    }

    if ($ls != "normal" && !isset($_COOKIE['source'])) {
        setcookie('source', $ls, time() + (86400 * 30), "/");
        if(!isset($_SESSION['source'])){
            session_start();
            $_SESSION['source']=$ls;
        }
    }
	
    if (isset($_GET['utm_placement'])) {
        $_SESSION['utm_placement']=$_GET['utm_placement'];
        $_SESSION['utm_source']=$_GET['utm_source'];
        $_SESSION['utm_campaign']=$_GET['utm_campaign'];
        $_SESSION['utm_content']=$_GET['utm_content'];
        $_SESSION['utm_device']=$_GET['utm_device'];
        $_SESSION['utm_phone']=$_GET['utm_phone'];
        $_SESSION['utm_term']=$_GET['utm_term'];
        $_SESSION['utm_adposition']=$_GET['utm_adposition'];
        $_SESSION['utm_medium']=$_GET['utm_medium'];
	}
?>

<!DOCTYPE HTML>
<html lang="en">

<head>

    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/> 
    <link rel="shortcut icon" href="<?php if(get_field('favicon', 'option')){the_field('favicon', 'option');} ?>"/>
    <?php wp_head(); ?>
    
    <?php if(get_field('script_header', 'option')){the_field('script_header', 'option');} ?>

    <?php if(get_field('he_script')){the_field('he_script');} ?>
    
<!-- Global Site Tag (gtag.js) - Google Analytics --->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-5227931-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-5227931-2');
</script>

</head>

<body <?php echo body_class('newnav'); ?> onload="var experience_id = getCookie('expid');">

    <nav class="mobile-nav">
        
        <div class="section">
        
            <div class="content">
            
                <?php wp_nav_menu( array('menu' => 'header')); ?>
                
            </div>
            
        </div>
        
    </nav>


    <div class="search-section">
        
        <div class="search-content">
        
            <div class="group">
                <a class="search-close fa fa-times" aria-hidden="true"></a>
            </div>
        
            <form id="search" action="/" autocomplete="off" method="get">
                <input class="search-input" type="text" name="s" value="<?php the_search_query(); ?>" placeholder="Start typing..." />
                <input class="search-submit" type="submit" value="Search" />
            </form>
            
            <h2 class="section-title"><b>Common</b> Searches</h2>
            
             <?php if( have_rows('common_searches', 'option') ): ?>
             
                <ul>

                    <?php while( have_rows('common_searches', 'option') ): the_row(); ?>

                        <?php if( get_sub_field('cs_text', 'option') ): ?>
                            <li><a href="<?php if(get_sub_field('cs_page', 'option')){the_sub_field('cs_page', 'option');} ?>"><?php the_sub_field('cs_text', 'option'); ?></a></li>
                        <?php endif; ?>
                        
                    <?php endwhile; ?>
                
                </ul>
                 
            <?php endif; ?>
            
        </div>
    
    </div>
    
    
    <div class="video-player">
    
        <a class="video-close fa fa-times" aria-hidden="true"></a>
    
        <div class="video-center">
        
            <div class="video-content"></div>
        
        </div>
    
    </div>
    
    
    <?php if(!is_front_page()): ?>
    
        <div class="page-banner" style="background-image: url(<?php if(get_field('bn_image'.$GLOBALS['device'])){the_field('bn_image'.$GLOBALS['device']);}else{ echo get_template_directory_uri() . "/img/main-banner.jpg";} ?>);">
            
            <?php include "header-content.php"; ?>
            
            <div class="banner-content">
            
                <div class="content">
                
                    <?php if(get_the_ID() == 24): ?>
                    
                        <h4>Classes Start Every 5 Weeks!</h4>
                        
                    <?php endif; ?>
                    
                    <?php if(is_search()): ?>
                    
                        <h1>Search Results</h1>
                        
                    <?php elseif(is_404()): ?>
                        
                        <h1>404</h1>
                        
                    <?php elseif(is_category()): ?>
                        
                        <h1><?php echo single_cat_title(); ?></h1>
                        
                    <?php else: ?>
                    
                        <?php if (get_field('degree_type')): ?>
                            <h3><?=get_field('degree_type');?> in</h3>
                        <?php endif; ?>
                        <h1>
                        <?php
                            global $post;
                            $short_obj = get_post_meta($post->ID, 'rp_short_title');
                            $short_title = $short_obj[0] == "" ? get_the_title() : $short_obj[0];
                            echo $short_title;
                        ?>
                        </h1>
                        <input type="hidden" name="page_title" id="page_title" value="<?php echo get_the_title();?>">
                    <?php endif; ?>
                    
                    <?php if ($post->post_type == 'programs' && $post->post_parent) include "request-form.php";?>
                    
                </div>
                
            </div>
        
        </div>
        
        <div class="section-breadcrumb">
        
            <div class="content">
    
                <?php get_breadcrumb(); ?>
                
            </div>
            
        </div>
        
    <?php endif; ?>
    

    <div class="page-content">
        
        <?php get_sidebar(); ?>