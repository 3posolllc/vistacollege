<?php
/*
  Template Name: Vista Ground Learning Programs
  Template Post Type: post, page, projects, code, programs
 */
?>
<?php get_header(); ?>
<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">

<?php if ($post->post_parent) : ?>
    <?php
    $starting_dates = get_field('default_starting_dates', 'option');
    if ($starting_dates) {
        $starting_dates = explode(',', $starting_dates);
    } else {
        $starting_dates = [];
    }
    $today = date('Y-m-d');
    $starting_date = '';
    foreach ($starting_dates as $value) {
        $date_obj = explode('-', $value);
        $date = date('Y-m-d', mktime(0, 0, 0, $date_obj[0], $date_obj[1], $date_obj[2]));
        if ($date >= $today) {
            $starting_date = date('F j', mktime(0, 0, 0, $date_obj[0], $date_obj[1], $date_obj[2]));
            break;
        }
    }
    ?>
    <?php if (have_rows('ta_tabs')) : ?>
        <?php $content_array = array(); ?>
        <?php $x = 1;
        while (have_rows('ta_tabs')) : the_row(); ?>
            <?php global $post; ?>
            <?php if (have_rows('ta_sections')) : ?>
                <?php while (have_rows('ta_sections')) : the_row(); ?>
                    <?php if (get_sub_field('ta_description')) : ?>
                        <?php $content_array[] = get_sub_field('ta_description'); ?>
                        </div>
                    <?php endif; ?>
                <?php endwhile; ?>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>
    <div class="online-content  vista-online-body">
        <div class="online-section content section-1">
            <div class="row sectionrow">
                <div class=" <?php if (get_field('tech_out')) {
                                    echo 'col-xs-12';
                                } else {
                                    echo 'col-xs-12 col-sm-12 col-md-6 col-lg-8';
                                } ?> leftcontent">
                    <?php echo @$content_array[0]; ?>
                </div>
				
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 rightcontent <?php if (get_field('tech_out')) echo 'hideempty'; ?>">
                    <div class="fast-facts fast-facts-new">
                        <div class="ff-inner">
                            <h4>Fast Facts</h4>
                            <hr class="fastfacts">
                            <div <?php if (!get_field('course_length')) echo ' class="hideempty"'; ?>>
                                <strong>Course Length:</strong> <?= get_field('course_length'); ?> *
                            </div>
                            <div <?php if (!get_field('class_hours')) echo ' class="hideempty"'; ?>>
                                <strong>Class Hours:</strong> <?= get_field('class_hours'); ?>
                            </div>
                            <div <?php if (!get_field('lab_hours')) echo ' class="hideempty"'; ?>>
                                <strong>Lab Hours:</strong> <?= get_field('lab_hours'); ?>
                            </div>
                            <div <?php if (!get_field('clinical_hours')) echo ' class="hideempty"'; ?>>
                                <strong>Clinical Hours:</strong> <?= get_field('clinical_hours'); ?>
                            </div>
							
						<?php
							$onlinecontent=get_field('online_plus_learning_model_content');
							$blendedcontent=get_field('blended_learning_model_content');
							$tradcontent=get_field('traditional_learning_model_content');
							if($onlinecontent!='') {$campuslearn[]="an Online Plus"; $learn=" learning format";}
							if($tradcontent!='') {$campuslearn[]="a Traditional";  $learn=" learning format";}
							if($blendedcontent!='') {$campuslearn[]="new Blended learning format, which combines flexible online classes with hands-on lab work"; $learn="";}
							
						?>
                            <hr class="fastfacts">
                            <div>
                                <strong>Campus:</strong><br>
								<?php if(count($campuslearn)<2){ ?>
			<p class="factstext">Course is offered in <?php echo $campuslearn[0]; ?> learning format.</p>
								<?php } else { ?>

                                <p class="factstext">Course is offered in either <?php echo $campuslearn[0]; ?>  or our <?php echo $campuslearn[1]; echo $learn; ?>.</p>
								<?php } ?>
                                <p id="campus-error">Please select a campus</p>
                                <div id="campur-select-form">
                                    <?php $campuses_loop = new WP_Query(array('post_type' => 'campus', 'meta_query' => array(array('key' => 'av_programs', 'value' => '"' . get_the_ID() . '"', 'compare' => 'LIKE'))));
                                    $learning_campuses = get_field('choose_learning_model_for_each_campus');
                                    if ($campuses_loop->have_posts()) : ?>
                                        <?php while ($campuses_loop->have_posts()) : $campuses_loop->the_post(); ?>
                                            <div class="d-flex justify-content-between cam-list">
                                                <div class="d-flex">
                                                    <input type="radio" name="campus_select" value="<?php $value = get_the_ID() != 26 ? the_title() : 'Online Campus';
                                                                                                    echo $value; ?>"><?php the_title(); ?>
                                                </div>
                                                <?php $model_campus_id = 'model_campus_'.get_the_ID(); ?> 
                                                <?php if (array_key_exists($model_campus_id, $learning_campuses) && 'online plus' == $learning_campuses[$model_campus_id]) : ?>
                                                     <span class="online-plus">Online Plus</span>
                                                <?php endif; ?>  
												<?php if (array_key_exists($model_campus_id, $learning_campuses) && 'traditional' == $learning_campuses[$model_campus_id]) : ?>
                                                    <span class="traditional-learning">Traditional</span>
                                                <?php endif; ?>   
												<?php if (array_key_exists($model_campus_id, $learning_campuses) && 'blended' == $learning_campuses[$model_campus_id]) : ?>
                                                     <span class="bleneded-learning">Blended</span>
                                                <?php endif; ?> 
												                                             
                                            </div>
                                        <?php endwhile; ?>
                                    <?php wp_reset_postdata();
                                    endif; ?>
                                </div>
                            </div>
                            <a class="get-more-info-new online-get-started" onclick="_gaq.push(['_trackEvent', 'RFI', 'Click', 'NEXT']);">GET STARTED</a>
                            <div class="fastfactdisclaimer">
                                <small>* Based on successful, full time enrollment</small>
                            </div>
                        </div>
                        <div class="fastfact-footer">
                            <div>Next Start Date</div>
                            <div class="cl-start-date"><?php echo $starting_date; ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-content-section content">
            <div class="content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="list-group tab-buttons" id="myList" role="tablist">
                           <?php
								
								if($onlinecontent!=''){
							?>
                            <a class="list-group-item list-group-item-action tabb-btn" data-toggle="list" href="#tab_3" role="tab">
                                <?php echo esc_html_e('Online Plus Learning Model', 'vista-college'); ?>
                            </a>
							<?php } ?>
							<?php
								if($blendedcontent!=''){
							?>
							<a class="list-group-item list-group-item-action tabb-btn" data-toggle="list" href="#tab_1" role="tab">
                                <?php echo esc_html_e('Blended Learning Model', 'vista-college'); ?>
                            </a>
							<?php } ?>
							<?php
								if($tradcontent!=''){
							?>
                            <a class="list-group-item list-group-item-action tabb-btn" data-toggle="list" href="#tab_2" role="tab">
                                <?php echo esc_html_e('Traditional Learning Model', 'vista-college'); ?>
                            </a>
							<?php } ?>
							
                        </div>
                        
                        <div class="tab-content">
                            <div class="tab-pane" id="tab_1" role="tabpanel">
                                <?php echo the_field('blended_learning_model_content'); ?>
                            </div>
                            <div class="tab-pane" id="tab_2" role="tabpanel">
                                <?php echo the_field('traditional_learning_model_content'); ?>
                            </div>
                            <div class="tab-pane" id="tab_3" role="tabpanel">
                                <?php echo the_field('online_plus_learning_model_content'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<div id="learningmodels" class="course-popup">
	<h3>Ground Campus Learning Models</h3>
	<table class="program-tf">
<tbody><tr>
	<th></th>
<th>Traditional</th>
<th><strong>Blended</strong> <br>

(On-Campus and Online)
</th>
<th>Online Plus</th>
</tr>
<tr>
<td><strong>Location/
Content Delivery Method
</strong></td>
<td>Traditional face-to-face on-campus theory and lab instruction. </td>
<td>Combination of on-campus and online.  </td>
<td>Online courses that may be accessed from anywhere.  </td>
</tr>
<tr>
<td><strong>Meeting Times
</strong></td>
<td>Classes take place on scheduled days and times.</td>
<td>Portions of the course are live real-time virtual instruction with other students at specific days/times.  Other portions are independent activities that may be completed at any time each week. Most laboratory portions of the course must be completed at the campus. </td>
<td>Portions of the course are live real-time remote instruction with other students at specific days/times. Other portions are independent activities that may be completed at any time each week.  Most laboratory segments are completed online. </td>
</tr>
<tr>
<td><strong>Labs
</strong></td>
<td>On-campus labs allow for hands-on learning with experienced instructors and your peers. </td>
<td>On-campus labs allow for hands-on learning with experienced instructors and your peers. </td>
<td>Most practice labs are completed online, enabling real-world experience.  </td>
</tr>
<tr>
<td><strong><strong>Externship</strong> <br>
(Healthcare programs only)
</strong></td>
<td colspan="3">Students are required to complete an externship in an applicable healthcare workplace environment. </td>
</tr>
<tr>
<td><strong><strong>Canvas Learning Management System
</strong></td>
<td colspan="3">The Canvas LMS provides students 24/7 access to all course materials and information.  Students will access assignments as well as submit work through Canvas. Students will also be able to view course schedules, access grades, and have student-to-student or student-to-teacher conversations.</td>
</tr>
</tbody></table>
</div>		
<div id="readmore" class="course-popup">
            <h2>Blended Learning FAQs</h2>
           
            <div class="faqhead">What kind of program structure can I expect from a blended learning model?</div>
<div>All of our blended learning courses are 90 hours, taught over five weeks. Each one consists of</div>
<ul>
  <li>20 hours of online  faculty-led lectures (also called synchronous learning) which occur on specific  days and times, Monday through Thursday</li>
  <li>25 hours of  asynchronous learning which includes learning activities, conducted in an  online education environment, that aren&rsquo;t scheduled for specific times and can  be done more according to your schedule </li>
  <li>45 hours of  instructor-led lab time taken at the campus</li>
</ul>

 <div class="faqhead">What is the online course structure?</div>
<div>Vista uses the online learning management system Canvas, which is where you’ll access all your blended learning course content and links to any other information or software you need. You’ll utilize McGraw Hill’s Connect software to complete activities and assignments. For online, real-time lectures, we utilize Zoom, which allows you to see your instructors, view any shared visuals and classroom materials, and break into small groups to interact with others for group projects and discussions.</div>

<div class="faqhead">Will I need  to attend at specific times in a blended learning program?</div>
<div>Yes, some components of both our online and on-campus  hybrid courses have some set times you&rsquo;ll need to attend. These include:</div>

<table class="program-tf">
<tbody><tr>
	<th>Components</th>
<th>Distance  Education – Blended Schedule</th>
</tr>
<tr>
<td class="centertd"><strong>Theory</strong></td>
<td><strong>4 hours per week</strong> - 2 hours twice weekly Synchronous/Scheduled in Zoom
 </td>
</tr>
<tr>
<td class="centertd"><strong>Lab</strong></td>
<td><strong>9 hours per week</strong> - 4.5 hours twice weekly	Synchronous/Scheduled
</td>
</tr>
<tr>
<td class="centertd"><strong>Independent Student Learning Activities</strong></td>
<td><strong>5 hours per week</strong> - Work time determined by the student outside of class time, Monday – Friday at 4:00 p.m. (CT) Asynchronous/Unscheduled
 </td>
</tr>
<tr>
<td class="centertd"><strong>Total Weekly Attendance</strong></td>
<td><strong>18 hours</strong>
</td>
</tr>
</tbody></table>

<div>Attendance is expected and recorded for each hour.</div>

<div class="faqhead">How will I  interact with my instructors</div>
<div>
  The great thing about Zoom is it implements a lot of  the features of a classroom. Multiple users are able to interact at the same  time, you&rsquo;ll be able to ask questions and participate in discussions, and  there&rsquo;s also a chat feature where you can interact with your blended learning  course instructors and classmates. Your instructor may even use a feature that  allows for small breakout groups of students.</div>

<div class="faqhead">What kind of  equipment will I need for the blended learning program?</div>
<div>
  Mostly, you&rsquo;ll need Internet access in order to  connect to the online components of your hybrid courses. We provide new  students with a Chromebook so you&rsquo;ll be assured of having a working laptop with  a microphone and a webcam. This will allow you to access the Canvas Learning  Management System, Zoom, and any other course materials. We make it a priority  to provide you with all software and ebooks required for the program.</div>

<div class="faqhead">Will I be  able to get tutoring if I need it?</div>
<div>Absolutely. Vista&rsquo;s blended learning model still  gives you access to our valuable support services. You&rsquo;ll be able to connect  with your instructors for online tutoring, either as an individual or in small  groups, through Zoom or telephone.</div>


                        </div>
        <div class="online-section orangesection healthcare-ba-orange">
            <div class="content">
                <div class="row">
                    <div class="col-lg-6 col-12  align-self-center">
                    	<style>
                    		.video-container {
							    overflow: hidden;
							    position: relative;
							    width:100%;
							}

							.video-container::after {
							    padding-top: 56.25%;
							    display: block;
							    content: '';
							}

							.video-container iframe {
							    position: absolute;
							    top: 0;
							    left: 0;
							    width: 100%;
							    height: 100%;
							}
						</style>
                    	<div class="video-container">
							<?php if(get_field('youtube_link')){ ?>
  <iframe src="<?= get_field('youtube_link'); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							<?php } else { ?>
							<style>
								.video-container{
    							background: url(<?= get_field('career_image');?>) no-repeat left;
								background-size: auto;
								background-size: auto;
								background-size: cover;}</style>
							<?php } ?>
</div>
                       
                    </div>
                    <div class="orange-right col-lg-6 col-12 align-self-center">
                        <?php echo @$content_array[1]; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="center-content-program">
        <?php echo do_shortcode('[fx_courses toggle=true]'); ?>
    </div>
    <?php if (have_rows('fc_content', 'option')) : ?>
        <?php if (!get_field('tech_out')) : ?>
            <div class="footer-top get-started">
                <div class="footer-top started-left">
                    <div class="footer-left-top">
                        <div class="left-side-top">
                            <div class="left-side-title">
                                <h1>Let's Get Started!</h1>
                                <span>Questions? An Admission Representative would love to help<span>
                            </div>
                        </div>
                        <div class="left-side-bottom">
                            <input type="hidden" name="online_status" id="online_status" value="<?php echo $online; ?>">
                            <select id="request_select">
                                <option value="">- Select Campus -</option>
                                <?php $campuses_loop = new WP_Query(array('post_type' => 'campus', 'meta_query' => array(array('key' => 'av_programs', 'value' => '"' . get_the_ID() . '"', 'compare' => 'LIKE'))));
                                if ($campuses_loop->have_posts()) : ?>
                                    <?php while ($campuses_loop->have_posts()) : $campuses_loop->the_post(); ?>
                                        <option value="<?php $value = get_the_ID() != 26 ? the_title() : 'Online Campus';
                                                        echo $value; ?>"><?php the_title(); ?></option>
                                    <?php endwhile; ?>
                                <?php wp_reset_postdata();
                                endif; ?>
                            </select>
                            <a class="request-btn" onClick="_gaq.push(['_trackEvent', 'RFI', 'Click', 'NextStep']);">Next</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>
<?php else : ?>
    <?php
    $args = array(
        'post_parent' => $post->ID,
        'post_type'   => 'programs',
        'numberposts' => -1,
        'post_status' => 'any'
    );
    $children = get_children($args);
    if (sizeof($children) > 0) {
        include "single-areas.php";
    } else {
        include "single-programs-update.php";
    }
    ?>
<?php endif; ?>
<style type="text/css">
::-webkit-scrollbar {
  background: #000000;
  border-radius: 4px;
  height: 8px;
  width: 8px;
}
.orange-right {
  padding-left: 30px;
  padding-bottom: 0px !important;
  padding-top: 25px !important;
}
::-webkit-scrollbar-thumb {
  background: #ad1119;
  border-radius: 4px;
}
#readmore h2 {
  text-transform: none !important;
}
#readmore ul {
  margin-bottom: 0px !important;
}
.faqhead {
  color: #ad1119 !important;
  font-weight: bold;
  padding-top: 20px;
}
.readup {
  color: #ad1119 !important;
}
.readup:hover {
  color: #339999 !important;
}
i.fas.fa-chevron-circle-right {
  font-family: 'FontAwesome';
  font-style: normal !important;
}
.list-group-item:last-child {
  border-radius: 0px !important;
  margin-bottom: -1px !important;
}
.tab-content-section {
  padding-top: 50px;
  padding-left: 0 !important;
}
.tab-content {
  border: none !important;
}
.factstext {
  margin-bottom: 16px;  
  font-size: 14px;
  line-height: 16px;
}
.fastfact-footer {
  font-size: 16px;
}
.cam-list {
  margin-bottom: 3px;
  font-size: 15px;
}
select {
  color: black !important;
}
.course-header {
  background-color: #151c38;
}
.course .course__num {
  color: #339999;
}
.get-started {
  background: url(/wp-content/themes/vista-college/img/footer-online-background.png) no-repeat;
}
.online-get-started {
  display: block;
  width: 100%;
  height: 40px;
  margin-top: 25px;
  margin-left: 0;
  border: 0;
  background: #339999;
  padding: 0;
  cursor: pointer;
  line-height: 40px;
  -webkit-appearance: none;
  border-radius: 0;
  font-weight: bold;
  z-index: 999;
}
.online-get-started:hover {
  background: #e97a6d;
}
#menu-header-1 .sub-menu {
  width: 239px !important;
}
.healthcare-ba-online-thumb2 {
  background: url(<?=get_field('career_image');?>) no-repeat left;
  background-size: cover;
  margin-top: -50px;
  margin-bottom: -50px;
}
#request_select {
  height: 40px;
  padding: 0 6px;
  min-width: 30%;
  border: 2px solid #dedbdb;
  border-radius: 2px;
  margin-bottom: 30px;
  max-width: 90%;
}
.request-btn {
  display: inline-block;
  margin-left: 0px !important;
  padding: 9px 20px;
  font-size: 20px !important;
  font-weight: bold;
  color: #fff;
  background: #339999;
  border: none;
  border-radius: 4px;
  text-transform: uppercase;
  cursor: pointer;
}
a.request-btn:hover {
  background-color: #e97a6d;
  color: #ffffff;
}
.orange-right {
  padding-top: 24px;
  padding-bottom: 24px;
}
.videoiframe {
  height: calc(100% + 24px) !important;
  min-height: 350px;
  margin-top: -12px;
}
input[type='checkbox'],
input[type='radio'] {
  margin: 5px 0 0;
}
@-moz-document url-prefix() {
  input[type='checkbox'],
  input[type='radio'] {
    margin: -3px 0 0;
  }
}
#campus-error {
  display: none;
  margin-bottom: 8px;
  color: red;
}
@media only screen and (max-width: 768px) {
  .fast-facts {
    margin: 0 auto;
  }
}
@media only screen and (max-width: 991px) {
  .videoiframe {
    margin: 12px auto;
  }
}
@media only screen and (max-width: 1338px) {
  .video-container {
    height: calc(100%) !important;
  }
}
@media only screen and (max-width: 991px) {
  .video-container {
    margin-top: 20px;
  }
  .tab-content-section,
  .tab-content {
    padding-bottom: 0px !important;
  }
  .healthcare-ba-orange {
    padding-top: 20px !important;
    margin-top: 20px !important;
  }
  .video-container {
    height: calc(100%) !important;
  }
}
@media (min-width: 994px) and (max-width: 1035px) {
  .cam-list {
    font-size: 13px;
  }
}

@media (max-width: 600px) {
  .sectionrow {
    margin-top: -20px;
  }
}

</style>
<?php global $onlinePage;
$onlinePage = true; ?>
<?php get_footer(); ?>