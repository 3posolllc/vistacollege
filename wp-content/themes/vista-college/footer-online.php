<div class="section footer-online">
    <div class="section online-info">
        
        <div class="content group">
            <div class="grid grid-4">
                <div class="grid-content">
                    <div class="grid-box">
                        <img src="<?php echo get_stylesheet_directory_uri().'/img/FP-Icon.png';?>">
                        <div class="footer-online-divider"></div>
                        <h4>We Welcome Military Learners</h4>
                        <p>Talk to us about how our programs can fit your unique needs. We can also assist with tuition benefits.</p>
                    </div>

                    <div class="grid-box">
                        <img src="<?php echo get_stylesheet_directory_uri().'/img/AL-Icon.png';?>">
                        <div class="footer-online-divider"></div>
                        <h4>Accelerated Learning</h4>
                        <p>Courses that focus on the skills employers seek. That's all. Complete your program quickly and pursue new opportunities.</p>
                    </div>

                    <div class="grid-box">
                        <img src="<?php echo get_stylesheet_directory_uri().'/img/SO-Icon.png';?>">
                        <div class="footer-online-divider"></div>
                        <h4>Schedule Options</h4>
                        <p>Take classes on your time, at your pace. Our ACCSC-accredited online coursework is designed for your busy schedule.</p>
                    </div>

                    <div class="grid-box">
                        <img src="<?php echo get_stylesheet_directory_uri().'/img/CO-Icon.png';?>">
                        <div class="footer-online-divider"></div>
                        <h4>Creating Opportunities</h4>
                        <p>Our mission is to design curriculum that will exceed the requirements of open jobs and give our students lifelong career opportunities.</p>
                    </div>                                                            
                </div>
            </div>
        </div>
    </div>

    <div class="section get-started">
        <div class="footer-top started-left">
            <div class="footer-left-top">
                <div class="left-side-top">
                    <div class="left-side-title">
                        <h1>Let's Get Started!</h1>
                        <span>Questions? An Admission Representative would love to help<span>
                    </div>
                </div>
                <div class="left-side-bottom">
                    <?php $online_status = get_the_ID() == 26 ? true : false; ?>
                    <input type="hidden" name="online_status" id="online_status" value="<?php echo $online_status; ?>">
                    <select id="program_select">
                        <option value="">Which Program are You Interested in?</option>
                        <?php $ids = get_field('av_programs'); ?>

                        <?php $program_loop = new WP_Query(array('post_type' => 'programs', 'post__in' => $ids, 'posts_per_page' => -1));
                        if ($program_loop->have_posts()) : ?>
                            <?php while ($program_loop->have_posts()) : $program_loop->the_post(); ?>

                                <option value="<?php the_title(); ?>"><?php the_title(); ?></option>

                            <?php endwhile; ?>
                            <?php wp_reset_postdata(); endif; ?>
                    </select><br>

                   <a class="next-request" href="<?php the_permalink(5530); ?>" onclick="_gaq.push(['_trackEvent', 'RFI', 'Click', 'NEXT']);">NEXT</a>
                </div>
            </div>
        </div>
    </div>

</div>