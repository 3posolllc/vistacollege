<?php /* Template name: Home */ ?>


<?php get_header(); ?>

<div class="section-1">

    <?php include "header-content.php"; ?>
    
    <div class="video-banner">
    	<div class="embed background" data-aspectratio="16x9">
    		<iframe src="https://fast.wistia.net/embed/iframe/sz62iv7dxn?videoFoam=true&autoplay=true&muted=true&endVideoBehavior=loop" title="Vista College" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="100%" height="100%"></iframe><script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>
    	</div>
    </div>
    
    <?php if( have_rows('sl_slides'.$GLOBALS['device']) ): ?>

        <div class="slideshow-banner" id="slideshow-1">
            
            <div class="banner-slides">
                
                <?php while( have_rows('sl_slides'.$GLOBALS['device']) ): the_row(); ?>
                    <?php 
                        $slide_status = true;
                        $today = date('d/m/Y');
                        $display_start_date = get_sub_field('display_start_date'.$GLOBALS['device']);
                        if ($display_start_date && $display_start_date > $today) {
                            $slide_status = false;
                        }
                        $display_end_date = get_sub_field('display_end_date'.$GLOBALS['device']);
                        if ($display_end_date && $display_end_date < $today) {
                            $slide_status = false;
                        }
                    ?>
                    <?php if ($slide_status): ?>
                        <div class="banner-slide" <?php if( get_sub_field('sl_background'.$GLOBALS['device']) ): ?>style="background-image: url(<?php  the_sub_field('sl_background'.$GLOBALS['device']); ?>);"<?php endif; ?>></div>
                    <?php endif;?>                    
                
                <?php endwhile; ?>

            </div>
            
        </div>
        
        <div class="slideshow-background"></div>
        
        <div class="slideshow-boxes">
        
            <div class="content group">
                
                <div class="slideshow-box">
                
                    <a class="fa fa-angle-left" id="prev"></a>
                    <a class="fa fa-angle-right" id="next"></a>
                    
                    <div class="slideshow-content" id="slideshow-2">
                
                        <div class="content-slides">
                        
                            <?php while( have_rows('sl_slides'.$GLOBALS['device']) ): the_row(); ?>
                                <?php 
                                    $slide_status = true;
                                    $today = date('d/m/Y');
                                    $display_start_date = get_sub_field('display_start_date'.$GLOBALS['device']);
                                    if ($display_start_date && $display_start_date > $today) {
                                        $slide_status = false;
                                    }
                                    $display_end_date = get_sub_field('display_end_date'.$GLOBALS['device']);
                                    if ($display_end_date && $display_end_date < $today) {
                                        $slide_status = false;
                                    }
                                ?>
                                <?php if ($slide_status): ?>
                                    <div class="content-slide group">
                                        
                                        <div class="slide-box">
                                        
                                            <div class="slide-center">
                                            
                                                <?php if(get_sub_field('sl_line_1'.$GLOBALS['device'])){the_sub_field('sl_line_1'.$GLOBALS['device']); echo "<br/>";} ?>
                                        
                                                <?php if(get_sub_field('sl_line_2'.$GLOBALS['device'])){the_sub_field('sl_line_2'.$GLOBALS['device']);} ?>
                                                
                                                <?php if(get_sub_field('sl_type'.$GLOBALS['device']) == "External"): ?>

                                                    <?php if(get_sub_field('sl_title'.$GLOBALS['device']) && get_sub_field('sl_link'.$GLOBALS['device'])): ?>
                                                    
                                                        <a href="<?php the_sub_field('sl_link'.$GLOBALS['device']); ?>" <?php if(get_sub_field('sl_click'.$GLOBALS['device'])): ?>onclick="<?php the_sub_field('sl_click'.$GLOBALS['device']); ?>"<?php endif; ?> target="_blank"><?php the_sub_field('sl_title'.$GLOBALS['device']); ?></a>
                                                        
                                                    <?php endif; ?>
                                                    
                                                <?php else: ?>

                                                    <?php if(get_sub_field('sl_page'.$GLOBALS['device'])): $id=get_sub_field('sl_page'.$GLOBALS['device']); ?>

                                                        <a href="<?php echo get_the_permalink($id); ?>" <?php if(get_sub_field('sl_click_cu'.$GLOBALS['device'])): ?>onclick="<?php the_sub_field('sl_click_cu'.$GLOBALS['device']); ?>"<?php endif; ?>>
                                                            <?php if(get_sub_field('sl_title_cu'.$GLOBALS['device'])){ the_sub_field('sl_title_cu'.$GLOBALS['device']);}else{ echo get_the_title($id);} ?>
                                                        </a>
                                                
                                                    <?php endif; ?>
                                                
                                                <?php endif; ?>
                                                
                                            </div>
                                        
                                        </div>
                                        
                                    </div>
                                <?php endif;?>
                                
                            <?php endwhile; ?>

                        </div>
                        
                    </div>
                
                </div>
                
                <div class="slideshow-form closed">
                    <div id="closeform"><a href="javascript:;">close</a></div>
                    <div class="slideshow-form-content">
                
                        <h2>Request Information</h2>
                        <?php
                            $starting_dates = get_field('default_starting_dates', 'option');
                            if ($starting_dates) {
                                $starting_dates = explode(',', $starting_dates);
                            } else {
                                $starting_dates = [];
                            }
                            $today = date('Y-m-d');
                            $starting_date = '';
                            foreach ($starting_dates as $value) {
                                $date_obj = explode('-', $value);
                                $date = date('Y-m-d', mktime(0,0,0,$date_obj[0],$date_obj[1],$date_obj[2]));
                                if ($date >= $today) {
                                    $starting_date = date('F j', mktime(0,0,0,$date_obj[0],$date_obj[1],$date_obj[2]));
                                    break;
                                }
                            }
                        ?>                        
                        <!--<h3>- Next Classes Begin <?php //echo $starting_date;?> -</h3>-->
                        <div class='showform'><a href='javascript:;'>Let's Get Started</a></div>
                        <?php echo do_shortcode("[gravityform id='1' title='false' description='false' ajax='false']"); ?>
                
                        <p class="request-info">
                            We <a href="<?php echo site_url(); ?>/privacy-statement/">Respect Your Privacy</a>
                        </p>
                
                    </div>
                	
                	<div class="start_date"> 
                		<p><i class="fa fa-calendar"></i> Classes Begin <?php echo $starting_date;?></p>
                	</div>
                	
                </div>
            
            </div>
            
        </div>
        
    <?php endif; ?>
    
</div>


<?php if( get_field('ca_enable'.$GLOBALS['device']) ): ?>

    <div class="section section-2">
            
        <div class="content group">
          <div class="da-vid-programs">
            <?php if(get_field('ca_heading'.$GLOBALS['device'])): ?>
                <h2 class="section-title"><?php the_field('ca_heading'.$GLOBALS['device']); ?></h2>
            <?php endif; ?>
            
            <?php if(get_field('ca_description'.$GLOBALS['device'])){the_field('ca_description'.$GLOBALS['device']);} ?>
        
            <div class="careers-form">

                <select id="careers">
                    
                    <option value="">Area of Interest</option>
                
                    <?php $area_loop = new WP_Query( array( 'post_type' => 'programs', 'post_parent' => 0, 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1 ) ); if ( $area_loop->have_posts() ) : ?>
                    
                        <?php while ( $area_loop->have_posts() ) : $area_loop->the_post(); ?>
                    
                            <option value="<?php the_permalink(); ?>"><?php the_title(); ?></option> 

                        <?php endwhile; ?>
                        
                    <?php wp_reset_postdata(); endif; ?>

                 </select>
                
                <a href="">View Program <i class="fa fa-angle-right"></i></a>
            
            </div>

            <div class="programs-form">
                <a href="https://www.vistacollege.edu/degree-programs/">View Programs</a>
                <a href="https://www.vistacollege.edu/campuses/">View Campuses</a>
            </div>
            
          </div>
          <div class="da-vid-img">
            <?php 
                $vid_data = array(
                    array('link'=> '<iframe width="854" height="480" src="https://www.youtube.com/embed/9KQgMCOBi5s?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>',
                        'image'=>'/img/weSeeGreatness-her.jpg'
                    ),
                    array('link'=> '<iframe width="854" height="480" src="https://www.youtube.com/embed/YMtuT0kCdCY?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>',
                        'image'=>'/img/weSeeGreatness-his.jpg'
                    )
                );
                $i = rand(0,1);
            ?>
            <input type="hidden" class="video-iframe" value='<?php echo $vid_data[$i]['link'];?>'/>
            <img alt="We See Greatness Video Link" src="<?php echo get_stylesheet_directory_uri().$vid_data[$i]['image'];?>" class="vid-player">
          </div>        

        </div>
        
        <div class="content group view-programs ">
            <div class="grid grid-3">
                <div class="grid-content">
                    <!--HEALTHCARE CARD-->
                    <div class="grid-box flex-child noleftpad">
                        <a href="https://www.vistacollege.edu/degree-programs/healthcare/">
                        <img src="https://www.vistacollege.edu/wp-content/uploads/2019/02/VIS_LP_Health-Icon_20181213_v002.png" class="prog-img skip-lazy"></a>
                        <h3 class="prog-title">HEALTHCARE</h3>
                        
                        <p class="prog-paragraph">
                            Discover a range of programs designed to teach you the skills you need, so you can pursue the career
                            of your dreams.</p>
                        <div class="prog-btn">
                            <a href="https://www.vistacollege.edu/degree-programs/healthcare/" style="width: 100%;">
                                <p class="btn-text">View Programs</p>
                            </a></div>
                    </div>
                    <!--BUSINESS CARD-->
                    <div class="grid-box flex-child">
                        <a href="https://www.vistacollege.edu/degree-programs/business/"><img src="https://www.vistacollege.edu/wp-content/uploads/2019/02/VIS_LP_Busi-Icon_20181213_v002.png" class="prog-img skip-lazy"></a>
                        <h3 class="prog-title">BUSINESS</h3>
                        <p class="prog-paragraph">Looking for career advancement? Do you aspire to begin a new career in the
                            world of business? Find education that prepares you to excel.</p>
                        <div class="prog-btn">
                            <a href="https://www.vistacollege.edu/degree-programs/business/">
                                <p class="btn-text">View Programs</p>
                            </a></div>
                    </div>
                    <!--TECH CARD-->
                    <div class="grid-box flex-child">
                        <a href="https://www.vistacollege.edu/degree-programs/technology/"><img src="https://www.vistacollege.edu/wp-content/uploads/2019/02/VIS_LP_Tech-Icon_20181213_v002.png" class="prog-img skip-lazy"></a>
                        <h3 class="prog-title">TECHNOLOGY</h3>
                        <p class="prog-paragraph">
                            Learn about emergent technology and skills from leaders in the IT industry to stay competitive and
                            secure career advancement opportunities.</p>
                        <div class="prog-btn">
                            <a href="https://www.vistacollege.edu/degree-programs/technology/">
                                <p class="btn-text">View Programs</p>
                            </a></div>
                    </div>
                    <!--LEGAL CARD-->
                    <div class="grid-box flex-child">
                    <a href="https://www.vistacollege.edu/degree-programs/legal/"><img src="https://www.vistacollege.edu/wp-content/uploads/2019/02/VIS_LP_Law-Icon_20181213_v002.png" class="prog-img skip-lazy"></a>
                        <h3 class="prog-title">LEGAL</h3>
                        <p class="prog-paragraph">
                            Our accelerated programs in legal fields focus on the skills you need in a competitive job
                            landscape.</p>
                        <div class="prog-btn">
                            <a href="https://www.vistacollege.edu/degree-programs/legal/">
                                <p class="btn-text">View Programs</p>
                            </a></div>
                    </div>
                    <!--COSMETOLOGY CARD-->
                    <div class="grid-box flex-child">
                    <a href="https://www.vistacollege.edu/degree-programs/cosmetology/"><img src="https://www.vistacollege.edu/wp-content/uploads/2019/02/VIS_LP_Cos-Icon_20181213_v002.png" class="prog-img skip-lazy"></a>
                        <h3 class="prog-title">COSMETOLOGY</h3>
                        <p class="prog-paragraph">
                            We combine hands-on salon experience and classroom education to give our students the training they
                            need to enter the cosmetology field with confidence.</p>
                        <div class="prog-btn">
                            <a href="https://www.vistacollege.edu/degree-programs/cosmetology/">
                                <p class="btn-text">View Programs</p>
                            </a></div>
                    </div>
                    <!--TRADES CARD-->
                    <div class="grid-box flex-child norightpad">
                    <a href="https://www.vistacollege.edu/degree-programs/trades/"><img src="https://www.vistacollege.edu/wp-content/uploads/2019/02/VIS_LP_Trades-Icon_20181213_v002.png" class="prog-img skip-lazy"></a>
                        <h3 class="prog-title">TRADES</h3>
                        <p class="prog-paragraph">Whether you’re looking to work in electrical, HVAC, construction, or other
                            trades, get hands-on instruction using real equipment.</p>
                        <div class="prog-btn">
                            <a href="https://www.vistacollege.edu/degree-programs/trades/">
                                <p class="btn-text">View Programs</p>
                            </a></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    
<?php endif; ?>


<?php if( get_field('in_enable'.$GLOBALS['device']) ): ?>

    <?php if( have_rows('in_informations'.$GLOBALS['device']) ): ?>

        <div class="section section-22">
            
            <div class="content group">
            
                <div class="grid grid-4">
                            
                    <div class="grid-content">
                    
                        <?php while( have_rows('in_informations'.$GLOBALS['device']) ): the_row(); ?>
                    
                            <div class="grid-box">
                                
                                <?php if(get_sub_field('in_heading'.$GLOBALS['device'])): ?>
                                    <h2><?php the_sub_field('in_heading'.$GLOBALS['device']); ?></h2>
                                <?php endif; ?>
                                
                                <div class="divider-1"></div>
                                
                                <?php if(get_sub_field('in_description'.$GLOBALS['device'])){the_sub_field('in_description'.$GLOBALS['device']);} ?>
                                
                                <?php if(get_sub_field('in_type'.$GLOBALS['device']) == "External"): ?>

                                    <?php if(get_sub_field('in_title'.$GLOBALS['device']) && get_sub_field('in_link'.$GLOBALS['device'])): ?>
                                    
                                        <a class="link" href="<?php the_sub_field('in_link'.$GLOBALS['device']); ?>" target="_blank"><?php the_sub_field('in_title'.$GLOBALS['device']); ?></a>
                                        
                                    <?php endif; ?>
                                    
                                <?php else: ?>

                                    <?php if(get_sub_field('in_page'.$GLOBALS['device'])): $id=get_sub_field('in_page'.$GLOBALS['device']); ?>

                                        <a class="link" href="<?php echo get_the_permalink($id); ?>">
                                            <?php if(get_sub_field('in_title_cu'.$GLOBALS['device'])){ the_sub_field('in_title_cu'.$GLOBALS['device']);}else{ echo get_the_title($id);} ?>
                                        </a>
                                
                                    <?php endif; ?>
                                
                                <?php endif; ?>

                            </div>
                        
                        <?php endwhile; ?>
                    
                    </div>
                    
                </div>

            </div>
            
        </div>
        
        <div class="section featuredprogram"> 
        	<div class="content group">
        		<div class="inner"> 
        			<div class="heading">Featured Program</div> 
        			<div class="progname">Logistics and Operations Management Associate Degree Online</div> 
        			<div class="more"><a class="buttonopen" href="https://www.vistacollege.edu/degree-programs/business/logistics-operations-management-associate-of-applied-science-online/">Learn More</a></div> 
        		</div>
        	</div>
        </div>
        
    <?php endif; ?>

<?php endif; ?>


<?php if( get_field('nu_enable'.$GLOBALS['device']) ): ?>

    <div class="section section-3" <?php if( get_field('nu_background'.$GLOBALS['device']) ): ?>style="background-image: url(<?php the_field('nu_background'.$GLOBALS['device']); ?>);"<?php endif; ?>>
        
        <div class="content group">
        
            <?php if(get_field('nu_heading'.$GLOBALS['device'])): ?>
                <h2 class="section-title"><?php the_field('nu_heading'.$GLOBALS['device']); ?></h2>
            <?php endif; ?>
            
            <?php if( have_rows('nu_numbers'.$GLOBALS['device']) ): ?>
        
                <div class="grid grid-4">
                            
                    <div class="grid-content">
                    
                        <?php while( have_rows('nu_numbers'.$GLOBALS['device']) ): the_row(); ?>
                    
                            <div class="grid-box">
                                
                                <?php if(get_sub_field('nu_number'.$GLOBALS['device'])): ?>
                                    <h3><?php the_sub_field('nu_number'.$GLOBALS['device']); ?></h3>
                                <?php endif; ?>
                                
                                <p><?php if(get_sub_field('nu_text'.$GLOBALS['device'])){the_sub_field('nu_text'.$GLOBALS['device']);} ?></p>
                                
                            </div>
                        
                        <?php endwhile; ?>
                    
                    </div>
                    
                </div>
            
            <?php endif; ?>

        </div>
        
    </div>

<?php endif; ?>


<?php if( get_field('op_enable'.$GLOBALS['device']) ): ?>

    <div class="section section-4">
        
        <div class="content group">

            <div class="half-slideshow" style="display:none;">
        
                <a class="fa fa-angle-left" id="featured-prev"></a>
                <a class="fa fa-angle-right" id="featured-next"></a>
                
                <h3>New Programs & Highlights</h3>
            
                <div class="news-content">
                
                    <?php if( have_rows('op_opportunities'.$GLOBALS['device']) ): ?>
                
                        <div class="news-slideshow" id="slideshow-4">
                            
                            <div class="news-slides">
                            
                                <?php while( have_rows('op_opportunities'.$GLOBALS['device']) ): the_row(); ?>

                                    <div class="news-slide group">
                                    
                                        <?php if(get_sub_field('op_page'.$GLOBALS['device'])): $id=get_sub_field('op_page'.$GLOBALS['device']); ?>
                                        
                                            <div class="slide-box">
                                                <?php echo get_the_title($id); ?><br/>
                                                <?php if(get_sub_field('op_information'.$GLOBALS['device'])){ echo the_sub_field('op_information'.$GLOBALS['device']);} ?>
                                                <a href="<?php echo get_the_permalink($id); ?>">Learn More</a>
                                            </div>
                                            
                                        <?php endif; ?>
                                        
                                    </div>
                                        
                                <?php endwhile; ?>

                            </div>
                            
                        </div>
                    
                    <?php endif; ?>
                    
                </div>
                
            </div>

            <div class="half-slideshow centeralign">
        
                <a class="fa fa-angle-left" id="news-prev"></a>
                <a class="fa fa-angle-right" id="news-next"></a>
                
                <h3>News & Articles</h3>
            
                <div class="news-content">
                
                    <div class="news-slideshow" id="slideshow-3">
                        
                        <div class="news-slides">
                        
                            <?php query_posts('post_type=post&cat=6&posts_per_page=10'); ?>
                
                            <?php if(have_posts()) : ?>

                                <?php while (have_posts()) : the_post(); ?>

                                    <div class="news-slide group">
                                        
                                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                        
                                        <p>
                                            <?php excerpt(get_the_ID()); ?>
                                            <a href="<?php the_permalink(); ?>" class="link">Read More</a>
                                        </p>
                                        
                                    </div>
                                    
                                <?php endwhile; ?>

                            <?php wp_reset_query(); endif; ?>

                        </div>
                        
                    </div>
                    
                </div>
                
            </div>

        </div>
        
    </div>

<?php endif; ?>


<?php if( get_field('te_enable'.$GLOBALS['device']) ): ?>

    <div class="section section-5">
        
        <div class="content group">
        
            <div class="grey-border"></div>
        
            <?php if(get_field('te_heading'.$GLOBALS['device'])): ?>
                <h2 class="section-title"><?php the_field('te_heading'.$GLOBALS['device']); ?></h2>
            <?php endif; ?>
            
            <?php if( get_field('te_testimonial'.$GLOBALS['device']) ): ?>
            
                <?php $testimonials = get_field('te_testimonial'.$GLOBALS['device']); ?>

                <div class="grid grid-3" id="slideshow-5">
                            
                    <div class="grid-content">
                        
                        <?php foreach( $testimonials as $post): ?>

                            <?php setup_postdata($post); ?>

                            <div class="grid-box">
                    
                                <div class="testimonial" <?php if( get_field('te_image_fe') ): ?>style="background-image: url(<?php  the_field('te_image_fe'); ?>);"<?php endif; ?>>
                            
                                    <?php if( get_field('te_fe_video') ): ?>
                                    
                                        <input type="hidden" class="video-iframe" value='<?php the_field('te_fe_video') ?>'/>
                                        
                                        <a class="player-btn fa fa-play-circle" aria-hidden="true"></a>
                                    
                                    <?php endif; ?>
                            
                                    <div>
                                    
                                        <?php if( get_field('te_testimonial_fe') ): ?>
                                            <p>"<?php the_field('te_testimonial_fe'); ?>"</p>
                                        <?php endif; ?>
                                        
                                        <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                            <h4><?php the_field('te_name'); ?><br/> <?php the_field('te_campus'); ?></h4>
                                        <?php endif; ?>
                                        
                                    </div>
                                
                                </div>
                                
                            </div>
                        
                        <?php endforeach; ?>

                    </div>
                    
                </div>
            
            <?php wp_reset_postdata(); endif; ?>
            
        </div>
        
    </div>
    
<?php endif; ?>
    <div class="section get-started">
        
        <div class="footer-top started-left">
            <div class="footer-left-top">
                <div class="left-side-top">
                    <div class="left-side-title">
                        <h1>Let's Get Started!</h1>
                        <span>Questions? An Admission Representative would love to help<span>
                    </div>
                </div>
                <div class="left-side-bottom">
                   <a class="next-request" href="<?php the_permalink(5530); ?>" onclick="_gaq.push(['_trackEvent', 'RFI', 'Click', 'NEXT']);">NEXT</a>
                </div>
            </div>
        </div>

    </div>

<?php get_footer(); ?>