<?php get_header(); ?>


<div class="section section-7 sub-programs">

    <div class="content group testimonial-wrap">

        <div class="group">
            
            <div class="left-content botomtab">
            
                <?php $online_loop = new WP_Query( array( 'p' => 26, 'post_type' => 'campus' ) ); if ( $online_loop->have_posts() ) : ?>
        
                    <?php while ( $online_loop->have_posts() ) : $online_loop->the_post(); ?>

                        <?php if(get_field('av_programs')): ?>
                            
                            <?php $online_ids = get_field('av_programs'); ?>

                        <?php endif; ?>

                    <?php endwhile; ?>
                        
                <?php endif; wp_reset_postdata(); ?>
            
                <ul class="page-navigation group">
                    <li class="tab-btn-1 active"><a onclick="tab(1, false);">Ground Campus Programs</a></li>
                    <?php if(!empty($online_ids)): ?>
                        <li class="tab-btn-2" id="campustab"><a onclick="tab(2, false);">Online Campus Programs</a></li>
                    <?php endif; ?>
                </ul>
                
                <ul class="page-navigation vertical mobile-tabs group">
                    <li class="tab-btn-1 active"><a onclick="tab(1, false);">Ground Campus Programs</a></li>
                </ul>
       
                <div class="tab" id="tab-1">
                    <?php $parent = get_the_ID();
                        $program_loop = new WP_Query( array( 'post_type' => 'programs', 'post_parent' => $parent, 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1 ) ); 
                        if($program_loop->have_posts()) : ?>
                            <div class="detail-box">
                                <ul>
                                    <?php while ( $program_loop->have_posts() ) : $program_loop->the_post(); ?>
                                    
                                        <?php if (!in_array(get_the_ID(), $online_ids)): ?>
                                            <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?><?php echo ' ' . get_field('degree_type');?></a></li>
                                        <?php endif; ?>
                                    
                                    <?php endwhile; ?>
                                </ul>
                            </div>

                            <div class="sub-programs-footer">
                                <p>Vista College ground campuses are accredited by <b>Council on Occupational Education</b></p>
                            </div>                        
                        <?php endif; wp_reset_postdata(); ?>
                
                </div>
                
                <?php if(!empty($online_ids)): ?>
                    <ul class="page-navigation vertical mobile-tabs group">
                        <li class="tab-btn-2"><a onclick="tab(2, false);">Online Campus Programs</a></li>
                    </ul>
                <?php endif; ?>
                
                <div class="tab" id="tab-2">
                    
                    <?php $program_loop = new WP_Query( array( 'post_type' => 'programs', 'post_parent' => $parent, 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1 ) ); 
                    if($program_loop->have_posts()) : ?>
                        <div class="detail-box">
                            <ul>
                                <?php $onlineprog=0; while ( $program_loop->have_posts() ) : $program_loop->the_post(); ?>
                                
                                    <?php if (in_array(get_the_ID(), $online_ids)): ?>
                                        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>										                                        
                                    <?php 
										$onlineprog=1; 
									endif; ?>
                                
                                <?php endwhile; ?>
                                <?php if($onlineprog=='0'){ ?>
<style type="text/css">
  #campustab{ display:none; }
</style>
                                <?php
									}
								?>
                            </ul>
                        </div>
                       
                        <div class="sub-programs-footer">
                            <p>Vista College online campus is accredited by <b>Accrediting Commission of Career Schools and Colleges</b></p>
                        </div>
                    <?php endif; wp_reset_postdata(); ?>                    
                
                </div>
            
            </div>
            
            <aside class="left-side right campus">
            
                <div class="program-areased">
            
                <iframe width="590" height="400" src="https://www.youtube.com/embed/GRzA6OQ9j2g" frameborder="0" allowfullscreen></iframe>
                    <!-- <h2> <b>Areas</b></h2>
                    
                    <div>
                    
                        <p>We offer career training that will help you start in a new rewarding career in a matter of months.</p>
            
                        <?php $area_loop = new WP_Query( array( 'post_type' => 'areas', 'posts_per_page' => -1 ) ); if ( $area_loop->have_posts() ) : ?>

                            <ul>

                                <?php while ( $area_loop->have_posts() ) : $area_loop->the_post(); ?>
                                
                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                                <?php endwhile; ?>
                                
                            </ul>
                            
                        <?php wp_reset_postdata(); endif; ?>
                        
                    </div> -->
                
                </div>
            
            </aside>
        
        </div>

    </div>

    <div class="content">
        <div class="programs-intro group">

            <?php if( have_rows('ta_tabs'.$GLOBALS['device']) ): ?>
                <div class="left-content">
                    <?php $x=1; while( have_rows('ta_tabs'.$GLOBALS['device']) ): the_row(); ?>
                    
                        <div id="tab-<?php echo $x; ?>">
                        
                            <?php if( have_rows('ta_sections'.$GLOBALS['device']) ): ?>
                            
                                <?php while( have_rows('ta_sections'.$GLOBALS['device']) ): the_row(); ?>
                            
                                    <?php if(get_sub_field('ta_description'.$GLOBALS['device'])): ?>
                                
                                        <div class="detail-box">
                                                
                                            <?php the_sub_field('ta_description'.$GLOBALS['device']); ?>
                                            
                                        </div>
                                        
                                    <?php endif; ?>
                                    
                                <?php endwhile; ?>
                            
                            <?php endif; ?>
                        
                        </div>
                    
                    <?php $x++; endwhile; ?>
                </div>
            <?php endif; $testimonials = get_field('ft_testimonials');?>
			
            <aside class="right-side" <?php if(empty($testimonials)){echo "style='display:none'";} ?>>
            
                <?php if( have_rows('ft_testimonials'.$GLOBALS['device']) ): ?>
                
                    <?php while( have_rows('ft_testimonials'.$GLOBALS['device']) ): the_row(); ?>
            
                        <?php if( get_sub_field('ft_testimonial'.$GLOBALS['device']) ): ?>
                            
                            <?php $post = get_sub_field('ft_testimonial'.$GLOBALS['device']); setup_postdata($post); ?>
                            
                                <div class="featured-testimonial">
                                
                                    <div class="testimonial" <?php if( get_field('te_image_fe') ): ?>style="background-image: url(<?php the_field('te_image_fe'); ?>);"<?php endif; ?>>
                                        
                                        <?php if( get_field('te_fe_video') ): ?>
                                            
                                            <input type="hidden" class="video-iframe" value='<?php the_field('te_fe_video') ?>'/>
                                            
                                            <a class="player-btn fa fa-play-circle" aria-hidden="true"></a>
                                        
                                        <?php endif; ?>
                                        
                                        <a href="<?php the_permalink(144); ?>" class="testimonials-link"><i class="fa fa-angle-right"></i>View more Stories</a>
                                    
                                        <div>
                                    
                                            <?php if( get_field('te_testimonial_fe') ): ?>
                                                <p>"<?php the_field('te_testimonial_fe'); ?>"</p>
                                            <?php endif; ?>
                                            
                                            <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                                <h4><?php the_field('te_name'); ?><br/> <?php the_field('te_campus'); ?></h4>
                                            <?php endif; ?>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                </div>

                            <?php wp_reset_postdata(); ?>
                            
                        <?php endif; ?>
                        
                    <?php endwhile; ?>
                
                <?php else: ?>
                
                    <?php $tag = sanitize_title(get_the_title()); ?>
                
                    <?php query_posts("post_type=testimonials&tag=$tag&posts_per_page=5"); ?>
                
                        <?php if(have_posts()) : ?>
                        
                            <?php while (have_posts()) : the_post(); ?>
                            
                                <div class="featured-testimonial">
                                
                                    <div class="testimonial" <?php if( get_field('te_image_fe') ): ?>style="background-image: url(<?php the_field('te_image_fe'); ?>);"<?php endif; ?>>
                                        
                                        <?php if( get_field('te_fe_video') ): ?>
                                            
                                            <input type="hidden" class="video-iframe" value='<?php the_field('te_fe_video') ?>'/>
                                            
                                            <a class="player-btn fa fa-play-circle" aria-hidden="true"></a>
                                        
                                        <?php endif; ?>
                                        
                                        <a href="<?php the_permalink(144); ?>" class="testimonials-link"><i class="fa fa-angle-right"></i>View more Stories</a>
                                    
                                        <div>
                                    
                                            <?php if( get_field('te_testimonial_fe') ): ?>
                                                <p>"<?php the_field('te_testimonial_fe'); ?>"</p>
                                            <?php endif; ?>
                                            
                                            <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                                <h4><?php the_field('te_name'); ?><br/> <?php the_field('te_campus'); ?></h4>
                                            <?php endif; ?>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            
                            <?php endwhile; ?>
                        
                        <?php endif; ?>
                                    
                    <?php wp_reset_query(); ?>
                
                <?php endif; ?>
            </aside>           
        </div>
    </div>
        
</div>


<?php if( have_rows('fc_content', 'option') ): ?>

    <div class="section section-8" <?php if( get_field('fc_background', 'option') ): ?>style="background-image: url(<?php  the_field('fc_background', 'option'); ?>);"<?php endif; ?>>
        
        <div class="content group">

            <div class="career-areas">
                
                <div class="grid grid-3">
                        
                    <div class="grid-content">
                    
                        <?php while( have_rows('fc_content', 'option') ): the_row(); ?>
                    
                            <div class="grid-box">
                            
                                <div>
                                
                                    <?php if(get_sub_field('fc_heading', 'option')): ?>
                                        <h3 class="section-title"><?php the_sub_field('fc_heading', 'option'); ?></h3>
                                    <?php endif; ?>
                                    
                                    <?php if(get_sub_field('fc_description', 'option')){the_sub_field('fc_description', 'option');} ?>
                                    
                                    <?php if(get_sub_field('fc_type', 'option') == "External"): ?>

                                        <?php if(get_sub_field('fc_title', 'option') && get_sub_field('fc_link', 'option')): ?>
                                        
                                            <a class="link" href="<?php the_sub_field('fc_link', 'option'); ?>" target="_blank"><?php the_sub_field('fc_title', 'option'); ?></a>
                                            
                                        <?php endif; ?>
                                        
                                    <?php else: ?>

                                        <?php if(get_sub_field('fc_page', 'option')): $id=get_sub_field('fc_page', 'option'); ?>

                                            <a class="link" href="<?php echo get_the_permalink($id); ?>">
                                                <?php if(get_sub_field('fc_title_cu', 'option')){ the_sub_field('fc_title_cu', 'option');}else{ echo get_the_title($id);} ?>
                                            </a>
                                    
                                        <?php endif; ?>
                                    
                                    <?php endif; ?>
                                    
                                </div>
                                
                            </div>

                        <?php endwhile; ?>
                    
                    </div>
                    
                </div>
            
            </div>

        </div>
        
    </div>

<?php endif; ?>


<?php get_footer(); ?>