

<!-- ======================================== -->
<!-- =====  Template for Blog Loop  ======= -->
<!-- ======================================== -->
<?php get_header(); ?>


<div class="main-container container blog-loop">
  <section class="latest-posts__container">
    <div class="title-zone__container row">
    <hgroup class="title-container col-12 col-sm-8 col-lg-10">
      <h2 class="latest-posts__title">The <strong>latest</strong></h2>
      <h4 class="latest-post__subtitle">Stay updated with the most recent posts from Vista College</h4>
    </hgroup>
    <div class="three-col-loop__view-more col-12 col-sm-4 col-lg-2">
          <a href="https://www.vistacollege.edu/blog/careers/" class="btn btn--orange">View More</a>
</div>

    <?php get_template_part( 'partials/blog', 'latest-loop' ); ?>

  </section>


  <!-- three-col-loop - Cat 9 -->
  <?php $career_catID = 9; ?>
  <section class="three-col-loop__container">
    <div class="title-zone__container row">
      <hgroup class="title-group col-12 col-sm-8 col-lg-10">
        <h2 class="three-col-loop__title">Career <strong>Research</strong></h2>
        <h4 class="three-col-loop__subtitle">Discover the next path in your career</h4>
      </hgroup>
      <!-- Read more link -->
        <div class="three-col-loop__view-more col-12 col-sm-4 col-lg-2">
          <a href="<?php echo esc_url(get_category_link($career_catID)); ?>" class="btn btn--orange">View More</a>
        </div>
    </div>

    <?php
      set_query_var( 'blog-loop__category--3-items',  ['id' => $career_catID,
                                                      'showLink' => false
                                                    ]);

      get_template_part( 'partials/blog', 'three-col-loop' );
    ?>

  </section>

  <!-- Online Learning - Cat 7 -->
    <?php $learning_catID = 7; ?>
  <section class="three-col-loop__container">
    <div class="title-zone__container row">
      <hgroup class="title-group col-12 col-sm-8 col-lg-10">
        <h2 class="three-col-loop__title">Digital <strong>Learning</strong></h2>
        <h4 class="three-col-loop__subtitle">Online learning resources for students</h4>
      </hgroup>
      <!-- Read more link -->
        <div class="three-col-loop__view-more col-12 col-sm-4 col-lg-2">
          <a href="<?php echo esc_url(get_category_link($learning_catID)); ?>"
            class="btn btn--orange">View More</a>
        </div>
    </div>

    <?php
      set_query_var( 'blog-loop__category--3-items',  ['id' => $learning_catID,'showLink' => false]);

      get_template_part( 'partials/blog', 'three-col-loop' );
    ?>


  </section>


  <!-- Tabs  -->
  <!--
      Healthcare - cat 10
      Business - cat 11
      IT - cat 12
      Trade - cat 13
      Legal - cat 14
      Cosmetology - cat 175
  --> 

<div class="title-zone__container row">
      <hgroup class="title-group col-12 col-sm-8 col-lg-10">
        <h2 class="three-col-loop__title">Closer <strong>Look</strong></h2>
        <h4 class="three-col-loop__subtitle">Go in-depth into the program areas that interest you most</h4>
      </hgroup>
</div>
  <section class="blog-loop-cats__container soft-ends">
      <div class="row">

            <div class="blog-loop-cats__table col-sm-12">
              <ul class="js-tabs blog-loop-cats__tab-group">
              <li class="blog-loop-cats__tab active"><a href="#tab-healthcare">Healthcare</a></li>
              <li class="blog-loop-cats__tab"><a href="#tab-business">Business</a></li>
              <li class="blog-loop-cats__tab"><a href="#tab-IT">IT</a></li>
              <li class="blog-loop-cats__tab"><a href="#tab-trade">Trade</a></li>
              <li class="blog-loop-cats__tab"><a href="#tab-legal">Legal</a></li>
        <li class="blog-loop-cats__tab"><a href="#tab-cosmetology">Cosmetology</a></li>
              </ul>
              <div class="blog-loop-cats__tab-content active" id="tab-healthcare">
                <?php
                  set_query_var( 'blog-loop__category--3-items',  ['id' => 10, 'showLink' => true]);

                  get_template_part( 'partials/blog', 'three-col-loop' );
                ?>
              </div>
              <div  class="blog-loop-cats__tab-content" id="tab-business">
                <?php
                  set_query_var( 'blog-loop__category--3-items',  ['id' => 11, 'showLink' => true]);

                  get_template_part( 'partials/blog', 'three-col-loop' );
                ?>
              </div>
              <div  class="blog-loop-cats__tab-content" id="tab-IT">
                <?php
                  set_query_var( 'blog-loop__category--3-items',  ['id' => 12, 'showLink' => true]);

                  get_template_part( 'partials/blog', 'three-col-loop' );
                ?>
              </div>
              <div  class="blog-loop-cats__tab-content" id="tab-trade">
                <?php
                  set_query_var( 'blog-loop__category--3-items',  ['id' => 13, 'showLink' => true]);

                  get_template_part( 'partials/blog', 'three-col-loop' );
                ?>
              </div>
              <div  class="blog-loop-cats__tab-content" id="tab-legal">
                <?php
                  set_query_var( 'blog-loop__category--3-items',  ['id' => 14, 'showLink' => true]);

                  get_template_part( 'partials/blog', 'three-col-loop' );
                ?>
              </div>
              <div  class="blog-loop-cats__tab-content" id="tab-cosmetology">
                <?php
                  set_query_var( 'blog-loop__category--3-items',  ['id' => 175, 'showLink' => true]);

                  get_template_part( 'partials/blog', 'three-col-loop' );
                ?>
              </div>

        </div>
      </div>

  </section>


</div>


<?php get_footer(); ?>

<script type="text/javascript">
    // Adds bg image.
    jQuery( document ).ready(function() {
      jQuery('.page-content').addClass('section-7');
    })

</script>
