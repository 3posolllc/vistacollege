<?php /* Template name: News */ ?>


<?php get_header(); ?>

    
    <div class="section section-7">
    
        <div class="content group">
        
            <aside class="left-side">

                <?php if( have_rows('na_navigation'.$GLOBALS['device']) ): ?>
    
                    <ul class="categories">
                    
                        <?php while( have_rows('na_navigation'.$GLOBALS['device']) ): the_row(); ?>
                         
                            <?php if(get_sub_field('na_type'.$GLOBALS['device']) == "External"): ?>
                            
                                <?php if(get_sub_field('na_title'.$GLOBALS['device']) && get_sub_field('na_link'.$GLOBALS['device'])): ?>
                                
                                    <li>
                                        <a href="<?php the_sub_field('na_link'.$GLOBALS['device']); ?>" target="_blank"><?php the_sub_field('na_title'.$GLOBALS['device']); ?></a>
                                    </li>
                                    
                                <?php endif; ?>
                                
                            <?php else: ?>
                            
                                <?php if(get_sub_field('na_page'.$GLOBALS['device'])): $id=get_sub_field('na_page'.$GLOBALS['device']); ?>
                            
                                    <li>
                                        <a href="<?php echo get_the_permalink($id); ?>"><?php echo get_the_title($id); ?></a>
                                    </li>
                            
                                <?php endif; ?>
                            
                            <?php endif; ?>
                        
                        <?php endwhile; ?>
                        
                    </ul>

                <?php elseif(child_pages() != ""): ?>
                
                    <?php echo child_pages(); ?>
                    
                <?php else: ?>
                
                    <?php wp_nav_menu( array('menu' => 'footer-1')); ?>
                    
                <?php endif; ?>
            
            </aside>
        
            <div class="center-content">
            
                <?php if(get_field('co_content'.$GLOBALS['device'])){the_field('co_content'.$GLOBALS['device']);} ?>
                
                <?php query_posts('post_type=post&cat=147&posts_per_page=10&paged='.$paged); ?>
        
                <?php if(have_posts()) : ?>
                
                    <div class="search-results">
                    
                        <?php while (have_posts()) : the_post(); ?>
                            
                            <?php 
                                if(wp_is_mobile() && get_field('de_enable')){
                                    $GLOBALS['device'] = "_mo"; 
                                }
                                else {
                                    $GLOBALS['device'] = "";
                                }
                            ?>
                            
                            <div class="search-result group">
                                
                                <a href="<?php the_permalink(); ?>">
                                    <?php if(get_field('po_image'.$GLOBALS['device'])): ?>
                                        <?php $image = get_field('po_image'.$GLOBALS['device']); ?>
                                        <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                                    <?php else: ?>
                                        <?php the_post_thumbnail( 'thumbnail' ); ?>
                                    <?php endif; ?>
                                </a>
                                
                                <div <?php if(get_the_post_thumbnail() || get_field('po_image'.$GLOBALS['device'])): ?>class="post-excerpt"<?php endif; ?>>
                                
                                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    
                                    <span>Posted on <i><?php echo get_the_date(); ?></i></span>
                                    
                                    <p>
                                        <?php excerpt(get_the_ID()); ?>
                                        <a href="<?php the_permalink(); ?>" class="link">Read More</a>
                                    </p>
                                
                                </div>
                            
                            </div>
                            
                        <?php endwhile; ?>
                    
                    </div>
            
                    <?php if($wp_query->max_num_pages > 1) : ?>
                
                        <?php posts_nav(); ?>

                    <?php endif; ?>

                <?php else: ?>

                    <h3>Nothing Found</h3>

                    <p>The category you are looking for is empty.</p>

                    <a href="<?php echo site_url(); ?>" class="button">Return Home</a>

                <?php endif; ?>
                
                <?php wp_reset_query(); ?>
                
            </div>
            
            <aside class="right-side">
            
                <?php if( get_field('ft_testimonial'.$GLOBALS['device']) ): ?>
            
                    <?php $post=get_field('ft_testimonial'.$GLOBALS['device']); setup_postdata($post); ?>
                    
                    <div class="featured-testimonial">
                    
                        <div class="testimonial" <?php if( get_field('te_image_fe') ): ?>style="background-image: url(<?php  the_field('te_image_fe'); ?>);"<?php endif; ?>>
                            
                            <?php if( get_field('te_fe_video') ): ?>
                                
                                <input type="hidden" class="video-iframe" value='<?php the_field('te_fe_video') ?>'/>
                                
                                <a class="player-btn fa fa-play-circle" aria-hidden="true"></a>
                            
                            <?php endif; ?>
                            
                            <a href="<?php echo site_url(); ?>/vista-experience/testimonials/" class="testimonials-link"><i class="fa fa-angle-right"></i>View more Stories</a>
                        
                            <div>
                        
                                <?php if( get_field('te_testimonial_fe') ): ?>
                                    <p>"<?php the_field('te_testimonial_fe'); ?>"</p>
                                <?php endif; ?>
                                
                                <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                    <h4><?php the_field('te_name'); ?><br/> <?php the_field('te_campus'); ?></h4>
                                <?php endif; ?>
                                
                            </div>
                            
                        </div>
                        
                    </div>

                <?php wp_reset_postdata(); endif; ?>
        
                <div class="program-areas">
                
                    <h2>Program <b>Areas</b></h2>
                    
                    <div>
                    
                        <p>We offer career training that will help you start in a new rewarding career in a matter of months.</p>
            
                        <?php $area_loop = new WP_Query( array( 'post_type' => 'programs', 'post_parent' => 0, 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1 ) ); if ( $area_loop->have_posts() ) : ?>

                            <ul>

                                <?php while ( $area_loop->have_posts() ) : $area_loop->the_post(); ?>
                                
                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                                <?php endwhile; ?>
                                
                            </ul>
                            
                        <?php wp_reset_postdata(); endif; ?>
                        
                    </div>
                
                </div>
            
            </aside>
            
        </div>
        
    </div>

		
<?php get_footer(); ?>