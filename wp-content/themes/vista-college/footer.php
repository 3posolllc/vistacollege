    </div>
    
    <?php global $onlinePage;?>
    <?php if(!is_front_page() && !$onlinePage): ?>
    
        <div class="section section-4 featured-posts">
        
            <div class="content group">
            
                <a class="fa fa-angle-left" id="news-prev"></a>
                <a class="fa fa-angle-right" id="news-next"></a>
            
                <div class="news-content">
                
                    <div class="news-slideshow" id="slideshow-3">
                        
                        <div class="news-slides">
                        
                            <?php 
                                $t = "&cat=6";
                                if(get_field('rp_posts')){
                                    $t = "&tag=" . sanitize_title(get_field('rp_posts'));
                                }
                            ?>

                            <?php $x=1; while($x<=5): ?>

                                <?php query_posts("post_type=post$t&posts_per_page=2&paged=$x"); ?>
            
                                <?php if(have_posts()) : ?>
                                
                                    <div class="news-slide-box group">

                                        <?php while (have_posts()) : the_post(); ?>
                                        
                                            <?php 
                                                if(wp_is_mobile() && get_field('de_enable')){
                                                    $GLOBALS['device'] = "_mo"; 
                                                }
                                                else {
                                                    $GLOBALS['device'] = "";
                                                }
                                            ?>

                                            <div class="news-slide group">
                                            
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php if(get_field('po_image'.$GLOBALS['device'])): ?>
                                                        <?php $image = get_field('po_image'.$GLOBALS['device']); ?>
                                                        <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                                                    <?php else: ?>
                                                        <?php the_post_thumbnail( 'thumbnail' ); ?>
                                                    <?php endif; ?>
                                                </a>
                                                
                                                <div class="news-slide-excerpt">
                                                
                                                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                                    
                                                    <p>
                                                        <?php excerpt(get_the_ID()); ?>
                                                        <a href="<?php the_permalink(); ?>" class="link">Read More</a>
                                                    </p>
                                                    
                                                </div>

                                            </div>
                                            
                                        <?php endwhile; ?>
                                        
                                    </div>

                                <?php endif; ?>
                                
                                <?php wp_reset_query(); ?>
                                
                            <?php $x++; endwhile; ?>

                        </div>
                        
                    </div>
                    
                </div>

            </div>
            
        </div>
    
    <?php endif; ?>

    
    <footer class="section">
    
        <div class="content group">
    
            <div class="footer-box">
            
                <?php if( get_field('logo_dark', 'option') ): ?>
                    <a href="<?php echo site_url(); ?>">
                        <?php $logo = get_field('logo_dark', 'option'); ?>
                        <img src="<?php echo $logo['sizes']['thumbnail']; ?>" alt="<?php echo $logo['alt']; ?>" />
                    </a>
                <?php endif; ?>
                
                <?php if( get_field('description', 'option') ): ?>
                
                    <div class="footer-description">
                    
                        <?php the_field('description', 'option'); ?>
                    
                    </div>
                
                <?php endif; ?>
                
            </div>
            
            <div class="footer-box">
                
                <?php wp_nav_menu( array('menu' => 'footer-1')); ?>
                
            </div>
            
            <div class="footer-box">
                
                <?php wp_nav_menu( array('menu' => 'footer-2')); ?>
                
            </div>
            
            <div class="footer-box footer-social">
            
                <p>&copy; <?php echo date("Y"); ?> Vista College</p>
        
                <ul class="social-icons">
                    <?php if( get_field('facebook', 'option') ): ?>
                        <li>
                            <a class="fa fa-facebook-square" href="<?php the_field('facebook', 'option'); ?>" target="_blank"></a>
                        </li>
                    <?php endif; ?>
                    <?php if( get_field('youtube', 'option') ): ?>
                        <li>
                            <a class="fa fa-youtube-square" href="<?php the_field('youtube', 'option'); ?>" target="_blank"></a>
                        </li>
                    <?php endif; ?>
                    <?php if( get_field('google', 'option') ): ?>
                        <li>
                            <a class="fa fa-google-plus-square" href="<?php the_field('google', 'option'); ?>" target="_blank"></a>
                        </li>
                    <?php endif; ?>
                    <?php if( get_field('twitter', 'option') ): ?>
                        <li>
                            <a class="fa fa-twitter-square" href="<?php the_field('twitter', 'option'); ?>" target="_blank"></a>
                        </li>
                    <?php endif; ?>
                    <?php if( get_field('instagram', 'option') ): ?>
                        <li>
                            <a class="instagram" href="<?php the_field('instagram', 'option'); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
                        </li>
                    <?php endif; ?>
                    <?php if( get_field('linkedin', 'option') ): ?>
                        <li>
                            <a class="fa fa-linkedin-square" href="<?php the_field('linkedin', 'option'); ?>" target="_blank"></a>
                        </li>
                    <?php endif; ?>
                    <?php if( get_field('pinterest', 'option') ): ?>
                        <li>
                            <a class="fa fa-pinterest-square" href="<?php the_field('pinterest', 'option'); ?>" target="_blank"></a>
                        </li>
                    <?php endif; ?>
                </ul>
            
            </div>
        
        </div>
        
    </footer>
    
    
    <div class="fixed-info">
    
        <div class="content group">
        
            <ul>

                <li>
                    <a href="<?php the_permalink(5530); ?>" class="buttond" onClick="_gaq.push(['_trackEvent', 'RFI', 'Click', 'Footerbar']);"><span class="button-helper"></span>Request Info</a>
                </li>
            
                <li>
                    <a href="<?php the_permalink(5530); ?>" class="buttond" onClick="_gaq.push(['_trackEvent', 'RFI', 'Click', 'Footerbar']);"><span class="button-helper"></span>Request Info</a>
                </li>
                
                <?php
                   $postid = get_the_ID();
                   switch($postid){
                       case '5759':
                           $chaturl='online-2-2';
                           break;
                       case '116':
                           $chaturl='online-2-2-1';
                           break;
                       case '24':
                           $chaturl='online-2-2-2';
                           break;
                       case '62':
                           $chaturl='online-2-2-1-1-1';
                           break;
                        case '9254':
                            $chaturl='online-2-2-2-8c6899f4';
                            break;
                        case '9295':
                            $chaturl='online-2-2-2-8c6899f4-0e66dc24';
                            break;
                        case '106':
                            $chaturl='online-2-2-2-8c6899f4-0e66dc24-d9f587ab';
                            break;
                        case '5762':
                            $chaturl='online-2-2-2-8c6899f4-0e66dc24-d9f587ab-f6042103';
                            break;
                        case '5530':
                            $chaturl='online-2-2-2-8c6899f4-0e66dc24-d9f587ab-f6042103-5107395d-14695feb'; //new code
                            break;
                        case '46':
                            $chaturl='online-2-2-2-8c6899f4-0e66dc24-d9f587ab-f6042103-5107395d';
                            break;
                       default:
                           $chaturl='online-2';
                           break;
                   }
                   $cstDate = gmdate('G') - 5;
                   $chatLink = ($cstDate >= 9 && $cstDate <= 21) ? "#$chaturl" : "#offline";
                    $currentTime = time('-5 hours');
                    $fromTime1 = mktime(15, 0, 0, 12, 21, 2018);
                    $toTime1 = mktime(0, 0, 0, 12, 26, 2018);
                    $fromTime2 = mktime(0, 0, 0, 1, 1, 2019);
                    $toTime2 = mktime(0, 0, 0, 1, 2, 2019);
                    if (($currentTime >= $fromTime1 && $currentTime < $toTime1) || ($currentTime >= $fromTime2 && $currentTime < $toTime2)) {
                        $chatLink = "#offline";
                    }
                ?>
                <li class="get-chat footer-chat">
                    <a class="drift-open-chat" href="<?php echo $chatLink;?>"><i class="fa fa-comment"></i><span> Chat Now</span></a>
                </li>
               
                 <?php if(get_field('phone', 'option') && get_field('phone_link', 'option')): ?>
                    <li class="get-chat">
                        <a class="phone-desktop" href="tel:<?php the_field('phone_link', 'option'); ?>"><i class="fa fa-phone"></i><span> <?php the_field('phone', 'option'); ?></span></a>
                        <a class="phone-mobile" href="tel:<?php the_field('phone_link', 'option'); ?>"><i class="fa fa-phone"></i><span> Call Us</span></a>
                    </li>
                <?php endif; ?>
                <li class="footer-social-group"><!--footer social icons-->
                    <ul class="footer-social-icons">
                        <?php if( get_field('pinterest', 'option') ): ?>
                            <li>
                                <a class="fa fa-pinterest-square" href="<?php the_field('pinterest', 'option'); ?>" target="_blank"></a>
                            </li>
                        <?php endif; ?>
                        <?php if( get_field('linkedin', 'option') ): ?>
                            <li>
                                <a class="fa fa-linkedin-square" href="<?php the_field('linkedin', 'option'); ?>" target="_blank"></a>
                            </li>
                        <?php endif; ?>
                        <?php if( get_field('instagram', 'option') ): ?>
                            <li>
                                <a class="instagram" href="<?php the_field('instagram', 'option'); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
                            </li>
                        <?php endif; ?>
                        <?php if( get_field('twitter', 'option') ): ?>
                            <li>
                                <a class="fa fa-twitter-square" href="<?php the_field('twitter', 'option'); ?>" target="_blank"></a>
                            </li>
                        <?php endif; ?>
                        <?php if( get_field('google', 'option') ): ?>
                            <li>
                                <a class="fa fa-google-plus-square" href="<?php the_field('google', 'option'); ?>" target="_blank"></a>
                            </li>
                        <?php endif; ?>
                        <?php if( get_field('youtube', 'option') ): ?>
                            <li>
                                <a class="fa fa-youtube-square" href="<?php the_field('youtube', 'option'); ?>" target="_blank"></a>
                            </li>
                        <?php endif; ?>
                        <?php if( get_field('facebook', 'option') ): ?>
                            <li>
                                <a class="fa fa-facebook-square" href="<?php the_field('facebook', 'option'); ?>" target="_blank"></a>
                            </li>
                        <?php endif; ?>                
                    </ul>
                </li>
            </ul>
        
        </div>
    
    </div>
    
    
    <?php if(get_the_ID() != 5530): ?>
    
        <div class="request-form">
        
            <div class="request-center">
            
                <div class="request-content">
                
                    <!-- <a class="request-close fa fa-times" aria-hidden="true"></a> -->
                
                    <div class="request-left">
                    
                        <h3>Request More Information</h3>
                        
                        <ul class="request-progress group">
                            <li class="request-back active-progress">Step 1 <span>Your Interests</span></li>
                            <li>Step 2 <span>Your Information</span></li>
                        </ul>
                        
                        <div class="request-form-full">
                        
                            <?php echo do_shortcode("[gravityform id='2' title='false' description='false' ajax='false']"); ?>
                        
                            <p class="request-info">
                                We <a href="<?php echo site_url(); ?>/privacy-statement/">Respect Your Privacy</a>
                            </p>
                            
                        </div>
                    
                    </div>
                                    
                    <div class="request-right">
                        <a class="request-close fa fa-times" aria-hidden="true"></a>
                        <!-- <h2>Why Choose <b>Vista College?</b></h2> -->
                        
                        <!-- <div class="divider-1"></div> -->  
                        <div class="text-middle">
                        <p>By submitting this form, I agree that Vista College may use this information to contact me by methods I provided and consented, including phone (both mobile or home, dialed manually or automatically), social media, email, mail and text message.</p>
                        </div>
                        <!-- <a class="link" href="//vista-college.com/our-programs/">View Our Programs</a> -->
                        
                    </div>

                </div>
            
            </div>
        
        </div>
    
    <?php endif; ?>
    
    <?php if ($post->post_type == 'programs' && $post->post_parent): ?>
        <div id="programModal" class="modal">
          <div class="modal-content">
          <span class="close cursor" id="closeProgramModal">&times;</span>
            <img src="https://421adeda4f5398838124-4a6f88f3967fda9431176351f921614c.ssl.cf1.rackcdn.com/global/imagelib/vista_college/vis_lp_ion-challenger-lps_20180802-logo.png" alt="logo">
            <p>By submitting this form, I am giving express written consent to receive text messages and/or telephone calls from or on behalf of Vista College at the phone number(s) I provided. I understand that standard text and/or usage rates may apply and that I am not required to provide consent as a condition of any sale of a good or service.<br>
<br>
We <a href="/privacy-statement/" target="_blank" style="font-size:13px">Respect Your Privacy</a>.</p>
          </div>
        </div>
        <div class="mobile_form_backdrop"></div>
    <?php endif; ?>    
    
    <?php if(isset($_POST['input_8'])): ?>

        <script>
        
            jQuery(document).ready(function(){
            
                jQuery('#part-1').css("display","none");
                jQuery('#part-2, #gform_wrapper_2 .gform_footer').css("display","block");
                jQuery('.request-progress li:first-child').removeClass("active-progress");
                jQuery('.request-progress li:last-child').addClass("active-progress");
                jQuery(".request-form").css("display","table");
            
            });

           /* jQuery('.sbi_photo.sbi_imgLiquid_bgSize.sbi_imgLiquid_ready').click(function() {
            jQuery('#popUpModal .modal-body').load(jQuery(this).data('href'), function(e) {
            jQuery('#popUpModal').modal('show')
            });
            });
           */
        
        </script>
        
        <?php if(get_the_ID() == 5530): ?>
        
            <script>
        
                jQuery(document).ready(function(){
                
                    jQuery('html, body').animate({
                        scrollTop: jQuery("#request-page").offset().top - 30
                    }, 10);
                
                });
            
            </script>
        
        <?php endif; ?>

    <?php endif; ?>
    

    <?php if( (get_the_ID() == 5530 || is_front_page()) && isset($GLOBALS['source']) ): ?>
        
        <script>
        
            jQuery(document).ready(function(){
            
                jQuery("#input_1_24").val("<?php echo $GLOBALS['source']; ?>");
                jQuery("#input_2_24").val("<?php echo $GLOBALS['source']; ?>");
            
            });
        
        </script>
        
    <?php endif; ?>
    
    
    <?php if(get_field('script_footer', 'option')){the_field('script_footer', 'option');} ?>

    <?php if(get_field('fo_script')){the_field('fo_script');} ?>

<div id="popform">
	<div id="popform-inner">[gravityform id="4" title="true" description="true"]</p><div id="closetheform"><a href="javascript:;">Close the Form</a></div></div>
</div>

<script>

//============================================
// Pop Out Form Controls
// April 2020
// by Jamon Abercrombie w/ Edwards Schoen

jQuery('.poptheform').click(function () {
	jQuery('.request-form').fadeIn(300);
	return false;
});
jQuery('#closetheform').click(function () {
	jQuery('.request-form').fadeOut(300);
	return false;
});
//============================================

</script>
<script>
if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
   $('body').addClass('idevice');
}
</script>
    <?php wp_footer(); ?>
    
 	<?php
		if ( !is_front_page() ) 
		{ echo '<style>#wpfront-notification-bar-spacer{display:none!important}</style>';}
	?>

</body>

</html>