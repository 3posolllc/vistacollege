<?php if (!get_field('tech_out')) : ?>
    <?php $online_status = false; $postID = $post->ID; $online_loop = new WP_Query( array( 'p' => 26, 'post_type' => 'campus' ) ); if ( $online_loop->have_posts() ) : ?>
        <?php while ( $online_loop->have_posts() ) : $online_loop->the_post(); ?>
            <?php if(get_field('av_programs')): ?>
                <?php
                    $ids = get_field('av_programs');
                    if (in_array($postID, $ids)) {
                        $online_status = true;
                    }
                ?>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php wp_reset_postdata(); endif; ?>

    <div class="slideshow-form programs-request-form">
        <div class="slideshow-form-content">
            <span id="programCloseButton">×</span>
            <h2>Request Information</h2>
            <?php echo do_shortcode("[gravityform id='3' title='false' description='false' ajax='false']"); ?>
            
            <p id="form-number-2">Step 2 of 2</p>
            
            <p class="form-desc">By submitting this form, I am giving express written consent to be contacted by Vista College using the information provided.</p>
            <input type="hidden" id="online_title" value="<?php echo $online_status ? $post->post_title : '' ?>">
            <input type="hidden" id="post_id" value="<?php echo $post->ID;?>">
        </div>
    </div>
<?php endif; ?>