<?php /* Template name: Testimonials */ ?>


<?php get_header(); ?>

<?php
//   Danielle Crosby-5344, Edgar Macias-5313,   Latosha Fitts- 5342, Trevon Alexander- 5340, Bonnie Cain- 5301
//  Laura Deleon-5338, Kimberly Mathis-5306, Lee Ette Lujan-5335, Carla McDonald- 5303, Eric Cisneros-5328
//  Taylor Thomas- 433, Robert Mitchell-5323, Josie Reid- 5321, Simone Wesley- 430
//  Ronald Cowan-5319, Darryl Leonard-429, Jessica Gordon-5317, Carl McDonald-432, Sarah Velasquez-5315
    $video_urls = array();
    $video_urls[5971] = "https://www.youtube.com/embed/hAzvZMp3kKA";    // George H.
    $video_urls[440] = "https://www.youtube.com/embed/_jXgQ0T5Y_M";    // Skylar Flowers
    $video_urls[5308] = "https://www.youtube.com/embed/0QaoeSZ08fg";    // Erika Hernandez
    $video_urls[5310] = "https://www.youtube.com/embed/EwhRfcHbjC4";    // Misty Gonzalez
    $video_urls[5331] = "https://www.youtube.com/embed/LKBJWYsWFMk";    // Jessica Fernandez
    $video_urls[5325] = "https://www.youtube.com/embed/J_aOACjINps";    // Christian Casillas
    $video_urls[428] = "https://www.youtube.com/embed/0jVT0GgKebU";    // Kim Martin
?>
<div class="section section-7">

    <div class="content group">
    
    
        <?php if( get_field('ft_enable'.$GLOBALS['device']) ): ?>
    
            <div class="programs-intro group">
            
                <?php if( get_field('ft_description'.$GLOBALS['device']) ): ?>
                
                    <div class="left-content">

                        <?php the_field('ft_description'.$GLOBALS['device']); ?>
                        
                    </div>
                
                <?php endif; ?>
                
                <?php if( get_field('ft_testimonial'.$GLOBALS['device']) ): ?>
                
                    <?php $post=get_field('ft_testimonial'.$GLOBALS['device']); setup_postdata($post); ?>
                
                    <aside class="right-side">
                    
                        <div class="testimonial" <?php if( get_field('te_image_fe') ): ?>style="background-image: url(<?php  the_field('te_image_fe'); ?>);"<?php endif; ?>>
                            
                            <?php if( get_field('te_fe_video') ): ?>
                                
                                <input type="hidden" class="video-iframe" value='<?php the_field('te_fe_video') ?>'/>
                                
                                <a class="player-btn fa fa-play-circle" aria-hidden="true"></a>
                            
                            <?php endif; ?>
                        
                            <div>
                        
                                <?php if( get_field('te_testimonial_fe') ): ?>
                                    <p>"<?php the_field('te_testimonial_fe'); ?>"</p>
                                <?php endif; ?>
                                
                                <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                    <h4><?php the_field('te_name'); ?><br/> <?php the_field('te_campus'); ?></h4>
                                <?php endif; ?>
                                
                            </div>
                            
                        </div>
                    
                    </aside>
                
                <?php endif; ?>
            
            </div>
            
        <?php endif; ?>
        
        
        <?php
        $testimonial_count = new WP_Query( array( 'post_type' => 'testimonials', 'posts_per_page' => -1, 'meta_query' => array(array('key' => 'te_testimonial_de', 'value' => '', 'compare' => '!=')) ) );
        $per_page = round($testimonial_count->post_count / 2);
        ?>
  
        <div class="group testimonial-group">

            <aside class="testimonial-left">
            
                <?php wp_reset_postdata(); ?>
            
                <?php echo child_pages(); ?>

                <div class="program-areas">
                
                    <h2>Program <b>Areas</b></h2>
                    
                    <div>
                    
                        <p>We offer career training that will help you start in a new rewarding career in a matter of months.</p>
            
                        <?php $area_loop = new WP_Query( array( 'post_type' => 'programs', 'post_parent' => 0, 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1 ) ); if ( $area_loop->have_posts() ) : ?>

                            <ul>

                                <?php while ( $area_loop->have_posts() ) : $area_loop->the_post(); ?>
                                
                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                                <?php endwhile; ?>
                                
                            </ul>
                            
                        <?php wp_reset_postdata(); endif; ?>
                        
                    </div>
                
                </div>
            
            </aside>

            <div class="testimonial-right">
            
                <div class="testimonial-boxes group">
                
                
                    <?php $testimonial_loop = new WP_Query( array( 'post_type' => 'testimonials', 'posts_per_page' => $per_page, 'paged' => 1, 'meta_query' => array(array('key' => 'te_testimonial_de', 'value' => '', 'compare' => '!=')) ) ); if ( $testimonial_loop->have_posts() ) : ?>
                
                        <div class="testimonial-column">
                        
                            <?php while ( $testimonial_loop->have_posts() ) : $testimonial_loop->the_post(); ?>
                        
                                <div class="testimonial-box">
                                
                                    <?php if( get_field('te_testimonial_de') ): ?>
                                    
                                        <div class="testimonial-content">
                                            
                                            <?php echo '"';the_field('te_testimonial_de'); echo '"';?>
                                            
                                        </div>
                                        
                                    <?php endif; ?>
                                    
                                    <div class="group">
                                        <div class="test-sparator"></div>
                                        <div class="success-testimonial-body">
                                            <?php if( get_field('te_image_de') ): ?>
                                                <div class="success-testimonial-image">
                                                    <?php $image = get_field('te_image_de'); ?>
                                                    <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                                                </div>
                                            <?php endif; ?>

                                            <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                                <div class="success-testimonial-text">
                                                    <h3><?php the_field('te_name'); ?>
                                                        <span><?php the_field('te_campus'); ?></span>
                                                        <?php if($video_urls[$post->ID]):?>
                                                        <input type="hidden" class="video-iframe" value="<?php echo "<iframe src='".$video_urls[$post->ID]."' frameborder='0' allowfullscreen=''></iframe>"; ?>">
                                                            <a class="video-btn fa fa-play-circle" aria-hidden="true"></a>
                                                        <?php endif; ?>
                                                    </h3>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                
                                </div>
                            
                            <?php endwhile; ?>
                        
                        </div>
                    
                    <?php endif; wp_reset_postdata(); ?>

                    
                    <?php $testimonial_loop = new WP_Query( array( 'post_type' => 'testimonials', 'posts_per_page' => $per_page, 'paged' => 2, 'meta_query' => array(array('key' => 'te_testimonial_de', 'value' => '', 'compare' => '!=')) ) ); if ( $testimonial_loop->have_posts() ) : ?>
                
                        <div class="testimonial-column">
                        
                            <?php while ( $testimonial_loop->have_posts() ) : $testimonial_loop->the_post(); ?>
                        
                                <div class="testimonial-box">
                                
                                    <?php if( get_field('te_testimonial_de') ): ?>
                                    
                                        <div class="testimonial-content">
                                            
                                            <?php echo '"';the_field('te_testimonial_de'); echo '"';?>
                                            
                                        </div>
                                        
                                    <?php endif; ?>
                                    
                                    <div class="group">
                                        <div class="test-sparator"></div>
                                        <div class="success-testimonial-body">
                                            <?php if( get_field('te_image_de') ): ?>
                                                <div class="success-testimonial-image">
                                                    <?php $image = get_field('te_image_de'); ?>
                                                    <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                                                </div>
                                            <?php endif; ?>

                                            <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                                <div class="success-testimonial-text">
                                                    <h3><?php the_field('te_name'); ?>
                                                        <span><?php the_field('te_campus'); ?></span>
                                                        <?php if($video_urls[$post->ID]):?>
                                                        <input type="hidden" class="video-iframe" value="<?php echo "<iframe src='".$video_urls[$post->ID]."' frameborder='0' allowfullscreen=''></iframe>"; ?>">
                                                            <a class="video-btn fa fa-play-circle" aria-hidden="true"></a>
                                                        <?php endif; ?>
                                                    </h3>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                
                                </div>
                            
                            <?php endwhile; ?>
                        
                        </div>
                    
                    <?php endif; wp_reset_postdata(); ?>
                    
                
                </div>
                
                <a class="show-more">Show More</a>
                <a class="show-less">Show Less</a>
            
            </div>
            
        </div>
        

    </div>
    
</div>


<?php get_footer(); ?>