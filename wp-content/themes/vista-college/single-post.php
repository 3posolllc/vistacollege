<?php get_header(); ?>

    <?php if ( have_posts() ) : ?>
    
        <div class="section section-7">
        
            <div class="content group">
            
                <aside class="left-side">
            
                    <div class="subnav-button">
                        <i class="fa fa-navicon"></i>
                        <h3>Categories</h3>
                    </div>
            
                    <div class="subnav categories">
                        <?php wp_nav_menu( array('menu' => 'blog')); ?>
                    </div>
                
                </aside>
            
                <div class="center-content">
                
                    <div class="share-buttons">
                
                        <?php echo do_shortcode( '[social_warfare]' ); ?>
                        
                        <span>Posted on <i><?php echo get_the_date(); ?></i></span>
                    </div>
                
                    
                        <?php if(get_field('po_image'.$GLOBALS['device'])): ?>
                            <?php $image = get_field('po_image'.$GLOBALS['device']); ?>
                            <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
                        <?php else: ?>
                            <?php the_post_thumbnail( 'large' ); ?>
                        <?php endif; ?>
                    
                    
                    <?php if(get_field('po_post'.$GLOBALS['device'])): ?>
                        <?php the_field('po_post'.$GLOBALS['device']); ?>
                    <?php else: ?>
                        <?php the_content(); ?>
                    <?php endif; ?>
                    
                </div>
                
                <aside class="right-side">
    
                    <?php if( have_rows('sidebar', 'option') ): ?>
                     
                        <?php while( have_rows('sidebar', 'option') ): the_row(); ?>
                        
                            <?php if( get_sub_field('si_item', 'option') ): ?>
                            
                                <div class="sidebar-box">
                            
                                    <?php the_sub_field('si_item', 'option'); ?>
                                </div>
                                
                            <?php endif; ?>
                            
                        <?php endwhile; ?>
                         
                    <?php endif; ?>
                
                </aside>
                
            </div>
            
        </div>

    <?php endif; ?>
		
<?php get_footer(); ?>