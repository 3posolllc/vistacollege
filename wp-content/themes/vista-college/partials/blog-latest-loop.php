
<?php
  $cat = 5; // Slug 'blog'
  $latestPosts = new WP_Query([
    'cat' => $cat,
    'posts_per_page' => 2
  ]);

?>

<?php if ($latestPosts->have_posts()): ?>
  <div class="row latest-posts__posts">
  <?php while ($latestPosts->have_posts()): $latestPosts->the_post(); ?>

    <?php
        $post_thumbnail = villacollege_get_post_thumbnail();
    ?>

      <div class="col-12 col-md-6 latest-posts__post">
        <a class="link--style-inherit" href="<?php the_permalink(); ?>">
          <div class="latest-posts__img-box">

            <?php if ($post_thumbnail && is_array($post_thumbnail)): ?>
              <img src="<?php echo esc_url($post_thumbnail['url']); ?>"
              class="latest-posts__img"
              alt="<?php echo esc_attr($post_thumbnail['alt']); ?>">
            <?php else: ?>
              <img class="latest-posts__img"
              src="/wp-content/themes/vista-college/img/blog/backup_bg_blog.png"
              alt="Back up image">
            <?php endif; ?>

          </div>
          <div class="latest-posts__content">
            <h3 class="latest-posts__post-title"><?php the_title(); ?></h3>
            <div class="latest-posts__post-date">
              <b>Posted on <em> <?php echo get_the_date(); ?></em></b>
            </div>
            <div class="latest-posts__excerpt">
              <?php echo esc_html(villacollege_excerpt()); ?>... <b class="read-more">Read More</b>
            </div>
          </div>
        </a>
      </div>
  <?php endwhile; ?>
  </div>

<?php endif; ?>
