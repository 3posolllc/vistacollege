<?php
// Set "blog-loop__category--3-items" on parent on when calling partial
// Like: set_query_var( 'blog-loop__category--3-items',  ['id' => int]);

// Get most recent posts and add IDs to array for exclusion in below categories.
  $latestPosts = new WP_Query([
    'cat' => 5,
    'posts_per_page' => 2
  ]);
  $excludeposts = array();
  while($latestPosts->have_posts()) : $latestPosts->the_post();
      $excludeposts[] = $post->ID; // add post id to array
  endwhile;

  $params = get_query_var('blog-loop__category--3-items');
  $excludeposts2 = explode(",",$excludeposts);
  $cat_query = new WP_Query([
    'cat' => $params['id'],
    'posts_per_page' => 3,
    'post__not_in' => $excludeposts
  ])
?>

<?php if ($cat_query->have_posts()): ?>
  <div class="row three-col-loop__posts">
  <?php while ($cat_query->have_posts()): $cat_query->the_post(); ?>

        <?php

        $post_thumbnail = villacollege_get_post_thumbnail();

        ?>

      <div class="col-12 col-md-4 three-col-loop__post">
        <a class="link--style-inherit" href="<?php the_permalink(); ?>">
          <div class="three-col-loop__img-box">

            <?php if ($post_thumbnail && $post_thumbnail['url']): ?>
              <img src="<?php echo esc_url($post_thumbnail['url']); ?>"
              class="lastest-posts__img"
              alt="<?php echo esc_attr($post_thumbnail['alt']); ?>">
            <?php else: ?>
              <img class="lastest-posts__img"
              src="/wp-content/themes/vista-college/img/blog/backup_bg_blog.png"
              alt="Back up image">
            <?php endif; ?>

          </div>
          <div class="three-col-loop__content">
            <h3 class="three-col-loop__post-title"><?php the_title(); ?></h3>
            <div class="three-col-loop__post-date">
              <b>Posted on <em> <?php echo get_the_date(); ?></em></b>
            </div>
            <div class="three-col-loop__excerpt">
              <?php echo esc_html(villacollege_excerpt(160)); ?>...
              <b class="read-more">Read More</b>
            </div>
          </div>
        </a>
      </div>
  <?php endwhile; ?>
  </div>
  <!-- Read more link -->
  <?php if ($params['showLink']): ?>
    <div class="three-col-loop__view-more --longer col-12">
      <a href="/blog/2/"
        class="btn btn--orange">View More Posts</a>
    </div>
  <?php endif; ?>

<?php endif; ?>
