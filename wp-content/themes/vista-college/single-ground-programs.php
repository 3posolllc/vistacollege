<?php
/*
  Template Name: Vista Ground Programs
  Template Post Type: post, page, projects, code, programs
 */
?>
<?php get_header(); ?>

<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
<?php if ( $post->post_parent ) : ?>
    <?php 
        $starting_dates = get_field('default_starting_dates', 'option');
        if ($starting_dates) {
            $starting_dates = explode(',', $starting_dates);
        } else {
            $starting_dates = [];
        }
        $today = date('Y-m-d');
        $starting_date = '';
        foreach ($starting_dates as $value) {
            $date_obj = explode('-', $value);
            $date = date('Y-m-d', mktime(0,0,0,$date_obj[0],$date_obj[1],$date_obj[2]));
            if ($date >= $today) {
                $starting_date = date('F j', mktime(0,0,0,$date_obj[0],$date_obj[1],$date_obj[2]));
                break;
            }
        }
    ?>
    <?php if( have_rows('ta_tabs') ): ?>
        <?php $content_array = array();?>
        <?php $x=1; while( have_rows('ta_tabs') ): the_row(); ?>
            <?php global $post;?>
            <?php if( have_rows('ta_sections') ): ?>
                <?php while( have_rows('ta_sections') ): the_row(); ?>
            
                    <?php if(get_sub_field('ta_description')): ?>
                            <?php $content_array[] = get_sub_field('ta_description'); ?>
                        </div>
                        
                    <?php endif; ?>
                    
                <?php endwhile; ?>
            
            <?php endif; ?>
        <?php endwhile;?>
    <?php endif;?>

    <div class="online-content  vista-online-body">
        <div class="online-section content section-1">
            <div class="row sectionrow">
                <div class=" <?php if (get_field('tech_out')) {echo 'col-xs-12';} else {echo 'col-lg-9 col-xs-12 col-sm-7 col-md-8';} ?> leftcontent">
                    <?php echo @$content_array[0];?>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 rightcontent <?php if (get_field('tech_out')) echo 'hideempty'; ?>">
                    <div class="fast-facts">
                        <h4>Fast Facts</h4>
                        <div> <strong>Next Start Date</strong> </div>
                        <?php   
                        if ( $post->ID == 9434 || $post->ID == 9433) { 
                            $starting_date="September 9"; 
                        }
                        ?>
                        <div><?php echo $starting_date; ?></div>
                        <hr class="fastfacts">
                        <div <?php if(!get_field('course_length')) echo ' class="hideempty"'; ?>>
                            <strong>Course Length:</strong> <?= get_field('course_length'); ?> *
                        </div>
                        <div <?php if(!get_field('class_hours')) echo ' class="hideempty"'; ?>>
                            <strong>Class Hours:</strong> <?= get_field('class_hours'); ?>
                        </div>
                        <div <?php if(!get_field('lab_hours')) echo ' class="hideempty"'; ?>>
                            <strong>Lab Hours:</strong>  <?= get_field('lab_hours'); ?>
                        </div>
                        <div <?php if(!get_field('clinical_hours')) echo ' class="hideempty"'; ?>>
                            <strong>Clinical Hours:</strong>  <?= get_field('clinical_hours'); ?>
                        </div>
                        <hr class="fastfacts">
                        <div>
                            <strong>Campus:</strong><br>
                            <select id="campus_select">
                                <option value="">- Select Campus -</option>
                                <?php $campuses_loop = new WP_Query( array( 'post_type' => 'campus', 'meta_query' => array(array('key' => 'av_programs','value' => '"' . get_the_ID() . '"','compare' => 'LIKE')) ) ); if ( $campuses_loop->have_posts() ) : ?>                            
                                    <?php while ( $campuses_loop->have_posts() ) : $campuses_loop->the_post(); ?>
										
										<?php //if (get_the_title()!="Lubbock, TX") { ?>
                                        <option value="<?php $value = get_the_ID()!=26 ? the_title(): 'Online Campus';echo $value; ?>"><?php the_title(); ?></option>
										<?php //} ?>
                                    <?php endwhile; ?>
                                <?php wp_reset_postdata(); endif; ?>
                            </select>
                        </div>
                        <a class="get-more-info online-get-started" onclick="_gaq.push(['_trackEvent', 'RFI', 'Click', 'NEXT']);">GET STARTED</a>
                        <div class="fastfactdisclaimer">
                            <small>* Based on successful, full time enrollment</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        <div class="online-section orangesection healthcare-ba-orange">
            <div class="content">
                <div class="orange-row">
                    <div class="orange-left healthcare-ba-online-thumb2">
                        <!-- <img src="http://staging.vistacollege.edu/wp-content/uploads/2019/06/onlineprog.jpg" class="orange-image"> -->
                    </div>
                    <div class="orange-right">
                        <?php echo @$content_array[1];?>
                    </div>
                </div>
            </div>
        </div>
        <div class="online-section content <?php if (get_field('tech_out')) echo 'hideempty'; ?>">
            <div class="row online-sheight-row">
        		<div class="col-sm-8">
        			<?php echo @$content_array[2];?>
        		</div>
        		<div class="col-sm-4 video-section">
        			<iframe width="650"  src="<?= get_field('youtube_link'); ?>?rel=0" frameborder="0"  frameborder="0" allowfullscreen></iframe>
        		</div>
        	</div>
    </div>
    </div>
        
    <div class="center-content-program">
        <?php echo do_shortcode('[fx_courses toggle=true]'); ?>
    </div>

    <?php if( have_rows('fc_content', 'option') ): ?>
        <?php if (!get_field('tech_out')) : ?> 
            <div class="footer-top get-started">
                <div class="footer-top started-left">
                    <div class="footer-left-top">
                        <div class="left-side-top">
                                                <div class="left-side-title">
                                                    <h1>Let's Get Started!</h1>
                                                    <span>Questions? An Admission Representative would love to help<span>
                                                </div>
                        </div>
                        <div class="left-side-bottom">
                            <input type="hidden" name="online_status" id="online_status" value="<?php echo $online;?>">
                            <select id="request_select">
                                <option value="">- Select Campus -</option>
                                <?php $campuses_loop = new WP_Query( array( 'post_type' => 'campus', 'meta_query' => array(array('key' => 'av_programs','value' => '"' . get_the_ID() . '"','compare' => 'LIKE')) ) ); if ( $campuses_loop->have_posts() ) : ?>                            
                                    <?php while ( $campuses_loop->have_posts() ) : $campuses_loop->the_post(); ?>
                                        <option value="<?php $value = get_the_ID()!=26 ? the_title(): 'Online Campus';echo $value; ?>"><?php the_title(); ?></option>
                                    <?php endwhile; ?>

                                <?php wp_reset_postdata(); endif; ?>

                            </select>
                            <a class="request-btn" onClick="_gaq.push(['_trackEvent', 'RFI', 'Click', 'NextStep']);">Next</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>

<?php else: ?>
    <?php
        $args = array(
            'post_parent' => $post->ID,
            'post_type'   => 'programs', 
            'numberposts' => -1,
            'post_status' => 'any' 
        );
        $children = get_children( $args );
        if (sizeof($children)>0) {
            include "single-areas.php";
        } else {
            include "single-programs-update.php";
        }
    ?>
<?php endif; ?>
 
<style type="text/css">
select {
    color: black !important
}
.course-header {
    background-color: #151c38
}
.course .course__num {
    color: #339999
}
.get-started {
    background: url(/wp-content/themes/vista-college/img/footer-online-background.png) no-repeat
}
.online-get-started {
    display: block;
    width: 100%;
    height: 40px;
    margin-top: 25px;
    margin-left: 0;
    border: 0;
    background: #339999;
    padding: 0;
    cursor: pointer;
    line-height: 40px;
    -webkit-appearance: none;
    border-radius: 0;
}
.online-get-started:hover {
    background: #e97a6d;
}
#menu-header-1 .sub-menu {
    width: 239px !important;
}
.healthcare-ba-online-thumb2 {
    background: url(<?= get_field('career_image');?>) no-repeat left;
    background-size: cover;
    margin-top: -50px;
    margin-bottom: -50px;
}
#request_select {
    height: 40px;
    padding: 0 6px;
    min-width: 30%;
    border: 2px solid #dedbdb;
    border-radius: 2px;
    margin-bottom: 30px;
    max-width: 90%;
}
.request-btn {
    display: inline-block;
    margin-left: 0px !important;
    padding: 9px 20px;
    font-size: 20px !important;
    font-weight: bold;
    color: #fff;
    background: #339999;
    border: none;
    border-radius: 4px;
    text-transform: uppercase;
    cursor: pointer;
}

a.request-btn:hover {
    background-color: #e97a6d;
    color: #ffffff;
}
</style>
<?php global $onlinePage; $onlinePage= true; ?>
<?php get_footer(); ?>