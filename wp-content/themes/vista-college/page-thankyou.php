<?php /* Template name: Thank You */ ?>


<?php get_header(); ?>


<div class="section">

    <div class="content group">
    
        <div class="thankyou group">
            

            <?php if (isset($GLOBALS['program'])): ?>

            
                <?php
                    $description = false;
                    $video = false;
                    $program = $GLOBALS['program']; 
                    $post = get_page_by_title($program, OBJECT, 'programs');
                    $pid = $post->ID;
                ?>
                
                
                <?php $programs_loop = new WP_Query( array( 'p' => $pid, 'post_type' => 'programs' ) ); if ( $programs_loop->have_posts() ) : ?>

                    <?php while ( $programs_loop->have_posts() ) : $programs_loop->the_post(); ?>
                        
                        <?php if(get_field('ty_video'.$GLOBALS['device'])){$video=true;} ?>
            
                        <?php if(get_field('ty_description'.$GLOBALS['device'])){$description=true;} ?>

                    <?php endwhile; ?>
                  
                <?php endif; wp_reset_postdata(); ?>
                
                
                <?php if($video == false): ?>
                
                    <?php if(get_field('ty_video'.$GLOBALS['device'])): ?>
                        
                        <div class="thankyou-video">
                            
                            <?php the_field('ty_video'.$GLOBALS['device']); ?>
                        
                        </div>
                        
                    <?php endif; ?>
                
                <?php endif; ?>

                
                <?php $programs_loop = new WP_Query( array( 'p' => $pid, 'post_type' => 'programs' ) ); if ( $programs_loop->have_posts() ) : ?>

                    <?php while ( $programs_loop->have_posts() ) : $programs_loop->the_post(); ?>
                        
                        <?php if(get_field('ty_video'.$GLOBALS['device'])): ?>
                            
                            <div class="thankyou-video">
                            
                                <?php the_field('ty_video'.$GLOBALS['device']); ?>
                                
                            </div>
                            
                        <?php endif; ?>
            
                        <?php if(get_field('ty_description'.$GLOBALS['device'])): ?>
                        
                            <div class="thankyou-content">
                            
                                <?php the_field('ty_description'.$GLOBALS['device']); ?>
                                
                            </div>
                        
                        <?php endif; ?>

                    <?php endwhile; ?>
                  
                <?php endif; wp_reset_postdata(); ?>
                
                
                <?php if($description == false): ?>
                
                    <?php if(get_field('ty_description'.$GLOBALS['device'])): ?>
                    
                        <div class="thankyou-content">
                    
                            <?php the_field('ty_description'.$GLOBALS['device']); ?>
                        
                        </div>
                        
                    <?php endif; ?>
                
                <?php endif; ?>
                
                
            <?php else: ?>
            
                <?php if(get_field('ty_video'.$GLOBALS['device'])): ?>
                
                    <div class="thankyou-video">
                
                        <?php the_field('ty_video'.$GLOBALS['device']); ?>
                        
                    </div>
                    
                <?php endif; ?>
            
                <div class="thankyou-content">
                
                    <?php if(get_field('ty_description'.$GLOBALS['device'])){the_field('ty_description'.$GLOBALS['device']);} ?>
                    
                </div>
            
            <?php endif; ?>
            
            
        </div>

    </div>
    
</div>


<?php get_footer(); ?>