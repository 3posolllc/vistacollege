<style>
	.mobile-buttons {
			display: none;
			
	}
	.header{
		height:110px;
	}
	#menu-header-1 .sub-menu, .dropdown{
		top:28px;
	}
	.header-navigation{
		margin-bottom:2px;
	}
	.newnav .header-right a.login{
		    background-image: linear-gradient(to bottom, #e64216 0%, #fb7310 100%);
		margin-right:3px;
	}
	.search-icon .search-get {
		color: #fff;
	}
	.search-icon{
		display: none;
	}
	#menu-header-1>.current-menu-item>a, #menu-header-1>li>a:hover {
		padding-bottom: 13px !important;
	}
	.logo img {
	    margin-bottom: 10px;
	    margin-top: 20px;
	    	width: 250px;
	}
	.headertest2019 .header-navigation, .newnav .header-navigation {
		margin-top: 25px;
	}
	.desktoplogo{
		display:block
	}
	.mobilelogo{
		display:none
	}
	.headertest2019 .header-info, .newnav .header-info {
		padding: 6px 5px;
	}
	i.fa.fa-comment {
   	 	padding-right: 5px!important;
	}
	.headertest2019 .header-right a.login, .newnav .header-right a.login {
    	padding: 8px 24px 6px;
	}
	
	@media (max-width: 910px){
		.newnav .header-info li i.fa{
				padding-right:0px !important;
		}
	}
	@media (max-width: 1278px){
		.header-right {
		 float: none; 
		}
		.header-navigation.group {
			margin-top: 0px !important;			
    		margin-bottom: 10px!important;
		}
	}
	@media (max-width: 910px){
		.search-get, .fa-bars{margin-left:0px;line-height:25px}
	}
	@media (max-width:910px){
		.header-right a.login{
			display:none !important;
		}
		.header-info a {
			color: #399;
		
		}
		.search-icon .search-get {
			color: #399 ;
		}
		.search-icon{
			display: block;
		}
		
		.header-info li.chat-now{display:none}
	}
	
	@media (max-width:600px){
		/*.newnav header {
			height: 55px;
		
		}*/
		.logo img {
			margin-top: 15px;
		}
		#drift-widget{
		right:5px !important;
		}


	}
	@media (max-width:650px) and (min-width:601px){
		
		.logo img {
			display:inline-block;
			margin-top:9px !important;
			margin-left:45px !important;
		}
		
		.mobile-buttons{
			position: absolute;
			
			top:0px;
			left:0;
		}
		
	}
	@media (max-width:910px) and (min-width:651px){
		/*.newnav header {
			height: 120px;
		
		}*/
		.header-info li{
			position: static;
		}
		.newnav .header-info li span {
   			 font-size: 11px;
		}
		.newnav .header-info li i.fa {
   			 font-size: 18px!important;
		}
		.headertest2019 .header-info, .newnav .header-info {
    		padding: 13px 5px;
		}
	}
	@media (max-width: 1024px){

		  
	}
	
	@media (max-width: 800px){
		.logo img {
    		display: inline-block;
			margin-top:10px;
		}
		header{
			/*padding-bottom:20px !important;*/
		}
			
	}
	@media (min-width:601px) and (max-width: 800px){
		
		header{
			/*padding-bottom:20px !important;*/
		}
		/*.mobile-buttons {
			
			top: 0px;
			
		}*/
			
	}

	@media   (max-width:1426px){
		
		#menu-header-1 li {
			
			margin: 0 5px;
			
		}
		

	}
	@media  (min-width:1299px) and (max-width:1426px){
		.logo img {
	    	width: 250px;
		}
		#menu-header-1 li {
			
			margin: 0 8px;
			
		}
		

	}
	
	/*@media  (min-width:1201px) and (max-width:1298px){
		.logo img {
	    	width: 200px;
		}
	}*/

	@media  (min-width:910px) and (max-width:944px){
		.logo img {
	    	/*width: 155px;*/
		}
	}
	@media  (min-width:945px) and (max-width:1100px){
		.logo img {
	    	/*width: 200px;*/
		}
		header{height:110px}
		.header-navigation.group {
    		margin-top: -40px !important;
		}
	}
	@media (max-width: 650px){
		.header-info li.book {
			right: 140px;
		}
		.header-info li.email {
			right: 80px !important;
		}
		.header-info li.search-icon {
		right: 20px;
	}
	.logo img {
		width: 110px;
		margin-left: 50px;
		    margin-top: 10px;
}
	}	
		.mobile-buttons{
			position:absolute;
			
    left: -15px;
		}
		.content.group{
			/*padding-left:0px;*/
		}
	
	}
	
	@media (min-width:601px) and (max-width: 650px){
		.mobile-buttons{
			
			margin-top: 10px !important;
			top:0px;
    
		}
	}
	
	@media(max-width:600px){
		.newnav header {
   
    padding-top: 0px;
}
		.mobile-buttons {
    
    margin-top: 0px;
}
	}
	
	@media (max-width: 47.99em){
		.content-slide {
    margin-bottom: 20px;
    margin-top: 80px;
}
		
	}
	
	@media  (min-width:651px) and (max-width:660px){
		.logo img {
			margin-top:10px;
		}
	}
	
	@media  (min-width:801px) and (max-width:910px){
		.mobile-buttons {
    
    left: 25px;
}
	
		.logo img{
			    margin-left: 45px;
			margin-bottom:5px;
		}
		
	}
	@media  (min-width:672px){
		
		.desktoplogo{
			display:block!important;
		}
		.mobilelogo{
			display:none!important;
		}
		.logo img{
			float:left;
		}
	}
	@media (max-width:800px){
		.logo img{
			margin-left:45px;
		}
		.mobile-buttons .fa-bars {
		    left: 40px;
			margin-top:0px !important;
		}
	}
	@media (max-width:671px){
		.desktoplogo{
			display:none!important;
		}
		.mobilelogo{
			display:block!important;
    		width: 110px!important;
    		float: left;
		}
		
	}
	@media (max-width:651px){
		
		.header-info li {
			top: 25px !important;
		
		}
		.header-info i{
			font-size: 18px !important;
		}
		
		
	}
	@media (max-width:600px){
		
		
		.mobile-buttons{
			margin-top:5px;
		}
		.logo img{
			margin-top:4px !important;
			margin-left:35px;
		}
		
	}
    
	@media (min-width:1101px) and (max-width:1250px){
		.header-navigation.group{
			/*position: absolute;
			
			left:60px;
			top:50px !important;*/
		}
		.desktop-nav{
			/*float: left*/
		}
		
		header {
			height: 135px!important;
		}
	}
	@media (max-width:1100px){
		.desktop-nav{
			display: none !important;
		  }
		  .header-navigation .fa-bars {
		display: table;
	  }
		.header-right{float:none}

	}
	@media (min-width:1101px) and (max-width:1200px){
		.header-navigation.group{
			left:0px;
			/*top:45px !important;*/
		}
		
		
		header{
			height: 105px;
		}
	}
	
	@media (max-width:1200px){
	.header-right .group, .header-navigation {
		float: none!important;
	}
	}
	@media  (min-width:1025px) and (max-width:1100px){
		.logo img {
	    	margin-left: 0px;
		}
		
		

	}
	
	@media (min-width:1251px){

		#menu-header-1 .sub-menu, .dropdown{
			top:36px !important;
		}
		#menu-header-1>li>a:hover{
			padding-bottom:13px !important;
		}
	}
	
	@media (min-width:1279px){
		.header-navigation.group{
			margin-top: 20px !important;
			margin-bottom: 10px !important;
		}
	}
	@media (max-width:910px){
		 .header-navigation {
			display: none;
		  }
		.mobile-buttons {
			display: block;
			float: left;
		  }
		.mobile-buttons .fa-bars {
			margin-left: 0;
			font-size: 24px;
			margin-top: 45px;
			margin-right: 35px;
		  }
	}
	@media  (min-width:911px) and (max-width:944px){
		.header-navigation {
			position: absolute;
			right: 17px;
			top:50px;
		}
	}
	@media (max-width:910px){
		.logo img{
			margin-top:0px;
		}
		.mobile-buttons{
			top:-20px !important;
		}
		header{
			height: 70px;
		}
	}
	@media(max-width:800px){
		.logo img{
			margin-top:9px;
		}
	}
	
	@media (min-width:911px){
		.header-info li{
			padding:0 10px !important;
		}
	}
	
	@media (min-width:911px) and (max-width:1100px){
		header{
			height:90px;
		}
		.logo img{
			margin-top:10px !important;
		}
	}
</style>
<header>
      
    <div class="content group">
    
        <div class="mobile-buttons">
            <a class="fa fa-bars"></a>
            <a class="search-get"><i class="fa fa-search"></i></a>
        </div>
        
        <?php if( get_field('logo_dark', 'option') ): ?>
            <a href="<?php echo site_url(); ?>" class="logo">
                <?php $logo = get_field('logo_bright', 'option'); ?>
                <img src="<?php echo $logo['sizes']['thumbnail']; ?>" alt="<?php echo $logo['alt']; ?>" class="desktoplogo" />
                <img src="/wp-content/uploads/2020/06/vista-logo-mobile.jpg" class="mobilelogo" />
            </a>
        <?php endif; ?>
    
        <div class="header-right">
        
            <div class="group">
        		<a href="//vistacollege.instructure.com" class="login"><i class="fa fa-user"></i><span class="more-info">Login</span></a>
                <ul class="header-info">
                	<!--<li class="call"> <a class="call-btn" href="tel:8004564564" aria-hidden="true"> <i class="fa fa-phone"></i><span class="more-info">(800) 456-4564</span> </a> </li>-->
                    <li class="book"> 
                    	<input type="hidden" class="video-iframe" value="<iframe src='//app.acuityscheduling.com/schedule.php?owner=16621574' width='100%' height='800' frameBorder='0'></iframe><script src='//d3gxy7nm8y4yjr.cloudfront.net/js/embed.js' type='text/javascript'></script>"> 
                    	<a class="book-btn" aria-hidden="true"> 
                    		<i class="fa fa-calendar-check-o"></i><span class="more-info">SET AN APPOINTMENT</span> 
                    	</a> 
                    </li>
                    <?php
                        $cstDate = gmdate('G') - 5;
                        $postid = get_the_ID();
                        switch($postid){
                            case '5759':
                           $chaturl='online-2-2';
                           break;
                       case '116':
                           $chaturl='online-2-2-1';
                           break;
                       case '24':
                           $chaturl='online-2-2-2';
                           break;
                       case '62':
                           $chaturl='online-2-2-1-1-1';
                           break;
                        case '9254':
                            $chaturl='online-2-2-2-8c6899f4';
                            break;
                        case '9295':
                            $chaturl='online-2-2-2-8c6899f4-0e66dc24';
                            break;
                        case '106':
                            $chaturl='online-2-2-2-8c6899f4-0e66dc24-d9f587ab';
                            break;
                        case '5762':
                            $chaturl='online-2-2-2-8c6899f4-0e66dc24-d9f587ab-f6042103';
                            break;
                        case '5530':
                            $chaturl='online-2-2-2-8c6899f4-0e66dc24-d9f587ab-f6042103-5107395d-14695feb'; //new code
                            break;
                        case '46':
                            $chaturl='online-2-2-2-8c6899f4-0e66dc24-d9f587ab-f6042103-5107395d';
                            break;
                       default:
                           $chaturl='online-2';
                           break;
                        }
                    ?>
                    <?php if($cstDate >= 9 && $cstDate <= 21):?>
                        <li class="chat-now">
                            <a class="drift-open-chat" href="#<?php echo $chaturl; ?>">
                                <i class="fa fa-comment"></i>
                                <span> Chat Now</span>
                            </a>
                        </li>
                    <?php elseif(get_field('phone', 'option') && get_field('phone_link', 'option')): ?>
                        <!--<li class="phone-number">
                            <a href="tel:<?php the_field('phone_link', 'option'); ?>">
                                <i class="fa fa-phone"></i><span>(866) 442-4197</span><span class="call-us">CALL US</span>
                            </a>
                        </li>-->
                    <?php endif; ?>
                    <li class="email">
                        <a href="<?php the_permalink(5530); ?>" onClick="_gaq.push(['_trackEvent', 'RFI', 'Click', 'Header']);">
                            <i class="fa fa-paper-plane"></i><span class="more-info">REQUEST INFO</span>
                        </a>
                    </li>   
					<li class="search-icon">
                        <a class="search-get">
                            <i class="fa fa-search"></i><span class="more-info">SEARCH</span> 
                        </a>
                    </li>                    
                </ul>
            </div>
            
            <div class="header-navigation group">
                
                <a class="fa fa-bars"></a>
                <a class="search-get"><i class="fa fa-search"></i></a>
                
				<nav class="desktop-nav">
                <?php 
                    if ($post->post_parent)	{
						$ancestors=get_post_ancestors($post->ID);
						$root=count($ancestors)-1;
						$parent = $ancestors[$root];
					} else {
						$parent = $post->ID;
					}
                    if ( is_page(11330) || $parent=="11330"){                    
                        wp_nav_menu( array('menu' => 'Careermenu','menu_id'=>'menu-header-1','class'=>'career-menu')); 
                    }
                    else{
                        wp_nav_menu( array('menu' => 'header')); 
                    }
                ?>
                </nav>
                
                <div class="dropdown group">
                
                    <div>
                    
                        <h3><a href="<?php echo get_permalink(62); ?>">Ground Campuses</a></h3>
                        
                        <?php $campus_loop = new WP_Query( array( 'post_type' => 'campus', 'post__not_in' => array(26), 'posts_per_page' => 4, 'paged' => 1 ) ); if ( $campus_loop->have_posts() ) : ?>

                            <ul>

                                <?php while ( $campus_loop->have_posts() ) : $campus_loop->the_post(); ?>
                                
                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                                <?php endwhile; ?>
                                
                            </ul>
                            
                        <?php endif; ?>
                        
                        <?php $campus_loop = new WP_Query( array( 'post_type' => 'campus', 'post__not_in' => array(26), 'posts_per_page' => 4, 'paged' => 2 ) ); if ( $campus_loop->have_posts() ) : ?>

                            <ul>

                                <?php while ( $campus_loop->have_posts() ) : $campus_loop->the_post(); ?>
                                
                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                                <?php endwhile; ?>
                                
                            </ul>
                            
                        <?php endif; ?>
                        
                        <?php $campus_loop = new WP_Query( array( 'post_type' => 'campus', 'post__not_in' => array(26), 'posts_per_page' => 4, 'paged' => 3 ) ); if ( $campus_loop->have_posts() ) : ?>

                            <ul>

                                <?php while ( $campus_loop->have_posts() ) : $campus_loop->the_post(); ?>
                                    
                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                                <?php endwhile; ?>
                                
                            </ul>
                            
                        <?php endif; ?>
                    
                    </div>
                    
                    <div style="display:none">
                        
                        <h3><a href="<?php echo get_permalink(26); ?>">Online Campus</a></h3>
                        
                        <?php $online_loop = new WP_Query( array( 'p' => 26, 'post_type' => 'campus' ) ); if ( $online_loop->have_posts() ) : ?>
    
                        <ul>
                            
                                                                
                            <li><a href="/vista-online/">Vista College Online Campus</a></li>
                        
                                                    
                        </ul>
                                
                        <?php wp_reset_postdata(); endif; ?>
                        
                    </div>
                    
                <div class="menuacc">
                	<span>Accredited by Council on Occupational Education</span>
                </div>
                </div>
                
            </div>
        
        </div>
        
    </div>
    
</header>