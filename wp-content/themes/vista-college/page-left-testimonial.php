<?php /* Template name: Left Sidebar Testimonial */ ?>


<?php get_header(); ?>

    <?php if ( have_posts() ) : ?>
    
        <div class="section section-7">
        
            <div class="content group">
            <div class="combine">
                <aside class="left-side left">

                	<?php if(is_page(10875) || is_page(11189) || is_page(11191) || is_page(11185) || is_page(11177) || is_page(11187) ) echo "<h4>CE Courses</h4>"; ?>
                    <?php if( have_rows('na_navigation'.$GLOBALS['device']) ): ?>
        
                        <ul class="categories">
                        
                            <?php while( have_rows('na_navigation'.$GLOBALS['device']) ): the_row(); ?>
                             
                                <?php if(get_sub_field('na_type'.$GLOBALS['device']) == "External"): ?>
                                
                                    <?php if(get_sub_field('na_title'.$GLOBALS['device']) && get_sub_field('na_link'.$GLOBALS['device'])): ?>
                                    
                                        <li>
                                            <a href="<?php the_sub_field('na_link'.$GLOBALS['device']); ?>" target="_blank"><?php the_sub_field('na_title'.$GLOBALS['device']); ?></a>
                                        </li>
                                        
                                    <?php endif; ?>
                                    
                                <?php else: ?>
                                
                                    <?php if(get_sub_field('na_page'.$GLOBALS['device'])): $id=get_sub_field('na_page'.$GLOBALS['device']); ?>
                                
                                        <li>
                                            <a href="<?php echo get_the_permalink($id); ?>"><?php echo get_the_title($id); ?></a>
                                        </li>
                                
                                    <?php endif; ?>
                                
                                <?php endif; ?>
                            
                            <?php endwhile; ?>
                            
                        </ul>

                    <?php elseif(child_pages() != ""): ?>
                    
                        <?php echo child_pages(); ?>
                        
                    <?php else: ?>
                    
                        <?php wp_nav_menu( array('menu' => 'footer-1')); ?>
                        
                    <?php endif; ?>
					
                <?php 
					if( have_rows('ft_testimonials'.$GLOBALS['device']) ): ?>
                
                            <?php while( have_rows('ft_testimonials'.$GLOBALS['device']) ): the_row(); ?>
                    
                                <?php if( get_sub_field('ft_testimonial'.$GLOBALS['device']) ): ?>
                                    
                                    <?php $post = get_sub_field('ft_testimonial'.$GLOBALS['device']); setup_postdata($post); ?>
                                    
                                        <div class="featured-testimonial">
                                        
                                            <div class="testimonial" <?php if( get_field('te_image_fe') ): ?>style="background-image: url(<?php the_field('te_image_fe'); ?>);"<?php endif; ?>>
                                                
                                                <?php if( get_field('te_fe_video') ): ?>
                                                    
                                                    <input type="hidden" class="video-iframe" value='<?php the_field('te_fe_video') ?>'/>
                                                    
                                                    <a class="player-btn fa fa-play-circle" aria-hidden="true"></a>
                                                
                                                <?php endif; ?>
                                                
                                                <a href="<?php the_permalink(144); ?>" class="testimonials-link"><i class="fa fa-angle-right"></i>View more Stories</a>
                                            
                                                <div>
                                            
                                                    <?php if( get_field('te_testimonial_fe') ): ?>
                                                        <p>"<?php the_field('te_testimonial_fe'); ?>"</p>
                                                    <?php endif; ?>
                                                    
                                                    <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                                        <h4><?php the_field('te_name'); ?><br/> <?php the_field('te_campus'); ?></h4>
                                                    <?php endif; ?>
                                                    
                                                </div>
                                                
                                            </div>
                                            
                                        </div>

                                    <?php wp_reset_postdata(); ?>
                                    
                                <?php endif; ?>
                                
                            <?php endwhile; ?>
                            
                        <?php else: ?>
                
                            <?php $tag = sanitize_title(get_the_title()); ?>
                        
                            <?php query_posts("post_type=testimonials&tag=$tag&posts_per_page=5"); ?>
                        
                                <?php if(have_posts()) : ?>
                                
                                    <?php while (have_posts()) : the_post(); ?>
                                    
                                        <div class="featured-testimonial">
                                        
                                            <div class="testimonial" <?php if( get_field('te_image_fe') ): ?>style="background-image: url(<?php the_field('te_image_fe'); ?>);"<?php endif; ?>>
                                                
                                                <?php if( get_field('te_fe_video') ): ?>
                                                    
                                                    <input type="hidden" class="video-iframe" value='<?php the_field('te_fe_video') ?>'/>
                                                    
                                                    <a class="player-btn fa fa-play-circle" aria-hidden="true"></a>
                                                
                                                <?php endif; ?>
                                                
                                                <a href="<?php the_permalink(144); ?>" class="testimonials-link"><i class="fa fa-angle-right"></i>View more Stories</a>
                                            
                                                <div>
                                            
                                                    <?php if( get_field('te_testimonial_fe') ): ?>
                                                        <p>"<?php the_field('te_testimonial_fe'); ?>"</p>
                                                    <?php endif; ?>
                                                    
                                                    <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                                        <h4><?php the_field('te_name'); ?><br/> <?php the_field('te_campus'); ?></h4>
                                                    <?php endif; ?>
                                                    
                                                </div>
                                                
                                            </div>
                                            
                                        </div>
                                    
                                    <?php endwhile; ?>
                                
                                <?php endif; ?>
                                            
                            <?php wp_reset_query(); ?>
                            
                        <?php endif; ?>
					
                </aside>
             
                </div>
                <div class="center-contents">
                
                    <?php if(get_field('co_content'.$GLOBALS['device'])){the_field('co_content'.$GLOBALS['device']);} ?>
                    
                </div>
                
                
                
            </div>
            
        </div>

    <?php endif; ?>
<style>
.featured-testimonial {
    margin-top: 30px;
}
.left-side {
    width: 320px;
    padding-right: 25px;
}
.right-side-testimonials .featured-testimonial:last-child .testimonials-link {
    padding-left: 0px!important; 
}
@media (max-width: 1441px) and (min-width: 1000px){
.featured-testimonial .testimonial {
    height: 280px!important;
    background-position: bottom right;
}
.testimonial p {
    font-size: 18px;
    line-height: 20px;}
}

@media (max-width: 1000px){
.featured-testimonial .testimonial{height:280px!important}
}
</style>     
<?php get_footer(); ?>