<?php /* Template name: Programs */ ?>

<?php get_header(); ?>

<div class="section section-7">

    <div class="content group testimonial-wrap">
    
        <div class="group">
        
            <div class="left-content botomtab">
            
                <?php $online_ids = array(); ?>
            
                <?php $online_loop = new WP_Query( array( 'p' => 26, 'post_type' => 'campus' ) ); if ( $online_loop->have_posts() ) : ?>
        
                    <?php while ( $online_loop->have_posts() ) : $online_loop->the_post(); ?>

                        <?php if(get_field('av_programs')): ?>
                            
                            <?php $online_ids = get_field('av_programs'); ?>

                        <?php endif; ?>

                    <?php endwhile; ?>
                        
                <?php endif; ?>
            
                <ul class="page-navigation group">
                    <li class="tab-btn-1 active"><a onclick="tab(1, false);">Ground Campus Programs</a></li>
                    <?php if(!empty($online_ids)): ?>
                        <li class="tab-btn-2"><a onclick="tab(2, false);">Online Campus Programs</a></li>
                    <?php endif; ?>
                </ul>
                
                <ul class="page-navigation vertical mobile-tabs group">
                    <li class="tab-btn-1 active"><a onclick="tab(1, false);">Ground Campus Programs</a></li>
                </ul>
                
                <div class="tab" id="tab-1">
                
                    <?php $area_loop = new WP_Query( array( 'post_type' => 'programs', 'post_parent' => 0, 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1 ) ); if ( $area_loop->have_posts() ) : ?>

                        <ul class="dropdown-list">

                            <?php while ( $area_loop->have_posts() ) : $area_loop->the_post(); ?>
                            
                                <?php $parent = get_the_ID(); ?>
                                
                                <?php $programs_count = new WP_Query( array( 'post_type' => 'programs', 'post_parent' => $parent, 'post__not_in' => $online_ids, 'posts_per_page' => -1 ) ); ?>
                                
                                <?php if($programs_count->post_count > 0): ?>
                            
                                    <li>
                                    
                                        <a><?php the_title(); ?></a>
                                        
                                        <?php $program_loop = new WP_Query( array( 'post_type' => 'programs', 'post_parent' => $parent, 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1 ) ); 
                                        if($program_loop->have_posts()) : ?>
                                            
                                            <ul>
                                        
                                            <?php while ( $program_loop->have_posts() ) : $program_loop->the_post(); ?>
                                            
                                                <?php if (!in_array(get_the_ID(), $online_ids)): ?>
                                                <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?><?php echo ' ' . get_field('degree_type');?></a></li>
                                                <?php endif; ?>
                                            
                                            <?php endwhile; ?>
                                            
                                            </ul>
                                        
                                        <?php endif; ?>
                                        
                                    </li>
                                    
                                <?php endif; ?>
                                
                            <?php endwhile; ?>
                            
                        </ul>
                        
                    <?php endif; ?>
                
                </div>
                
                <?php if(!empty($online_ids)): ?>
                    <ul class="page-navigation vertical mobile-tabs group">
                        <li class="tab-btn-2"><a onclick="tab(2, false);">Online Campus Programs</a></li>
                    </ul>
                <?php endif; ?>
                
                <div class="tab" id="tab-2">
                    
                    <?php $area_loop = new WP_Query( array( 'post_type' => 'programs', 'post_parent' => 0, 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1 ) ); if ( $area_loop->have_posts() ) : ?>

                        <ul class="dropdown-list">

                            <?php while ( $area_loop->have_posts() ) : $area_loop->the_post(); ?>
                            
                                <?php $parent = get_the_ID(); ?>
                                
                                <?php $programs_count = new WP_Query( array( 'post_type' => 'programs', 'post_parent' => $parent, 'post__in' => $online_ids, 'posts_per_page' => -1 ) ); ?>
                                
                                <?php if($programs_count->post_count > 0): ?>
                            
                                    <li>
                                    
                                        <a><?php the_title(); ?></a>
                                            
                                        <?php $program_loop = new WP_Query( array( 'post_type' => 'programs', 'post_parent' => $parent, 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1 ) ); if($program_loop->have_posts()) : ?>
                                            
                                            <ul>
                                        
                                            <?php while ( $program_loop->have_posts() ) : $program_loop->the_post(); ?>
                                            
                                                <?php if (in_array(get_the_ID(), $online_ids)): ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php endif; ?>
                                            
                                            <?php endwhile; ?>
                                            
                                            </ul>
                                        
                                        <?php endif; ?>
                                        
                                    </li>
                                    
                                <?php endif; ?>
                                
                            <?php endwhile; ?>
                            
                        </ul>
                        
                    <?php endif; ?>
                
                </div>
            
            </div>
            
            <aside class="left-side right campus">
            
                <div class="program-areased">
               
                <iframe width="590" height="400" src="https://www.youtube.com/embed/GRzA6OQ9j2g" frameborder="0" allowfullscreen></iframe>
                    <!-- <h2> <b>Areas</b></h2>
                    
                    <div>
                    
                        <p>We offer career training that will help you start in a new rewarding career in a matter of months.</p>
            
                        <?php $area_loop = new WP_Query( array( 'post_type' => 'areas', 'posts_per_page' => -1 ) ); if ( $area_loop->have_posts() ) : ?>

                            <ul>

                                <?php while ( $area_loop->have_posts() ) : $area_loop->the_post(); ?>
                                
                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                                <?php endwhile; ?>
                                
                            </ul>
                            
                        <?php wp_reset_postdata(); endif; ?>
                        
                    </div> -->
                
                </div>
            
            </aside>
        
        </div>
        <?php if( get_field('ft_enable'.$GLOBALS['device']) ): ?>
    
            <div class="programs-intro group">
            
                <?php if( get_field('ft_description'.$GLOBALS['device']) ): ?>
                    
                    <div class="left-content">

                        <?php the_field('ft_description'.$GLOBALS['device']); ?>
                        
                    </div>
                
                <?php endif; ?>
                
                <?php if( get_field('ft_testimonial'.$GLOBALS['device']) ): ?>
                
                    <?php $post=get_field('ft_testimonial'.$GLOBALS['device']); setup_postdata($post); ?>
                
                    <aside class="right-side">
                    
                        <div class="testimonial" <?php if( get_field('te_image_fe') ): ?>style="background-image: url(<?php  the_field('te_image_fe'); ?>);"<?php endif; ?>>
                            
                            <?php if( get_field('te_fe_video') ): ?>
                                
                                <input type="hidden" class="video-iframe" value='<?php the_field('te_fe_video') ?>'/>
                                
                                <a class="player-btn fa fa-play-circle" aria-hidden="true"></a>
                            
                            <?php endif; ?>
                            
                            <a href="<?php the_permalink(144); ?>" class="testimonials-link"><i class="fa fa-angle-right"></i>View more Stories</a>
                        
                            <div>
                        
                                <?php if( get_field('te_testimonial_fe') ): ?>
                                    <p>"<?php the_field('te_testimonial_fe'); ?>"</p>
                                <?php endif; ?>
                                
                                <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                    <h4><?php the_field('te_name'); ?><br/> <?php the_field('te_campus'); ?></h4>
                                <?php endif; ?>
                                
                            </div>
                            
                        </div>
                    
                    </aside>
                
                <?php endif; ?>
            
            </div>
            
        <?php endif; ?>

    </div>
    
</div>


<?php if( have_rows('fc_content', 'option') ): ?>

    <div class="section section-8" <?php if( get_field('fc_background', 'option') ): ?>style="background-image: url(<?php  the_field('fc_background', 'option'); ?>);"<?php endif; ?>>
        
        <div class="content group">

            <div class="career-areas">
                
                <div class="grid grid-3">
                        
                    <div class="grid-content">
                    
                        <?php while( have_rows('fc_content', 'option') ): the_row(); ?>
                    
                            <div class="grid-box">
                            
                                <div>
                                
                                    <?php if(get_sub_field('fc_heading', 'option')): ?>
                                        <h3 class="section-title"><?php the_sub_field('fc_heading', 'option'); ?></h3>
                                    <?php endif; ?>
                                    
                                    <?php if(get_sub_field('fc_description', 'option')){the_sub_field('fc_description', 'option');} ?>
                                    
                                    <?php if(get_sub_field('fc_type', 'option') == "External"): ?>

                                        <?php if(get_sub_field('fc_title', 'option') && get_sub_field('fc_link', 'option')): ?>
                                        
                                            <a class="link" href="<?php the_sub_field('fc_link', 'option'); ?>" target="_blank"><?php the_sub_field('fc_title', 'option'); ?></a>
                                            
                                        <?php endif; ?>
                                        
                                    <?php else: ?>

                                        <?php if(get_sub_field('fc_page', 'option')): $id=get_sub_field('fc_page', 'option'); ?>

                                            <a class="link" href="<?php echo get_the_permalink($id); ?>">
                                                <?php if(get_sub_field('fc_title_cu', 'option')){ the_sub_field('fc_title_cu', 'option');}else{ echo get_the_title($id);} ?>
                                            </a>
                                    
                                        <?php endif; ?>
                                    
                                    <?php endif; ?>
                                    
                                </div>
                                
                            </div>

                        <?php endwhile; ?>
                    
                    </div>
                    
                </div>
            
            </div>

        </div>
        
    </div>

<?php endif; ?>

<?php get_footer(); ?>