<?php get_header(); ?>

    <?php if ( have_posts() ) : ?>
    
        <div class="section section-7">
        
            <div class="content group">
            
                <aside class="left-side">

                    <?php if( have_rows('na_navigation'.$GLOBALS['device']) ): ?>
        
                        <ul class="categories">
                        
                            <?php while( have_rows('na_navigation'.$GLOBALS['device']) ): the_row(); ?>
                             
                                <?php if(get_sub_field('na_type'.$GLOBALS['device']) == "External"): ?>
                                
                                    <?php if(get_sub_field('na_title'.$GLOBALS['device']) && get_sub_field('na_link'.$GLOBALS['device'])): ?>
                                    
                                        <li>
                                            <a href="<?php the_sub_field('na_link'.$GLOBALS['device']); ?>" target="_blank"><?php the_sub_field('na_title'.$GLOBALS['device']); ?></a>
                                        </li>
                                        
                                    <?php endif; ?>
                                    
                                <?php else: ?>
                                
                                    <?php if(get_sub_field('na_page'.$GLOBALS['device'])): $id=get_sub_field('na_page'.$GLOBALS['device']); ?>
                                
                                        <li>
                                            <a href="<?php echo get_the_permalink($id); ?>"><?php echo get_the_title($id); ?></a>
                                        </li>
                                
                                    <?php endif; ?>
                                
                                <?php endif; ?>
                            
                            <?php endwhile; ?>
                            
                        </ul>

                    <?php elseif(child_pages() != ""): ?>
                    
                        <?php echo child_pages(); ?>
                        
                    <?php else: ?>
                    
                        <?php wp_nav_menu( array('menu' => 'footer-1')); ?>
                        
                    <?php endif; ?>
                
                </aside>
            
                <div class="center-content">
                
                    <?php if(get_field('co_content'.$GLOBALS['device'])){the_field('co_content'.$GLOBALS['device']);} ?>
                    
                </div>
                
                <aside class="right-side">
                
                    <div class="right-side-testimonials">
                
                        <?php if( have_rows('ft_testimonials'.$GLOBALS['device']) ): ?>
                
                            <?php while( have_rows('ft_testimonials'.$GLOBALS['device']) ): the_row(); ?>
                    
                                <?php if( get_sub_field('ft_testimonial'.$GLOBALS['device']) ): ?>
                                    
                                    <?php $post = get_sub_field('ft_testimonial'.$GLOBALS['device']); setup_postdata($post); ?>
                                    
                                        <div class="featured-testimonial">
                                        
                                            <div class="testimonial" <?php if( get_field('te_image_fe') ): ?>style="background-image: url(<?php the_field('te_image_fe'); ?>);"<?php endif; ?>>
                                                
                                                <?php if( get_field('te_fe_video') ): ?>
                                                    
                                                    <input type="hidden" class="video-iframe" value='<?php the_field('te_fe_video') ?>'/>
                                                    
                                                    <a class="player-btn fa fa-play-circle" aria-hidden="true"></a>
                                                
                                                <?php endif; ?>
                                                
                                                <a href="<?php the_permalink(144); ?>" class="testimonials-link"><i class="fa fa-angle-right"></i>View more Stories</a>
                                            
                                                <div>
                                            
                                                    <?php if( get_field('te_testimonial_fe') ): ?>
                                                        <p>"<?php the_field('te_testimonial_fe'); ?>"</p>
                                                    <?php endif; ?>
                                                    
                                                    <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                                        <h4><?php the_field('te_name'); ?><br/> <?php the_field('te_campus'); ?></h4>
                                                    <?php endif; ?>
                                                    
                                                </div>
                                                
                                            </div>
                                            
                                        </div>

                                    <?php wp_reset_postdata(); ?>
                                    
                                <?php endif; ?>
                                
                            <?php endwhile; ?>
                            
                        <?php else: ?>
                
                            <?php $tag = sanitize_title(get_the_title()); ?>
                        
                            <?php query_posts("post_type=testimonials&tag=$tag&posts_per_page=5"); ?>
                        
                                <?php if(have_posts()) : ?>
                                
                                    <?php while (have_posts()) : the_post(); ?>
                                    
                                        <div class="featured-testimonial">
                                        
                                            <div class="testimonial" <?php if( get_field('te_image_fe') ): ?>style="background-image: url(<?php the_field('te_image_fe'); ?>);"<?php endif; ?>>
                                                
                                                <?php if( get_field('te_fe_video') ): ?>
                                                    
                                                    <input type="hidden" class="video-iframe" value='<?php the_field('te_fe_video') ?>'/>
                                                    
                                                    <a class="player-btn fa fa-play-circle" aria-hidden="true"></a>
                                                
                                                <?php endif; ?>
                                                
                                                <a href="<?php the_permalink(144); ?>" class="testimonials-link"><i class="fa fa-angle-right"></i>View more Stories</a>
                                            
                                                <div>
                                            
                                                    <?php if( get_field('te_testimonial_fe') ): ?>
                                                        <p>"<?php the_field('te_testimonial_fe'); ?>"</p>
                                                    <?php endif; ?>
                                                    
                                                    <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                                        <h4><?php the_field('te_name'); ?><br/> <?php the_field('te_campus'); ?></h4>
                                                    <?php endif; ?>
                                                    
                                                </div>
                                                
                                            </div>
                                            
                                        </div>
                                    
                                    <?php endwhile; ?>
                                
                                <?php endif; ?>
                                            
                            <?php wp_reset_query(); ?>
                            
                        <?php endif; ?>
                        
                    </div>
            		<?php
                        if(is_page(46) || is_page(1026) || is_page(164) || is_page(52) || is_page(60)){ echo do_shortcode( '[personalization-condition 12363]' ); }
                    ?>
                    <div class="program-areas">
                    
                        <h2>Program <b>Areas</b></h2>
                        
                        <div>
                        
                            <p>We offer career training that will help you start in a new rewarding career in a matter of months.</p>
                
                            <?php $area_loop = new WP_Query( array( 'post_type' => 'programs', 'post_parent' => 0, 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1 ) ); if ( $area_loop->have_posts() ) : ?>

                                <ul>

                                    <?php while ( $area_loop->have_posts() ) : $area_loop->the_post(); ?>
                                    
                                        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                                    <?php endwhile; ?>
                                    
                                </ul>
                                
                            <?php wp_reset_postdata(); endif; ?>
                            
                        </div>
                    
                    </div>
                
                </aside>
                
            </div>
            
        </div>

    <?php endif; ?>
		
<?php get_footer(); ?>