<?php get_header(); ?>

<?php if ( $post->post_parent ) : ?>

    <div class="section section-7 single-program">

        <div class="content group">
        
        
            <aside class="left-side">
            
                <ul class="page-navigation vertical group">
                
                    <?php if( have_rows('ta_tabs'.$GLOBALS['device']) ): ?>
                
                        <?php $x=1; while( have_rows('ta_tabs'.$GLOBALS['device']) ): the_row(); ?>
                            
                            <?php if(get_sub_field('ta_title'.$GLOBALS['device'])): ?>
                                <li class="tab-btn-<?php echo $x; ?> <?php if($x==1){echo'active';} ?>"><a onclick="tab(<?php echo $x; ?>, true);"><?php the_sub_field('ta_title'.$GLOBALS['device']); ?></a></li>
                            <?php endif; ?>
                        
                        <?php $x++; endwhile; ?>
                        
                    <?php endif; ?>

                    <?php $campuses_loop = new WP_Query( array( 'post_type' => 'campus', 'meta_query' => array(array('key' => 'av_programs','value' => '"' . get_the_ID() . '"','compare' => 'LIKE')) ) ); if ( count($campuses_loop->posts) !== 1 || !get_field('tech_out')) : ?>
                        <li class="tab-btn-99 ">
                            <a onclick="tab(99, true);">Program Availability</a>
                        </li>
                    <?php endif;?>
                </ul>
                
            </aside>
            
        
            <div class="center-content">
            
            
                <?php if( have_rows('ta_tabs'.$GLOBALS['device']) ): ?>
                
                    <?php $x=1; while( have_rows('ta_tabs'.$GLOBALS['device']) ): the_row(); ?>
                    
                        <ul class="page-navigation vertical mobile-tabs group">
                            <?php if(get_sub_field('ta_title'.$GLOBALS['device'])): ?>
                                <li class="tab-btn-<?php echo $x; ?> <?php if($x==1){echo'active';} ?>"><a onclick="tab(<?php echo $x; ?>, true);"><?php the_sub_field('ta_title'.$GLOBALS['device']); ?></a></li>
                            <?php endif; ?>
                        </ul>
                    
                        <div class="tab" id="tab-<?php echo $x; ?>">
                        <?php if ($x==1):?>
                            <?php
                                global $post;
                                if ($post->ID == 6465 || $post->ID == 116) {
                                    $starting_dates = get_field('custom_starting_dates', 'option');
                                } else {
                                    $starting_dates = get_field('default_starting_dates', 'option');
                                }
                                if ($starting_dates) {
                                    $starting_dates = explode(',', $starting_dates);
                                } else {
                                    $starting_dates = [];
                                }
                                $today = date('Y-m-d');
                                $starting_date = '';
                                foreach ($starting_dates as $value) {
                                    $date_obj = explode('-', $value);
                                    $date = date('Y-m-d', mktime(0,0,0,$date_obj[0],$date_obj[1],$date_obj[2]));
                                    if ($date >= $today) {
                                        $starting_date = date('F j', mktime(0,0,0,$date_obj[0],$date_obj[1],$date_obj[2]));
                                        break;
                                    }
                                }
                            ?>
                            <?php if ($starting_date && $post->ID!=137 && $post->ID!=136 && !get_field('tech_out')): // if program is mibc or project mgmt online don't display start date
                             ?>
                                <div style="margin-bottom: 30px;padding: 20px;background:#AB2E2E; color:#ffffff;text-align: center; font-weight: 500;"><i class="fa fa-calendar"></i> Next Start Date: <?php echo $starting_date; ?> - <a style="color:#ffffff;text-decoration: underline;" href="/request-info/">Get Started Today</a>!</div>
                            <?php endif; ?>
                        <?php endif; ?>
                            <?php if( have_rows('ta_sections'.$GLOBALS['device']) ): ?>
                            
                                <?php while( have_rows('ta_sections'.$GLOBALS['device']) ): the_row(); ?>
                            
                                    <?php if(get_sub_field('ta_description'.$GLOBALS['device'])): ?>
                                
                                        <div class="detail-box">
                                                
                                            <?php the_sub_field('ta_description'.$GLOBALS['device']); ?>
                                            
                                        </div>
                                        
                                    <?php endif; ?>
                                    
                                <?php endwhile; ?>
                            
                            <?php endif; ?>
                            <?php if( strtolower( get_sub_field( 'ta_title'.$GLOBALS['device'] ) ) == 'program overview' ): ?>
                                <?php echo do_shortcode('[fx_courses toggle=true]'); ?>
                            <?php endif; ?>                        
                        </div>
                    
                    <?php $x++; endwhile; ?>
                
                <?php endif; ?>
            
                <div class="tab" id="tab-99">
                    <?php if(get_field('ao_notification')):?>
                        <div class="program-availability program-content">
                        
                            <h2 class="section-title"><?php echo get_the_title();?></h2>
                            
                            <?php $online = false; ?>
                            
                            <?php $campuses_loop = new WP_Query( array( 'post_type' => 'campus', 'meta_query' => array(array('key' => 'av_programs','value' => '"' . get_the_ID() . '"','compare' => 'LIKE')) ) ); if ( $campuses_loop->have_posts() ) : ?>
                            
                                <?php while ( $campuses_loop->have_posts() ) : $campuses_loop->the_post(); ?>
                                
                                    <?php if (get_the_ID() == 26){ $online = true; } ?>

                                <?php endwhile; ?>
                                
                            <?php wp_reset_postdata(); endif; ?>
                            
                            <div class="availability-boxes group">
                            
                                <div class="availability-box <?php if( !get_field('ao_notification') ){echo "no-border";} ?>">
                                    
                                    <?php if($online == false): ?>
                                        <h3>Ground Campuses</h3>
                                    <?php else: ?>
                                        <h3>Online Campus</h3>
                                    <?php endif; ?>
                            
                                    <?php $campuses_loop = new WP_Query( array( 'post_type' => 'campus', 'meta_query' => array(array('key' => 'av_programs','value' => '"' . get_the_ID() . '"','compare' => 'LIKE')) ) ); if ( $campuses_loop->have_posts() ) : ?>
                                    
                                        <ul>
                                        
                                            <?php while ( $campuses_loop->have_posts() ) : $campuses_loop->the_post(); ?>
                                            
                                                <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                                            <?php endwhile; ?>
                                            
                                        </ul>
                                        
                                    <?php wp_reset_postdata(); endif; ?>  
                                </div>
                                
                                <div class="availability-footer group">
                                    <p class="full-width">Vista College ground campuses are accredited by <b>Council on Occupational Education</b></p>                                
                                </div>
                                <div class="availability-box">
                                    <h3 style="text-transform:unset;">Interested in an online program?</h3>
                                    <p>Take most classes whenever you would like, always staying in touch with our knowledgeable, friendly instructors.</p>
                                </div>
                                <?php if( get_field('ao_notification') ): ?>
                                    <div class="availability-box">
                                    
                                        <h3>ONLINE PROGRAMS</h3>
                                        
                                        <ul>
                                            <?php
                                                $ao_programs = get_field('ao_programs', $post->ID);
                                                if ($ao_programs) {
                                                    foreach ($ao_programs as $ao_pro) { ?>
                                                        <li><?php echo $ao_pro; ?></li>
                                                    <?php
                                                    }
                                                }
                                            ?> 
                                        </ul>
                                        
                                    </div>
                                
                                <?php endif; ?>
                                
                            </div>
                            <div class="availability-footer group">
                                <p class="full-width">Vista College online campus is accredited by <b>Accrediting Commission of Career Schools and Colleges</b></p>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="program-availability">
                        
                            <h2 class="section-title">Program <b>Availability</b></h2>
                            
                            <?php $online = false; ?>
                            
                            <?php $campuses_loop = new WP_Query( array( 'post_type' => 'campus', 'meta_query' => array(array('key' => 'av_programs','value' => '"' . get_the_ID() . '"','compare' => 'LIKE')) ) ); if ( $campuses_loop->have_posts() ) : ?>
                            
                                <?php while ( $campuses_loop->have_posts() ) : $campuses_loop->the_post(); ?>
                                
                                    <?php if (get_the_ID() == 26){ $online = true; } ?>

                                <?php endwhile; ?>
                                
                            <?php wp_reset_postdata(); endif; ?>
                            
                            <div class="availability-boxes group">
                            
                                <div class="availability-box <?php if( !get_field('ao_notification') ){echo "no-border";} ?>">
                                    
                                    <?php if($online == false): ?>
                                        <h3>Ground Campuses</h3>
                                    <?php else: ?>
                                        <h3>Online Campus</h3>
                                    <?php endif; ?>
                            
                                    <?php $campuses_loop = new WP_Query( array( 'post_type' => 'campus', 'meta_query' => array(array('key' => 'av_programs','value' => '"' . get_the_ID() . '"','compare' => 'LIKE')) ) ); if ( $campuses_loop->have_posts() ) : ?>
                                    
                                        <ul>
                                        
                                            <?php while ( $campuses_loop->have_posts() ) : $campuses_loop->the_post(); ?>
                                            
                                                <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                                            <?php endwhile; ?>
                                            
                                        </ul>
                                        
                                    <?php wp_reset_postdata(); endif; ?>
                                    
                                </div>
                                
                                <?php if( get_field('ao_notification') && $online == false ): ?>
                                
                                    <?php $online_loop = new WP_Query( array( 'p' => 26, 'post_type' => 'campus' ) ); if ( $online_loop->have_posts() ) : ?>
                                    
                                        <?php while ( $online_loop->have_posts() ) : $online_loop->the_post(); ?>
                                
                                            <div class="availability-box">
                                            
                                                <h3>Program Also Available At</h3>
                                                
                                                <ul>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                </ul>
                                                
                                            </div>
                                            
                                        <?php endwhile; ?>
                                    
                                    <?php wp_reset_postdata(); endif; ?>
                                
                                <?php endif; ?>
                                
                            </div>
                            
                            <div class="availability-footer group">
                                
                                <?php if( $online == false ): ?>
                                    <p <?php if( !get_field('ao_notification') ){echo "class='full-width'";} ?>>Vista College ground campuses are accredited by <b>Council on Occupational Education</b></p>
                                <?php endif; ?>
                                
                                <?php if( get_field('ao_notification') || $online == true ): ?>
                                    <p <?php if( $online == true ){echo "class='full-width'";} ?>>Vista College online campus is accredited by <b>Accrediting Commission of Career Schools and Colleges</b></p>
                                <?php endif; ?>
                            
                            </div>
                            
                        </div>
                    <?php endif; ?>
                </div>
                
            
            </div>
            
            
            <aside class="right-side">
            
                <div class="right-side-testimonials">
            
                    <?php if( have_rows('ft_testimonials'.$GLOBALS['device']) ): ?>
                
                        <?php while( have_rows('ft_testimonials'.$GLOBALS['device']) ): the_row(); ?>
                
                            <?php if( get_sub_field('ft_testimonial'.$GLOBALS['device']) ): ?>
                                
                                <?php $post = get_sub_field('ft_testimonial'.$GLOBALS['device']); setup_postdata($post); ?>
                                
                                    <div class="featured-testimonial">
                                    
                                        <div class="testimonial" <?php if( get_field('te_image_fe') ): ?>style="background-image: url(<?php the_field('te_image_fe'); ?>);"<?php endif; ?>>
                                            
                                            <?php if( get_field('te_fe_video') ): ?>
                                                
                                                <input type="hidden" class="video-iframe" value='<?php the_field('te_fe_video') ?>'/>
                                                
                                                <a class="player-btn fa fa-play-circle" aria-hidden="true"></a>
                                            
                                            <?php endif; ?>
                                            
                                            <a href="<?php the_permalink(144); ?>" class="testimonials-link"><i class="fa fa-angle-right"></i>View more Stories</a>
                                        
                                            <div>
                                        
                                                <?php if( get_field('te_testimonial_fe') ): ?>
                                                    <p>"<?php the_field('te_testimonial_fe'); ?>"</p>
                                                <?php endif; ?>
                                                
                                                <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                                    <h4><?php the_field('te_name'); ?><br/> <?php the_field('te_campus'); ?></h4>
                                                <?php endif; ?>
                                                
                                            </div>
                                            
                                        </div>
                                        
                                    </div>

                                <?php wp_reset_postdata(); ?>
                                
                            <?php endif; ?>
                            
                        <?php endwhile; ?>
                        
                    <?php else: ?>
                
                        <?php $tag = sanitize_title(get_the_title()); ?>
                    
                        <?php query_posts("post_type=testimonials&tag=$tag&posts_per_page=5"); ?>
                    
                            <?php if(have_posts()) : ?>
                            
                                <?php while (have_posts()) : the_post(); ?>
                                
                                    <div class="featured-testimonial">
                                    
                                        <div class="testimonial" <?php if( get_field('te_image_fe') ): ?>style="background-image: url(<?php the_field('te_image_fe'); ?>);"<?php endif; ?>>
                                            
                                            <?php if( get_field('te_fe_video') ): ?>
                                                
                                                <input type="hidden" class="video-iframe" value='<?php the_field('te_fe_video') ?>'/>
                                                
                                                <a class="player-btn fa fa-play-circle" aria-hidden="true"></a>
                                            
                                            <?php endif; ?>
                                            
                                            <a href="<?php the_permalink(144); ?>" class="testimonials-link"><i class="fa fa-angle-right"></i>View more Stories</a>
                                        
                                            <div>
                                        
                                                <?php if( get_field('te_testimonial_fe') ): ?>
                                                    <p>"<?php the_field('te_testimonial_fe'); ?>"</p>
                                                <?php endif; ?>
                                                
                                                <?php if( get_field('te_name') && get_field('te_campus') ): ?>
                                                    <h4><?php the_field('te_name'); ?><br/> <?php the_field('te_campus'); ?></h4>
                                                <?php endif; ?>
                                                
                                            </div>
                                            
                                        </div>
                                        
                                    </div>
                                
                                <?php endwhile; ?>
                            
                            <?php endif; ?>
                                        
                        <?php wp_reset_query(); ?>
                        
                    <?php endif; ?>
                    
                </div>
                
                <?php if( get_field('yl_enable'.$GLOBALS['device']) ): ?>
                
                    <?php if( have_rows('yl_learn'.$GLOBALS['device']) ): ?>
                
                        <div class="learn-how-to">
                        
                            <h2>You'll Learn How To</h2>
                            
                            <div>
                            
                                <ul>
                                
                                    <?php while( have_rows('yl_learn'.$GLOBALS['device']) ): the_row(); ?>
                                     
                                        <?php if(get_sub_field('yl_type'.$GLOBALS['device']) == "External"): ?>
                                        
                                            <?php if(get_sub_field('yl_title'.$GLOBALS['device']) && get_sub_field('yl_link'.$GLOBALS['device'])): ?>
                                            
                                                <li>
                                                    <a href="<?php the_sub_field('yl_link'.$GLOBALS['device']); ?>" target="_blank"><?php the_sub_field('yl_title'.$GLOBALS['device']); ?></a>
                                                </li>
                                                
                                            <?php endif; ?>
                                            
                                        <?php else: ?>
                                        
                                            <?php if(get_sub_field('yl_page'.$GLOBALS['device'])): $id=get_sub_field('yl_page'.$GLOBALS['device']); ?>
                                        
                                                <li>
                                                    <a href="<?php echo get_the_permalink($id); ?>"><?php echo get_the_title($id); ?></a>
                                                </li>
                                        
                                            <?php endif; ?>
                                        
                                        <?php endif; ?>
                                    
                                    <?php endwhile; ?>
                                    
                                </ul>
                                
                            </div>
                        
                        </div>
                        
                    <?php endif; ?>
                
                <?php endif; ?>
                
                
                <?php if( get_field('rt_enable'.$GLOBALS['device']) ): ?>
                
                    <?php if( have_rows('rt_topics'.$GLOBALS['device']) ): ?>
                
                        <div class="program-areas relevant-topics">
                        
                            <h2>Relevant and<br/><b>Cutting-edge topics</b></h2>
                            
                            <div>
                            
                                <ul>
                                    
                                    <?php while( have_rows('rt_topics'.$GLOBALS['device']) ): the_row(); ?>
                                        
                                        <?php if(get_sub_field('rt_type'.$GLOBALS['device']) == "External"): ?>
                                        
                                            <?php if(get_sub_field('rt_title'.$GLOBALS['device']) && get_sub_field('rt_link'.$GLOBALS['device'])): ?>
                                            
                                                <li>
                                                    <a href="<?php the_sub_field('rt_link'.$GLOBALS['device']); ?>" target="_blank"><?php the_sub_field('rt_title'.$GLOBALS['device']); ?></a>
                                                </li>
                                                
                                            <?php endif; ?>
                                            
                                        <?php else: ?>
                                        
                                            <?php if(get_sub_field('rt_post'.$GLOBALS['device'])): $id=get_sub_field('rt_post'.$GLOBALS['device']); ?>
                                        
                                                <li>
                                                    <a href="<?php echo get_the_permalink($id); ?>"><?php echo get_the_title($id); ?></a>
                                                </li>
                                        
                                            <?php endif; ?>
                                        
                                        <?php endif; ?>
                                    
                                    <?php endwhile; ?>
                                    
                                </ul>
                                
                            </div>
                        
                        </div>
                        
                    <?php endif; ?>
                    
                <?php endif; ?>
            
            </aside>
            
            
        </div>
        
    </div>

    <?php if( have_rows('fc_content', 'option') ): ?>
        <?php if (!get_field('tech_out')) : ?> 
            <div class="footer-top">
                <div class="footer-left-top">
                    <div class="left-side-top">
                        <div class="left-side-title">
                            <h1>Let's Get Started!</h1>
                            <span>Questions? An Admission Representative would love to help<span>
                        </div>
                    </div>
                    <div class="left-side-bottom">
                        <input type="hidden" name="online_status" id="online_status" value="<?php echo $online;?>">
                        <select id="campus_select">
                            <option value="">Which Campus are You Interested in?</option>
                            <?php $campuses_loop = new WP_Query( array( 'post_type' => 'campus', 'meta_query' => array(array('key' => 'av_programs','value' => '"' . get_the_ID() . '"','compare' => 'LIKE')) ) ); if ( $campuses_loop->have_posts() ) : ?>                            
                                <?php while ( $campuses_loop->have_posts() ) : $campuses_loop->the_post(); ?>
                                    <option value="<?php $value = get_the_ID()!=26 ? the_title(): 'Online Campus';echo $value; ?>"><?php the_title(); ?></option>
                                <?php endwhile; ?>

                            <?php wp_reset_postdata(); endif; ?>

                            <?php /*if( get_field('ao_notification')): ?>
                            
                                <?php $online_loop = new WP_Query( array( 'p' => 26, 'post_type' => 'campus' ) ); if ( $online_loop->have_posts() ) : ?>
                                
                                    <?php while ( $online_loop->have_posts() ) : $online_loop->the_post(); ?>
                                        <option value="Online Campus"><?php the_title(); ?></option>                                
                                    <?php endwhile; ?>
                                
                                <?php wp_reset_postdata(); endif; ?>
                            
                            <?php endif; */?>                    
                        </select>
                        <a class="get-more-info" onClick="_gaq.push(['_trackEvent', 'RFI', 'Click', 'NextStep']);">REQUEST INFO <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="footer-right-top"></div>
            </div>
            <div class="started-program">
                <a class="get-started-today" onClick="_gaq.push(['_trackEvent', 'RFI', 'Click', 'NextStep']);">Get Started Today!</a>
            </div>
        <?php endif; ?>
    <?php endif; ?>

<?php else: ?>
    <?php
        $args = array(
            'post_parent' => $post->ID,
            'post_type'   => 'programs', 
            'numberposts' => -1,
            'post_status' => 'any' 
        );
        $children = get_children( $args );
        if (sizeof($children)>0) {
           include "single-sub-programs.php";
        } else {
            include "single-programs-update.php";
        }
    ?>
<?php endif; ?>
    

<?php get_footer(); ?>