<?php


// Detect Device
function device_detect(){
    if(wp_is_mobile() && get_field('de_enable')){
        $GLOBALS['device'] = "_mo";
    }
    else {
        $GLOBALS['device'] = "";
    }
}
add_action('wp_head', 'device_detect');

// Session
function ses_init() {
if (!session_id())
  session_start();
}
add_action('init','ses_init');


// ACF Theme
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title'    => 'Theme Settings',
        'menu_title'    => 'Theme Settings',
        'menu_slug'    => 'theme-settings',
        'capability'    => 'manage_options',
        'parent_slug'    => '',
        'position'    => 30,
        'icon_url'    => 'dashicons-layout',
    ));
}

// Filter Yoast Meta Priority
add_filter( 'wpseo_metabox_prio', function() { return 'low';});


// Script, Style
function enqueue_files() {
    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Roboto+Slab:700|Roboto:300,300i,400,400i,500,700|Vesper+Libre:400,700', array(), null );
    wp_enqueue_style( 'font-awesome', get_template_directory_uri().'/font/font-awesome/css/font-awesome.min.css', array(), null );

    wp_enqueue_style('bootstrap-grid', get_template_directory_uri()."/css/bootstrap-grid.css");

    wp_enqueue_style( 'main-style', get_template_directory_uri().'/style.css', array(), null );

    wp_enqueue_style('main-blog', get_template_directory_uri()."/css/main-blog.css");

    wp_enqueue_script( 'bootstrap', get_template_directory_uri().'/js/bootstrap.min.js', array('jquery'), null );
    wp_enqueue_script( 'main-script', get_template_directory_uri().'/js/main.js', array('jquery'), null );
    wp_enqueue_script( 'glide-script', get_template_directory_uri().'/js/glide.js', array('jquery'), null );
    wp_enqueue_script( 'request-script-2', get_template_directory_uri().'/js/requests-2.js', array('jquery'), null );
    wp_enqueue_script( 'request-script-3', get_template_directory_uri().'/js/requests-3.js', array('jquery'), null );


  wp_enqueue_script( 'fx-plugins', get_template_directory_uri().'/js/fx-plugins.js');
  wp_enqueue_script( 'fx-scripts', get_template_directory_uri().'/js/fx-scripts.js');

    if(is_front_page()){
        wp_enqueue_script( 'slideshow-script', get_template_directory_uri().'/js/slideshow.js', array('jquery'), null );
        wp_enqueue_script( 'request-script-1', get_template_directory_uri().'/js/requests-1.js', array('jquery'), null );
    }
    if(wp_is_mobile()){
        wp_enqueue_style( 'mobile-style', get_template_directory_uri().'/mobile.css', array(), null );
        wp_enqueue_script( 'mobile-script', get_template_directory_uri().'/js/mobile.js', array('jquery'), null );
    }
}
add_action( 'wp_enqueue_scripts', 'enqueue_files' );


// login Style
function my_login_logo() {
    if( get_field('logo_dark', 'option') ) {
    $logo = get_field('logo_dark', 'option'); ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo $logo['sizes']['thumbnail']; ?>);
            width: 320px;
            height: 127px;
            background-size: contain;
            margin-bottom: 30px;
        }
    </style>
<?php } }
add_action( 'login_enqueue_scripts', 'my_login_logo' );


// Admin Style
function my_admin_style() { ?>
    <style type="text/css">
        #toplevel_page_aiowpsec,
        #toplevel_page_social-warfare {
            display: none;
        }
    </style>
<?php }
add_action( 'admin_enqueue_scripts', 'my_admin_style' );


// Admin Favicon
function admin_favicon(){
    if(get_field('favicon', 'option')){
        echo '<link rel="shortcut icon" href="' . get_field('favicon', 'option') . '" />',"\n";
    }
}
add_action('login_head','admin_favicon');
add_action('admin_head','admin_favicon');


// Support
function custom_theme_setup() {
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_theme_support('page-template');
    add_theme_support('widgets');
    add_theme_support('menus');
    add_theme_support('woocommerce');
}
add_action( 'after_setup_theme', 'custom_theme_setup' );


// Sidebars
register_sidebar (array(
    'name'          => 'Language chooser',
    'id'            => 'language_chooser',
    'description'   => 'Language chooser sidebar',
    'class'         => '',
    'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>'
));


// Input check
function check($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}


// Remove Emojicons
function disable_wp_emojicons() {
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
}
add_action( 'init', 'disable_wp_emojicons' );


// Child Pages
function child_pages() {

    global $post;

    $post_id = $post->ID;


    $args = array(
        'post_parent' => $post_id,
        'post_type'   => 'page',
        'numberposts' => -1,
        'post_status' => 'any'
    );
    $children = get_children( $args );


    if ( empty($children) && $post->post_parent != 0 ) {

        $post_id = $post->post_parent;

        $args = array(
            'post_parent' => $post_id,
            'post_type'   => 'page',
            'numberposts' => -1,
            'post_status' => 'any'
        );
        $children = get_children( $args );

    }

    $pages = array();

    foreach ($children as $value) {
        array_push($pages,$value->ID);
    }

    if(!empty($pages)){

        array_push($pages,$post_id);

        $navigation = wp_list_pages( array(
            'sort_column' => 'menu_order',
            'title_li' => '',
            'depth' => 2,
            'echo' => 0,
            'include' => $pages
        ) );
        if($post_id==12218){
            $navigation = '<ul class="categories"><li class="page_item page-item-'.$post_id.' page_item_has_children current_page_item"><a href="https://www.vistacollege.edu/careerservices/career-goals-and-strategy/" aria-current="page">My Career</a>
            <ul class="children">'.$navigation.'</ul></li></ul>';
        }
        else if($post_id===12141){
            $navigation = '<ul class="categories"><li class="page_item page-item-'.$post_id.' page_item_has_children current_page_item"><a href="https://www.vistacollege.edu/careerservices/getting-the-job/" aria-current="page">Getting the Job</a>
            <ul class="children">'.$navigation.'</ul></li></ul>';
        }
        else{
            $navigation = '<ul class="categories">' . $navigation . '</ul>';
        }
        
        return $navigation;

    }

}


// Breadcrumb
function get_breadcrumb() {

    $text['home']     = 'Home';
    $text['category'] = 'Archive by Category "%s"';
    $text['search']   = 'Search Results for "%s"';
    $text['tag']      = 'Posts Tagged "%s"';
    $text['author']   = 'Articles Posted by %s';
    $text['404']      = 'Error 404';

    $show_current   = 1;
    $show_on_home   = 0;
    $show_home_link = 1;
    $show_title     = 1;
    $delimiter      = ' &raquo; ';
    $before         = '<span class="current">';
    $after          = '</span>';

    global $post;
    $home_link    = site_url();
    $link_before  = '<span typeof="v:Breadcrumb">';
    $link_after   = '</span>';
    $link_attr    = ' rel="v:url" property="v:title"';
    $link         = $link_before . '<a' . $link_attr . ' href="%1$s">%2$s</a>' . $link_after;
    $parent_id    = $parent_id_2 = $post->post_parent;
    $frontpage_id = get_option('page_on_front');

    if (is_home() || is_front_page()) {

        if ($show_on_home == 1) echo '<div class="breadcrumbs"><span class="first-page"><a href="' . $home_link . '">' . $text['home'] . '</a></span></div>';

    } else {

        echo '<div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">';
        if ($show_home_link == 1) {
            echo '<span class="first-page"><a href="' . $home_link . '" rel="v:url" property="v:title">' . $text['home'] . '</a></span>';
            if ($frontpage_id == 0 || $parent_id != $frontpage_id) echo $delimiter;
        }

        if ( is_category() ) {
            $this_cat = get_category(get_query_var('cat'), false);
            if ($this_cat->parent != 0) {
                $cats = get_category_parents($this_cat->parent, TRUE, $delimiter);
                if ($show_current == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
                $cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
                $cats = str_replace('</a>', '</a>' . $link_after, $cats);
                if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
                echo $cats;
            }
            if ($show_current == 1) echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;

        } elseif ( is_search() ) {
            echo $before . sprintf($text['search'], get_search_query()) . $after;

        } elseif ( is_day() ) {
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
            echo sprintf($link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $delimiter;
            echo $before . get_the_time('d') . $after;

        } elseif ( is_month() ) {
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
            echo $before . get_the_time('F') . $after;

        } elseif ( is_year() ) {
            echo $before . get_the_time('Y') . $after;

        } elseif ( is_single() && !is_attachment() ) {
            if ( get_post_type() != 'post' ) {
                $post_type = get_post_type_object(get_post_type());
                $slug = $post_type->rewrite;
                printf($link, $home_link . '/' . $slug['slug'] . '/', $post_type->labels->name);

                if ( get_post_type() == 'programs' ) {

                    if ( $post->post_parent ) {

                        global $post;
                        echo $delimiter . "<span typeof='v:Breadcrumb'><a rel='v:url' property='v:title' href='" . get_permalink( $post->post_parent ) . "'>" . get_the_title( $post->post_parent ) . "</a></span>";

                    }

                }

                if ($show_current == 1) {

                    $short_obj = get_post_meta($post->ID, 'rp_short_title');
                    $short_title = $short_obj[0] == "" ? get_the_title() : $short_obj[0];
                    $page_title = get_field('degree_type') ? get_field('degree_type') . ' in ' . $short_title : $short_title;
                    echo $delimiter . $before . $page_title . $after;
                }
            } else {
                $cat = get_the_category(); $cat = $cat[0];
                $cats = get_category_parents($cat, TRUE, $delimiter);
                if ($show_current == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
                $cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
                $cats = str_replace('</a>', '</a>' . $link_after, $cats);
                if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
                echo $cats;
                if ($show_current == 1) echo $before . get_the_title() . $after;
            }

        } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
            $post_type = get_post_type_object(get_post_type());
            echo $before . $post_type->labels->name . $after;

        } elseif ( is_attachment() ) {
            $parent = get_post($parent_id);
            $cat = get_the_category($parent->ID); $cat = $cat[0];
            $cats = get_category_parents($cat, TRUE, $delimiter);
            $cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
            $cats = str_replace('</a>', '</a>' . $link_after, $cats);
            if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
            echo $cats;
            printf($link, get_permalink($parent), $parent->post_title);
            if ($show_current == 1) echo $delimiter . $before . get_the_title() . $after;

        } elseif ( is_page() && !$parent_id ) {
            if ($show_current == 1) echo $before . get_the_title() . $after;

        } elseif ( is_page() && $parent_id ) {
            if ($parent_id != $frontpage_id) {
                $breadcrumbs = array();

                while ($parent_id) {
                    $page = get_page($parent_id);
                    if ($parent_id != $frontpage_id) {
                        $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
                    }
                    $parent_id = $page->post_parent;
                }
                $breadcrumbs = array_reverse($breadcrumbs);
                for ($i = 0; $i < count($breadcrumbs); $i++) {
                    echo $breadcrumbs[$i];
                    if ($i != count($breadcrumbs)-1) echo $delimiter;
                }
            }
            if ($show_current == 1) {
                if ($show_home_link == 1 || ($parent_id_2 != 0 && $parent_id_2 != $frontpage_id)) echo $delimiter;
                echo $before . get_the_title() . $after;
            }

        } elseif ( is_tag() ) {
            echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;

        } elseif ( is_author() ) {
            global $author;
            $userdata = get_userdata($author);
            echo $before . sprintf($text['author'], $userdata->display_name) . $after;

        } elseif ( is_404() ) {
            echo $before . $text['404'] . $after;
        }

        if ( get_query_var('paged') ) {
            if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
            echo __('Page') . ' ' . get_query_var('paged');
            if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
        }

        echo '</div><!-- .breadcrumbs -->';

    }

}


// Disable Theme & Plugin Editor
function disable_theme_editor() {
  define('DISALLOW_FILE_EDIT', TRUE);
}
add_action('init','disable_theme_editor');

// check if mobile content

function villacollege_post_has_alt_mobile_content()
{
  return get_field('de_enable') && wp_is_mobile();
}

add_action('post_has_villacollege_alt_mobile_content', 'villacollege_post_has_alt_mobile_content');

// Get post content

function villacollege_get_the_content()
{
  $has_alt_mobile_cont = villacollege_post_has_alt_mobile_content();

  $og_the_content = get_the_content();
  $has_alt_the_content = get_field('po_post');

  if ($og_the_content == "" || $has_alt_the_content){
    if ($has_alt_mobile_cont){
      $the_content = get_field('po_post_mo');
    } else {
      $the_content =  $has_alt_the_content;
    }
  } else {
    $the_content = $og_the_content;
  }

  return $the_content;
}

add_action( 'get_villacollege_the_content', 'villacollege_get_the_content');

// Get excerpt

function villacollege_excerpt($len = 120)
{
  $the_content = villacollege_get_the_content();

  $the_content_shorten = substr(strip_tags($the_content), 0, $len);

  return $the_content_shorten;
}

add_action( 'get_villacollege_excerpt', 'villacollege_excerpt');

// Post thumbnail

function villacollege_get_post_thumbnail ()
{
  if (has_post_thumbnail()) {

    return array(
      'alt' => get_the_post_thumbnail_caption(),
      'url' => get_the_post_thumbnail_url()
    );

  } else {

    if (villacollege_post_has_alt_mobile_content()) {
      return get_field('po_image_mo');
    } else {
      return get_field('po_image');
    }

  }

}


add_action( 'get_villacollege_post_thumbnail', 'villacollege_get_post_thumbnail');


// Search Pagination
function search_nav() {

    if( is_singular() )
        return;

    global $wp_query;

    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    if ( $paged >= 1 )
        $links[] = $paged;

    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<ul class="pagination">' . "\n";

    if ( get_previous_posts_link() ) {
        $pageto = $paged - 1;
        echo '<li class="prev-page"><a href="?s=' . get_search_query()  . '&paged=' . $pageto . '">« Previous</a></li>';
    }
    else {
        echo '<li class="prev-page"></li>';
    }

    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, '?s=' . get_search_query()  . '&paged=' . '1', '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }

    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, '?s=' . get_search_query()  . '&paged=' . $link, $link );
    }

    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, '?s=' . get_search_query()  . '&paged=' . $max, $max );
    }

    if ( get_next_posts_link() ){
        $pageto = $paged + 1;
        echo '<li class="next-page"><a href="?s=' . get_search_query()  . '&paged=' . $pageto . '">Next »</a></li>';
    }
    else {
        echo '<li class="next-page"></li>';
    }

    echo '</ul>' . "\n";

}


// Posts Pagination
function posts_nav() {

    if( is_singular() )
        return;

    global $wp_query;

    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    if ( $paged >= 1 )
        $links[] = $paged;

    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<ul class="pagination">' . "\n";

    if ( get_previous_posts_link() ) {
        printf( '<li class="prev-page">%s</li>' . "\n", get_previous_posts_link('« Previous') );
    }
    else {
        echo '<li class="prev-page"></li>';
    }

    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }

    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }

    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }

    if ( get_next_posts_link() ){
        printf( '<li class="next-page">%s</li>' . "\n", get_next_posts_link('Next »') );
    }
    else {
        echo '<li class="next-page"></li>';
    }

    echo '</ul>' . "\n";

}


//Shortcode [campuses]
function shortcode_campuses(){

    $html = '<div class="program-availability shortcode"><div class="availability-boxes group"><div class="availability-box"><h3>Ground Campuses</h3><ul>';

    $campuses_loop = new WP_Query( array( 'post_type' => 'campus', 'post__not_in' => array(26), 'posts_per_page' => -1 ) ); if ( $campuses_loop->have_posts() ){

        while ( $campuses_loop->have_posts() ){ $campuses_loop->the_post();

            $html .= '<li><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></li>';

        }

        wp_reset_postdata();

    }

    $html .= '</ul></div>';

    $online_loop = new WP_Query( array( 'p' => 26, 'post_type' => 'campus' ) ); if ( $online_loop->have_posts() ){

        while ( $online_loop->have_posts() ){ $online_loop->the_post();

            $html .= '<div class="availability-box"><h3>Online Campus</h3><ul><li><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></li></ul></div>';

        }

        wp_reset_postdata();

    }

    $html .= '</div><div class="availability-footer group"><p>Vista College ground campuses are accredited by <b>Council on Occupational Education</b></p><p>Vista College online campus is accredited by <b>Accrediting Commission of Career Schools and Colleges</b></p></div></div>';

    return $html;

}
add_shortcode( 'campuses', 'shortcode_campuses' );


//Excerpt
function excerpt($id){

    if(get_post_meta($id, '_yoast_wpseo_metadesc', true)){
        $excerpt = get_post_meta($id, '_yoast_wpseo_metadesc', true);
    }
    elseif(get_field('po_post'.$GLOBALS['device'], $id)){
        $excerpt = get_field('po_post'.$GLOBALS['device'], $id);
    }
    elseif(get_field('co_content'.$GLOBALS['device'], $id)){
        $excerpt = get_field('co_content'.$GLOBALS['device'], $id);
    }
    else {
        $page_data = get_page($id);
        if($page_data){
            $excerpt = $page_data->post_content;
        }
    }

    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt,0,170);
    $excerpt = substr($excerpt, 0, strrpos( $excerpt, ' '));
    if($excerpt != ""){echo $excerpt . "...";}
}


//Shortcode [mobile]
function shortcode_mobile(){
    return '<span class="mobile-content">';
}
add_shortcode( 'mobile', 'shortcode_mobile' );


//Shortcode [desktop]
function shortcode_desktop(){
    return '<span class="desktop-content">';
}
add_shortcode( 'desktop', 'shortcode_desktop' );


//Shortcode [device]
function shortcode_device(){
    return '</span>';
}
add_shortcode( 'device', 'shortcode_device' );


// Variables
$url = $_SERVER['REQUEST_URI'];
$variable = explode('=', $url);

parse_str(parse_url($url, PHP_URL_QUERY), $params);

if(isset($params["program"])){
    $GLOBALS['program'] = check($params["program"]);
}
elseif(($variable[1])&&(strpos($url, 'program') !== false)){
    $GLOBALS['program'] = urldecode($variable[1]);
}

if (isset($params['gclid'])) {
    $GLOBALS['source'] = check($params["gclid"]);
    setcookie('source', $GLOBALS['source'], time() + (86400 * 30), "/");
} else if(isset($params["utm_ls"])){
    $GLOBALS['source'] = check($params["utm_ls"]);
    setcookie('source', $GLOBALS['source'], time() + (86400 * 30), "/");
} else if(($variable[1])&&(strpos($url, 'utm_ls') !== false)){
  $GLOBALS['source'] = urldecode($variable[1]);
}
if (isset($_COOKIE['source'])) {
    $GLOBALS['source'] = $_COOKIE['source'];
    if(!isset($_SESSION['source'])){
        session_start();
        $_SESSION['source']=$GLOBALS['source'];
    }
}

if ( function_exists( 'acf_add_options_sub_page' ) ){
    acf_add_options_sub_page(array(
        'title'      => 'Starting Date',
        'parent'     => 'edit.php?post_type=programs',
        'capability' => 'manage_options'
    ));
}


function cf_search_join( $join ) {
    global $wpdb;
    if ( is_search() ) {
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }
    return $join;
}
add_filter('posts_join', 'cf_search_join' );
function cf_search_where( $where ) {
    global $pagenow, $wpdb;
    if ( is_search() ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
    }
    return $where;
}
add_filter( 'posts_where', 'cf_search_where' );
function cf_search_distinct( $where ) {
    global $wpdb;
    if ( is_search() ) {
        return "DISTINCT";
    }
    return $where;
}
add_filter( 'posts_distinct', 'cf_search_distinct' );

$onlinePage = false;

add_filter( 'gform_field_value_LevelAgencyExternalID', 'populate_LevelAgencyExternalID' );
function populate_LevelAgencyExternalID( $value ) {
  return $_COOKIE['expid'];
}

add_filter( 'gform_field_value_utm_placement', 'populate_utm_placement' );
function populate_utm_placement( $value ) {
  return $_SESSION['utm_placement'];
}

add_filter( 'gform_field_value_gclid', 'populate_gclid' );
function populate_gclid( $value ) {
  return $_SESSION['gclid'];
}

add_filter( 'gform_field_value_utm_sourcelvl', 'populate_utm_sourcelvl' );
function populate_utm_sourcelvl( $value ) {
  return $_SESSION['utm_source'];
}
add_filter( 'gform_field_value_utm_campaign', 'populate_utm_campaign' );
function populate_utm_campaign( $value ) {
  return $_SESSION['utm_campaign'];
}

add_filter( 'gform_field_value_utm_content', 'populate_utm_content' );
function populate_utm_content( $value ) {
  return $_SESSION['utm_content'];
}


add_filter( 'gform_field_value_utm_device', 'populate_utm_device' );
function populate_utm_device( $value ) {
  return $_SESSION['utm_device'];
}

add_filter( 'gform_field_value_utm_phone', 'populate_utm_phone' );
function populate_utm_phone( $value ) {
  return $_SESSION['utm_phone'];
}

add_filter( 'gform_field_value_utm_term', 'populate_utm_term' );
function populate_utm_term( $value ) {
  return $_SESSION['utm_term'];
}

add_filter( 'gform_field_value_utm_adposition', 'populate_utm_adposition' );
function populate_utm_adposition( $value ) {
  return $_SESSION['utm_adposition'];
}

add_filter( 'gform_field_value_utm_medium', 'populate_utm_medium' );
function populate_utm_medium( $value ) {
  return $_SESSION['utm_medium'];
}

function programs_offered_shortcode( $atts, $content = null ) {
    extract( shortcode_atts( array(
       'order' => 'area'
       ), $atts ) );
    $ids = get_field('av_programs');
    $programs = array();
    foreach($ids as $progid) {
        $title = get_field('rp_short_title', $progid);
        $pro_post = get_post($progid);
        $parent_post = get_post($pro_post->post_parent);
        $programs[] = array(
            'title' => $title ? $title : get_the_title( $progid ),
            'degree' => get_field('degree_type', $progid),
            'link' => get_permalink($progid),
            'course_length' => get_field( 'course_length', $progid),
            'icon_image' => get_field( 'icon_image', $progid),
            'parent' => $parent_post->post_title
        );
    }
    $columns = array_column($programs, $order);
    array_multisort($columns, SORT_ASC, $programs);
    if ($order === 'parent') {
        usort($programs, function($a, $b) {
            $sorting = array('Healthcare', 'Business', 'Technology', 'Legal', 'Cosmetology', 'Trades');
            return array_search($a['parent'], $sorting) - array_search($b['parent'], $sorting);
        });
    } else if ($order === 'degree') {
        usort($programs, function($a, $b) {
            $sorting = array('Certificate', 'Diploma', 'Associate of Applied Science', 'Bachelor');
            return array_search($a['degree'], $sorting) - array_search($b['degree'], $sorting);
        });
    }
    $degree = '';
    $parent = '';
    foreach( $programs as $pro) {
        if ($order === 'parent' && $parent !== $pro['parent']) {
            $parent = $pro['parent'];
            echo "<div class='col-md-12'><h3>".$parent."</h3></div>";
        }
        if ($order === 'degree' && $degree !== $pro['degree']) {
            $degree = $pro['degree'];
            echo "<div class='col-md-12'><h3>".$degree."</h3></div>";
        }
        ?>
        <div class="col-md-6 custom-program">
            <div>
                <a href="<?php echo $pro['link']; ?>">
                    <img src="<?=$pro['icon_image']; ?>" alt="Program Icon">
                </a>
            </div>
            <div>
                <a href="<?php echo $pro['link']; ?>"><?=$pro['title']; ?></a><BR><?=$pro['degree']; ?>
                <div class="program-divider"></div>
                <p>Course Length: <strong><?=$pro['course_length']; ?></strong></p>
            </div>
        </div>
        <?php
    }
}

add_shortcode('programs_offered', 'programs_offered_shortcode');
