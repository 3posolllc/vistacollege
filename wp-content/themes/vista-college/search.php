<?php get_header(); ?>


<div class="section section-7">
        
    <div class="content group">
    
        <aside class="left-side">
    
            <?php if(child_pages() != ""): ?>
                <?php echo child_pages(); ?>
            <?php else: ?>
                <?php wp_nav_menu( array('menu' => 'footer-1')); ?>
            <?php endif; ?>
        
        </aside>
    
        <div class="center-content">
        
            <?php wp_reset_postdata(); ?>
        
            <form class="search-page-form" action="/" autocomplete="off" method="get">
                <input class="search-input" type="text" name="s" value="<?php the_search_query(); ?>" placeholder="Start typing..." />
                <button type="submit" class="search-submit">
                    <i class="fa fa-search"></i><span>Search</span>
                </button>
            </form>
        
            <?php if(have_posts()) : ?>

                <?php while (have_posts()) : the_post(); ?>
                
                    <?php 
                        if(wp_is_mobile() && get_field('de_enable')){
                            $GLOBALS['device'] = "_mo"; 
                        }
                        else {
                            $GLOBALS['device'] = "";
                        }
                    ?>
                    
                    <div class="search-result group">
                        
                        <a href="<?php the_permalink(); ?>">
                            <?php if(get_field('po_image'.$GLOBALS['device'])): ?>
                                <?php $image = get_field('po_image'.$GLOBALS['device']); ?>
                                <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                            <?php else: ?>
                                <?php the_post_thumbnail( 'thumbnail' ); ?>
                            <?php endif; ?>
                        </a>
                        
                        <div <?php if(get_the_post_thumbnail() || get_field('po_image'.$GLOBALS['device'])): ?>class="post-excerpt"<?php endif; ?>>
                        
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            
                            <span><?php the_permalink(); ?></span>
                            
                            <p>
                                <?php excerpt(get_the_ID()); ?>
                                <a href="<?php the_permalink(); ?>" class="link">Read More</a>
                            </p>
                            
                        </div>

                    </div>
                    
                <?php endwhile; ?>
        
                <?php if($wp_query->max_num_pages > 1) : ?>
            
                    <?php search_nav(); ?>

                <?php endif; ?>

            <?php else: ?>

                <h3>Nothing Found</h3>

                <p>Please try again with some different keywords.</p>

            <?php endif; ?>
            
        </div>
        
        <aside class="right-side">
    
            <div class="program-areas">
            
                <h2>Program <b>Areas</b></h2>
                
                <div>
                
                    <p>We offer career training that will help you start in a new rewarding career in a matter of months.</p>
        
                    <?php $area_loop = new WP_Query( array( 'post_type' => 'programs', 'post_parent' => 0, 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1 ) ); if ( $area_loop->have_posts() ) : ?>

                        <ul>

                            <?php while ( $area_loop->have_posts() ) : $area_loop->the_post(); ?>
                            
                                <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                            <?php endwhile; ?>
                            
                        </ul>
                        
                    <?php wp_reset_postdata(); endif; ?>
                    
                </div>
            
            </div>
        
        </aside>
        
    </div>
    
</div>


<?php if( have_rows('fc_content', 'option') ): ?>

    <div class="section section-8" <?php if( get_field('fc_background', 'option') ): ?>style="background-image: url(<?php  the_field('fc_background', 'option'); ?>);"<?php endif; ?>>
        
        <div class="content group">

            <div class="career-areas">
                
                <div class="grid grid-3">
                        
                    <div class="grid-content">
                    
                        <?php while( have_rows('fc_content', 'option') ): the_row(); ?>
                    
                            <div class="grid-box">
                            
                                <div>
                                
                                    <?php if(get_sub_field('fc_heading', 'option')): ?>
                                        <h3 class="section-title"><?php the_sub_field('fc_heading', 'option'); ?></h3>
                                    <?php endif; ?>
                                    
                                    <?php if(get_sub_field('fc_description', 'option')){the_sub_field('fc_description', 'option');} ?>
                                    
                                    <?php if(get_sub_field('fc_type', 'option') == "External"): ?>

                                        <?php if(get_sub_field('fc_title', 'option') && get_sub_field('fc_link', 'option')): ?>
                                        
                                            <a class="link" href="<?php the_sub_field('fc_link', 'option'); ?>" target="_blank"><?php the_sub_field('fc_title', 'option'); ?></a>
                                            
                                        <?php endif; ?>
                                        
                                    <?php else: ?>

                                        <?php if(get_sub_field('fc_page', 'option')): $id=get_sub_field('fc_page', 'option'); ?>

                                            <a class="link" href="<?php echo get_the_permalink($id); ?>">
                                                <?php if(get_sub_field('fc_title_cu', 'option')){ the_sub_field('fc_title_cu', 'option');}else{ echo get_the_title($id);} ?>
                                            </a>
                                    
                                        <?php endif; ?>
                                    
                                    <?php endif; ?>
                                    
                                </div>
                                
                            </div>

                        <?php endwhile; ?>
                    
                    </div>
                    
                </div>
            
            </div>

        </div>
        
    </div>

<?php endif; ?>


<?php get_footer(); ?>