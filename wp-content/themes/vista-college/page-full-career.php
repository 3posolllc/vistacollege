<?php /* Template name: Left Career */ ?>

<style>
table.program-tf {
    width: 60%!important;
    margin: 0 auto!important;
}

footer.section {
    display: none;
}
.headertest2019 .header-right a.login, .newnav .header-right a.login{display:none!important}
@media screen and (min-width: 1290px) {
 .desktop-nav { padding-top: 60px; }
}
i.fa.fa-search {
    display: none;
}
.banner-content {
    background: url(https://www.vistacollege.edu/wp-content/themes/vista-college/img/online-banner.png) no-repeat!important;
}
.qlinks{font-weight:normal;color:#ffffff}
.qlinks:hover{color:#ededed}
.qemail {font-size:13px;}
.career-home {
    text-align: center;
    display: flex;
    justify-content: center;
}
.career-home a {
    pointer-events: inherit;
    display: inline-block;
    margin-bottom: 20px;
    line-height: 46px;
    color: #339999;
    background: #fff;
    cursor: pointer;
    border: 6px solid #339999;
    font-size: 18px;
    font-weight: 500;
    text-transform: uppercase;
    padding-left: 5px;
    padding-right: 5px;
    width: 100%;
}

.career-home a:hover{
    border: 6px solid #A08FCB;
}
</style>
<?php get_header(); ?>

    <?php if ( have_posts() ) : ?>
    
        <div class="section section-7">
        
            <div class="content group">
            <div class="combine">
                <aside class="left-side left" style="width:300px;padding-right: 20px!important;">
                
                        <?php  echo child_pages(); ?>
								
                	<div class="program-areas" style="margin-top:30px">
                    
                        <h2>Contact <b>Career Services</b></h2>
                        
                        <div>
                        
                            <p>Phone:  <a href="tel:+1972-707-8574" class="qlinks">972-707-8574</a><BR>
Text: <a href="sms:+1214-864-6239" class="qlinks">214-864-6239</a><BR>
or <a href="mailto:onlinecareerservices@vistacollege.edu"  class="qlinks">Email Us</a>
<BR><BR>
<strong>Department Hours: </strong><BR>
Our schedules are flexible to accommodate our student body and alumni.  Please reach out to schedule appointments convenient for you. 
 </p>
                   
                                                        
                        </div>
                    
                    </div>
					
                </aside>
             
                </div>
                <div class="center-contents">
                
                    <?php if(get_field('co_content'.$GLOBALS['device'])){the_field('co_content'.$GLOBALS['device']);} ?>
                    
                </div>
                
                
                
            </div>
            
        </div>

    <?php endif; ?>
<style>
	
/* accordion css */
/* Style the element that is used to open and close the accordion class */
.panel.show {
    padding-top: 10px;
    padding-bottom: 10px;
}
p.accordion {

    font-weight: bold;
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    text-align: left;
    border: none;
    outline: none;
    transition: 0.4s;
    margin-bottom:10px;
}

/* Add a background color to the accordion if it is clicked on (add the .active class with JS), and when you move the mouse over it (hover) */
p.accordion.active, p.accordion:hover {
    background-color: #ddd;
}

/* Unicode character for "plus" sign (+) */
p.accordion:after {
    content: '\2795'; 
    font-size: 13px;
    color: #777;
    float: right;
    margin-left: 5px;
}

/* Unicode character for "minus" sign (-) */
p.accordion.active:after {
    content: "\2796"; 
}

/* Style the element that is used for the panel class */

div.panel {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: 0.4s ease-in-out;
    opacity: 0;
    margin-bottom:10px;
}

div.panel.show {
    opacity: 1;
    max-height: 500px; /* Whatever you like, as long as its more than the height of the content (on all screen sizes) */
}
@media only screen and (max-width: 1200px) {
}

@media only screen and (max-width: 445px) {
p.accordion:after{content:none}
}
/* end accordion css */
.singlevideo{clear:both}
.recvids {
    background: #BDD6D7;
    margin-top: 40px;
    margin-bottom: 40px;
    padding: 30px;
    border: 5px solid #46B6B8;
}
.vidicon{float:left;max-width:45px}
.vidtitle{font-size:110%;font-weight:bold;padding-bottom:20px;}
table.program-tf {
  border-collapse: collapse;
  width: 100%;
  text-align: left;
  }
table.program-tf tr th {
    padding: 10px 10px;
    background-color: #A61E20;
    color: #fff;
    border-right: 1px solid #fff;
    text-align: center;
    font-weight: bold;
    font-size: 20px;
}
table.program-tf tr td {
    padding: 10px 10px;
    border-right: 1px solid #fff;
    color: #000;
    font-weight: 400;
}
table.program-tf tr td:last-child, table.program-tf tr th:last-child {border-right: none;}
table.program-tf tr {
    background-color: #D5D5D5;
}
table.program-tf tr:nth-child(even) {
  background-color: #E1E1E1;
}
.fixed-info {
    display: none;
}
.page-id-11026header ul.header-info, .page-id-11026.fixed-info  , .page-id-11026div#drift-widget-container{
    display: none;
}
.search-get, .fa-bars {display:block}

div#drift-widget-container {
    display: none;
}
div#drift-widget-container {
    display: none;
}
ul.header-info {
    display: none;
}
.right-side-testimonials .featured-testimonial:last-child .testimonials-link {
    padding-left: 0px!important; 
}
.footer-social {
    text-align: center;
}
ul.social-icons {
    margin: 0 auto!important;
    padding-top: 20px;
}
.footer-social p {
    clear: both;
}
.section.section-4.featured-posts {
    display: none;
}
.footerlnk a{font-weight:normal}

.footerlnk a:hover{color:#6E706F}

@media (max-width: 1441px) and (min-width: 1000px){
.featured-testimonial .testimonial {
    height: 370px;
    background-position: bottom right;
}
.testimonial p {
    font-size: 18px;
    line-height: 20px;}
}

@media (max-width: 1000px){
.featured-testimonial .testimonial{height:400px!important}
}
</style>        
<div class="footer-social">
            
        
                <ul class="social-icons">
                                            <li>
                            <a class="fa fa-facebook-square" href="https://www.facebook.com/VistaCollege/" target="_blank"></a>
                        </li>
                                                                <li>
                            <a class="fa fa-youtube-square" href="https://www.youtube.com/user/VistaCollegeBlog" target="_blank"></a>
                        </li>
                                                                                    <li>
                            <a class="fa fa-twitter-square" href="https://twitter.com/vistacollege" target="_blank"></a>
                        </li>
                                                                <li>
                            <a class="instagram" href="https://www.instagram.com/vista.college" target="_blank"><i class="fa fa-instagram"></i></a>
                        </li>
                                                                <li>
                            <a class="fa fa-linkedin-square" href="https://www.linkedin.com/school/1113166" target="_blank"></a>
                        </li>
                                                                <li>
                            <a class="fa fa-pinterest-square" href="https://sk.pinterest.com/vista_college/vista-college/" target="_blank"></a>
                        </li>
                                    </ul>
            <br>
                <p class="footerlnk">© 2020 Vista College | <a href="https://www.vistacollege.edu/terms-of-use/" target="_blank">Terms of Use</a> | <a href="https://www.vistacollege.edu/privacy-statement/" target="_blank">Privacy Policy</a></p><br>
            
            
                </div>
<?php get_footer(); ?>
