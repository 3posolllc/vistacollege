<?php
Header( "HTTP/1.1 301 Moved Permanently" );
Header( "Location: /rise-of-online-education/" );
?> 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="Learning is no longer limited to four walls.">
<title>Rise of Online Education | Infographic</title>
<style type="text/css">
body {
	font-family: Verdana,Arial, Helvetica, sans-serif;
}
</style>
</head>

<body>

<!-- AddThis Smart Layers BEGIN -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51fff5a646631478"></script>
<script type="text/javascript">
  addthis.layers({
    'theme' : 'transparent',
    'share' : {
      'position' : 'left',
      'numPreferredServices' : 6
    }, 
    'follow' : {
      'services' : [
        {'service': 'facebook', 'id': 'vistacollege'},
        {'service': 'twitter', 'id': 'vistacollege'},
        {'service': 'google_follow', 'id': '108015211374279344383'}
      ]
    }   
  });
</script>
<!-- AddThis Smart Layers END -->

<div align="center">
<span>Share this infographic on your site!</span><br/>

	<textarea rows="4" cols="40" onclick="this.select();"><a href="http://www.vistacollege.edu"><img src="http://www.vistacollege.edu/files/info/rise-of-online-education.jpg" alt="Which career is best for me?" width="700"  border="0" /></a><br />
<br />Image courtesy of: <a href="http://www.vistacollege.edu">www.vistacollege.edu</a></textarea>
   <br />
<br />
 <img src="rise-of-online-education.jpg" width="700px" />
</div>
</body>
</html>
