<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'vistacollege' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'KZC=f)bBD`c>PDfRUvJXL:CgxJ8WoQl8MI+r:zzY?0tt% u|%,v_@80|zY;Mdbo~' );
define( 'SECURE_AUTH_KEY',  '07oxs&&+nhdwO~KknP0%{>$&e.AJu4N:b {P!5*_<Kc]6&1jyyj6DWlgJGF#UbkK' );
define( 'LOGGED_IN_KEY',    'K.djn*v/e_xZm@7,dDPUZ.i4|F+4QUoAL#E Go>T5`V-tOPm}g:Qm{Q@0Ny!ay`a' );
define( 'NONCE_KEY',        ' of4{*c1)Ho:T21j7HcN3;-S~$F(pw1,n9.)0xI`~8>-a+nskIZ#2JI(7{?UvDW`' );
define( 'AUTH_SALT',        'R$&iV9gHs`@!(|gmU Jy`0H)8Eijulw$j44qhN`zMTDco<%TgD7`)OT_Ya[<x&Fq' );
define( 'SECURE_AUTH_SALT', 'VIO3.Ic1p&G?OBI/ur[wrH4Yaj#qeiynVe6B(B{F?nEUAEezgS$4J34|nyIt`lFR' );
define( 'LOGGED_IN_SALT',   '<O].;Ny}`JeL&hHoO1xe8B>r#L7/i6j&mA<;7Af3Ls,5o_m3zx9 8fR} c_&7xrL' );
define( 'NONCE_SALT',       '<b&X@Y%{P~R[f%H;&9IW5]3~ iu2EXH}lL!P*)&nA*,gCJ#bSab,F-l0IFED) r^' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
